---
date: 2019-07-21T10:36:00+02:00
title: Tests de fumée
slug: tests-de-fumee
author: chop
categories:
  - nouvelles
tags:
draft: true
aliases:
  - /smoke-tests/
---

Ce post ne devrait pas être publié car son unique but est de tester certaines fonctionnalités du générateur et du thème.

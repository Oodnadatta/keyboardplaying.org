---
date: 2019-07-21T10:36:00+02:00
title: Smoke tests
subtitle: Making sure the template works
slug: smoke-tests
banner:
  src: /img/duke-hello.jpg
  alt: Java's mascot Duke waving at the screen from behind a wall.
description: This is the description that should be used for SEO cards.
author: chop
categories:
  - news
tags:
  - blog
license: CC BY-SA 4.0
draft: true
aliases:
  - /smoke-tests/
---

This post should not be published as its only purpose is to test out some features of the static site generation and Keyboard Playing theme.

<!--more-->

## Markdown tests

### Title hierarchy

#### Title level 4

##### Title level 5

###### Title level 6

Hello, World!
I'm a **bold** post, but I also happen to be _slanted_ sometimes.

I have a bucket list:

* Be pretty
* Be smart

But there are priorities:

1. Be smart first
2. Be pretty second

> This is a blockquote.
> <footer>anonymous</footer>


## Goldmark tests

Goldmark has some additional features.

**Dash formatting:**
`-`, `--` and `---` should render respectively as a normal dash, a medium dash and a long dash: - -- ---

**Footnotes:**
This is an example of a footnote[^fn].

[^fn]: And it does have a footnote with a link to [Duck Duck Go](https://duck.com "Duck Duck Go")!

**Description lists**

Header 1
: Value 1

Header 2
: Value 2.1
: Value 2.2

Header 3
: Value 3

**Code block**

```java
System.out.println("Hello, World!");
/*
 * This code is highlighted,
 * even though it's a simple fenced block
 * and not a Hugo shortcode.
 * <p/>
 * I am adding some lines artificially,
 * so that this code block can be used to test
 * line numbers.
 * </p>
 * Finally, I have ten lines of code.
 */
```


## Custom-tailored things

### Shortcodes

* `icon`: {{< icon "cc" >}}
* `numfmt`: {{< numfmt 41.5 >}} {{< numfmt 13.37 2 >}}

### Styles

* This is `.surname`: John <span class="surname">Doe</span>
* This is `.century`: The <span class="century">xx</span><sup>th</sup> century.

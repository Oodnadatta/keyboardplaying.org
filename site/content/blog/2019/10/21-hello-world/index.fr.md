---
date: 2019-10-21T18:52:58+02:00
title: Bonjour, monde !
subtitle: Et n'est-ce pas le meilleur des mondes ?
slug: bonjour-monde
banner:
  src: /img/duke-hello.jpg
  alt: La mascotte de Java, Duke, faisant signe à l'écran depuis le coin d'un mur.
  by: alcibiade
  license: CC BY 4.0
author: chop
categories:
  - nouvelles
tags:
  - blogue
---

[blog-board]: https://gitlab.com/keyboardplaying/keyboardplaying.org/-/boards/1340192


Ce site s'est endormi il y a quelques années --- 3,5 ans exactement.
Aujourd'hui, je souhaite lui donner une nouvelle vie et, espérons-le, la rendre plus longue que ses précédentes.

Ce premier article est l'occasion de présenter _Keyboard Playing_ et le futur que je lui imagine dans les semaines à venir.
Il explique également pourquoi vous n'avez pas trouvé l'article que votre moteur de recherche vous indiquait.

<!--more-->

## À propos de _Keyboard Playing_ et de son passé

J'ai créé _Keyboard Playing_ en 2011.
Initialement, il devait surtout me servir à héberger des outils de ma création en ligne.

Trouver un nom de domaine correct a été un défi.
Je n'avais pas envie de quelque chose qui soit lié à... moi.
J'espérais en faire un espace collaboratif.
Je ne voulais pas non plus le rendre trop restrictif, car, bien que l'informatique occupe la majorité de mon temps, ce n'est pas ma seule passion.

Le site a connu plusieurs transformations au fil des ans, souvent pour tester de nouveaux outils pour mon travail[^twbs] ou par curiosité[^mdl].
En 2012, un blogue WordPress est apparu.
Il est resté actif pendant deux ans avant de tomber en désuétude.
J'ai fait une autre tentative en 2015.
Cela a fonctionné pendant un an avant que j'abandonne, essentiellement parce que je ne trouvais plus le temps.

[^twbs]: C'est ainsi que j'ai passé un week-end à apprendre les bases de [Bootstrap] (https://getbootstrap.com/).
[^mdl]: Et c'est ainsi que j'ai refait le site en utilisant [Material Design Lite] (https://getmdl.io/) pour en découvrir les possibilités.

Aujourd'hui, je rêve à nouveau de partage.
J'ai passé un certain temps à choisir les outils que j'allais utiliser et à préparer les bases de la version 2.0 de _Keyboard Playing_.
Le travail n'est pas encore terminé, mais, en accord avec la philosophie Agile, je vous offre aujourd'hui cette première version et continuerai à l'améliorer incrémentalement.


## À propos de son avenir (proche)

### Maintenant en deux langues !

Jusqu'ici, _Keyboard Playing_ n'était disponible qu'en anglais.
Cela me semblait effectivement plus logique pour pouvoir partager avec tous le potentiel lectorat intéressé de par le monde.

Cependant, cette nouvelle version sera multilingue et inclura une version française.
Tout simplement parce que le français est ma langue maternelle que je l'aime.
J'ai aussi pu me rendre compte qu'il est toujours plaisant de pouvoir lire un article dans sa langue maternelle.
Ce sera donc un confort que je serai heureux d'offrir à tous mes hypothétiques lecteurs français.

J'adorerais apprendre une nouvelle langue et l'inclure comme autre langue sur ce site[^lg].
Il ne s'agit cependant pour l'instant que d'une idée et, quoiqu'il y arrive, il y aura un long chemin avant que je sois prêt à écrire dans une troisième langue.
D'ici là, français et anglais feront l'affaire.

[^lg]: Je pense assez sérieusement à [l'espéranto](https://fr.wikipedia.org/wiki/Esp%C3%A9ranto) (car elle a été conçue comme un outil de communication internationale) ou au [luxembourgeois](https://fr.wikipedia.org/wiki/Luxembourgeois) (parce que c'est l’une des langues officielles du pays dans lequel je travaille).

Si vous cherchez la version anglaise d'une page, les liens de traductions sont à côté d'une icône de drapeau ({{< icon "flag" >}}) lorsqu'ils sont disponibles.


### Le blogue comme clé de voûte

La partie la plus active de _Keyboard Playing_ a toujours été son blogue.
Il est donc logique qu'il soit la pierre sur laquelle je rebâtirai le site, mais des modifications sont de mise.


#### Une approche de zéro

J'ai choisi d'oublier ce que j'avais fait par le passé.
Tout a été archivé afin de pouvoir repartir de zéro.
Si vous cherchiez un de mes précédents articles, il est [disponible dans le dépôt Gitlab de ce projet](https://gitlab.com/keyboardplaying/keyboardplaying.org/tree/archive/site/content/archive).
Dans le cas où le sujet vous intéressait, faites-le moi savoir sur le [tableau des sujets d'articles sur Gitlab][blog-board].


#### Des sujets variés

Sur quels sujets vais-je écrire ?

* La conception et la création logicielles (en bref : le « **codage** ») sont le sujet qui occupe la majeure partie de mon temps, et une véritable passion.
  Vous pouvez vous attendre à ce que j'écrive à ce sujet.
  J'aborderai des sujets tirés de mon expérience, allant de simples astuces aux bonnes pratiques et des raisons pour lesquelles elles sont recommandées.
  J'espère pouvoir également découvrir de façon approfondie de nouveaux langages, outils ou _frameworks_ et vous en faire des comptes-rendus intéressants.

* Étant donné le nom du site, il serait dommage de ne rien écrire sur les **claviers**.
  Je parlerai essentiellement de l'histoire de ces dispositifs et partagerai quelques réflexions sur le futur, les alternatives...

* Cela peut sembler en contradiction avec d'autres sujets, mais je souhaite également parler du développement durable appliqué à mon domaine d'expertise.
  En d’autres termes, je voudrais partager quelques réflexions sur le **numérique responsable** et sur les raisons pour lesquelles ce devrait être une préoccupation dès le début de la conception d'un service numérique.
  C'est vrai, nous pouvons penser que nous travaillons dans une industrie qui ne produit que du virtuel, mais ce virtuel a besoin de machines bien matérielles et polluantes.
  En plus de cela, l'aspect « planète » n'est pas suffisant : le développement durable repose également sur le pilier « société » (autrement dit : l'humain).
  Mais nous y reviendrons...

Il m'arrivera certainement d'écrire sur des sujets dont je ne suis pas un expert.
À ces moments, je mènerai une enquête jusqu'à ce que le matériel accumulé me semble suffisamment solide pour établir une synthèse cohérente.
Mes articles répertorieront mes sources.
Cela ne signifiera pas que tout sera juste ou complet.
N'hésitez pas à me corriger.
J'apprécie la critique constructive et les conseils.


Un [tableau sur Gitlab][blog-board] liste des sujets que j'aimerais aborder.
Il est accessible au public.
Si vous avez une idée d'un sujet sur lequel vous croyez que je serais compétent, proposez-le !


#### Élargir les horizons du blogue

Comme écrit plus haut, en créant ce site, j'espérais que des amis (professeurs, pianistes, bioinformaticiens...) auraient envie d'y partager leur propre expérience en matière de claviers, de musique, de conception d'algorithmes, d'écriture, d'enseignement...
Ça ne s'est pas fait à l'époque et je n'ai aucune raison de penser que cela sera différent cette fois-ci.

Néanmoins, si vous souhaitez occasionnellement sur ce que vous faites grâce à votre clavier, contactez-moi pour voir ce que nous pouvons faire.
Il n'est pas nécessaire que cela soit récurrent : la nouvelle structure du blogue me permet de partager facilement des posts ponctuels d'invités prestigieux.


### Ajout d'une dose de fiction dans l'écriture

Il n'y a pas que le travail dans la vie.
J'ai toujours eu envie d'être un artiste, mais je ne me suis jamais trouvé aucun talent.
D'autres, amis ou collègues, m'ont dit aimer la manière dont j'écris.

Depuis quelques semaines, je participe à un groupe de personnes qui essaient de développer cette compétence grâce à une motivation mutuelle.
Dans les prochaines versions du site, je partagerai certaines de mes créations dans une nouvelle section intitulée « _Histoires_ ».


### Rendre le site plus sexy

Je viens de le dire : je ne suis pas un artiste.
Ce site devrait être acceptable par un spécialiste UX[^ux], mais il n'est pas spécialement joli.
Si quelqu'un possédant des goûts plus sûrs que les miens lit cet article, [je suis preneur de tout conseil](https://gitlab.com/keyboardplaying/keyboardplaying.org/issues/6).

[^ux]: J'espère que _Keyboard Playing_ est utilisable et agréable à utiliser. Si vous voyez des choses qui pourraient être améliorées, n'hésitez pas à [créer un ticket](https://gitlab.com/keyboardplaying/keyboardplaying.org/issues/new).

Par exemple, j'adorerais un thème sombre par défaut, mais je peine à en créer un qui ne soit pas oppressant.
Je suppose que la disposition générale pourrait être rendue plus ergonomique également.
Toute idée pour améliorer les choses est la bienvenue !


### Préserver la flamme

À l'heure où j'écris ces lignes, j'ai l'espoir d'assurer une certaine fréquence dans la production de contenu.
Une publication par semaine serait un bon rythme, une par mois semble plus réaliste.

N'hésitez jamais à commenter.
Savoir que j'ai un lectorat intéressé est un moteur puissant !

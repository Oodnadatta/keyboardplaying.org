---
date: 2019-10-21T18:52:58+02:00
title: Hello, World!
subtitle: And what a brave new world it is!
slug: hello-world
banner:
  src: /img/duke-hello.jpg
  alt: Java's mascot Duke waving at the screen from behind a wall.
  by: alcibiade
  license: CC BY 4.0
author: chop
categories:
  - news
tags:
  - blog
---

[blog-board]: https://gitlab.com/keyboardplaying/keyboardplaying.org/-/boards/1340192


This website has been dormant for a few years, now---3.5 years, actually.
Today, I wish to resuscitate it, and hopefully see it a bit more alive than in the past.

This first post is the occasion to introduce _Keyboard Playing_ and what I hope it'll become in the coming weeks.
It may also explain why you haven't found the post your search engine pointed you to.

<!--more-->

## About _Keyboard Playing_ and Its Past

I created _Keyboard Playing_ in 2011.
Its initial purpose was to be a place for me to host some tools of my creation online.

Finding a suitable domain name was a challenge!
I didn't want something labeled to my name, as I hoped to make it a collaborative space down the way.
I didn't want to make it too restrictive either as, though IT[^it] is my main occupation, it's not my only passion.

[^it]: Yes, you know it, but just in case you don't, "_IT_" means "_Information Technologies_".

The website has known several transformations over the years, often to learn new tools for my job[^twbs] or out of curiosity[^mdl].
In 2012, a WordPress-based blog appeared.
It remained active for about two years and got dormant.
I made another attempt at it in 2015.
It worked for one more year before giving up in 2016, essentially because I didn't find the time anymore.

[^twbs]: This is how I spent one weekend learning the basics of [Bootstrap](https://getbootstrap.com/).
[^mdl]: And this is how I redesigned the website using [Material Design Lite](https://getmdl.io/) to discover its possibilities.

Today, the will to share is burning hot.
I've spent some time choosing the tools I wanted to use and preparing the basics of this v2.0 of _Keyboard Playing_.
The work's not finished yet but, following the Agile philosophy, I deliver this first version today and will continue working to improve it incrementally.


## About Its (Near) Future

### Now in Two Languages!

In previous versions, _Keyboard Playing_ was available only in English, because it's the language of international sharing and it's easier to understand for the majority of the people on the planet.

However, this new version will be bilingual and will include a French version.
Because French is my mother tongue and I love this language.
Plus, I've realized how much more comfortable it is to read in your native language, even if you're fluent.
This will be something I'm glad to offer to all my hypothetical French readers.

I'd love learning a new language and including it as another alternative on this website[^lg].
Right now however, this is just an idea and, in any case, it will be a long road before I'm ready to write anything in a third langauge.
In the meantime, English and French will do.

[^lg]: I'm currently thinking of [Esperanto](https://en.wikipedia.org/wiki/Esperanto)---because it was designed as an international communication tool---or [Luxembourgish](https://en.wikipedia.org/wiki/Luxembourgish)---because it's one of the languages of the country I work in.

If you're looking for the French version of a page, search the flag icon ({{< icon "flag" >}}) and you'll see translation links beside if they're available.


### The Blog as the Keystone

The most active part of _Keyboard Playing_ has always been its blog, so it will be the stone I'll rebuild it upon, but I'll make some changes on it.


#### A From-Scratch approach

I chose to forget everything I'd done in the past, archiving it to begin the blog anew.
Reader, if you were looking for one of my previous posts, it's available [somewhere in the Gitlab repository of this project](https://gitlab.com/keyboardplaying/keyboardplaying.org/tree/archive/site/content/archive).
In the case the topic was of interest to you, please mention it on [the subject board on Gitlab][blog-board].


#### Varied Topics

What topics do I think of writing about?

* Well, software design and creation (in short: "**coding**") is the topic that occupies most of my time and a real passion, so you can expect I'll write about it.
  I'll mainly discuss things I've learned from experience, from common tips to best practices and the reason they are recommendable.
  Hopefully I'll find time to dive into great new languages, tools or frameworks and share interesting summaries of it.

* Given the name of the website, it would be a shame not to write a thing or two about **keyboards**.
  I'll write a bit about the history of these everyday devices, and share some thoughts about the future, alternatives...

* This may seem at odds with other topics, but I also wish to write about sustainable development applied to my field of expertise.
  In other words, I'd like to share some thoughts about a **sustainable digital** and why it should be a focus from the start when we begin designing a new digital service.
  Yes, we may think we work in an industry that creates only virtual goods, but these goods _do_ need really material and polluting machines to run on.
  On top of that, the _planet_ aspect is not enough: _people_ are also a pillar of sustainable development.
  But more on that later.

I may write about subjects I'm not an expert about.
In such cases, I'll investigate until I feel I have enough material to fully understand and I'll include sources into my posts, trying to synthesize what's most important.
That won't make it correct or complete.
I'll rely on your feedback, because I believe constructive criticism can help me get better.

A [board is available on Gitlab][blog-board] where I listed some topics I'd like to write about.
It should be publicly available, so if you have an idea about a topic you think I'd be competent/relevant writing about, please submit an issue!


#### Expanding the Blog's Horizons

As I told earlier, I created this website hoping some friends (teachers, pianists, bioinformaticians...) would wish to share about their own experiences with keyboards, music playing or composing, algorithms designing, writing, teaching...
It did not work at the time and I have no reason to think it'll be different this time.

Still, if you're willing to occasionally share about things you do with a keyboard (music, creative writing, video editing... What do I know?), don't hesitate to contact me to see what we could do.
This doesn't even have to be a regular thing: the new structure of the blog makes it easy for me to share one-shot, guest starring posts.


### Adding a Dose of Fiction in the Writing

Work is not all there is to life.
I've always wished I were an artist, but I've never recognized myself any talent.
Still, many friends and colleagues told me they like the way I write.

Since a few weeks, I've joined a group of people trying to develop this skill through mutual motivation.
In the upcoming versions of the website, I'll share some of my creations in a new section called "_stories_."


### Building a More Eye-Pleasing Website

As I just said, I'm no artist.
This website meets usability requirements[^ux], but it may look better.
If anyone with surer tastes than mine comes to read this post, [please know your advice is welcome](https://gitlab.com/keyboardplaying/keyboardplaying.org/issues/6).

[^ux]: That is, I hope it's usable and pleasant to use. If you see things that could be better, please don't hesitate to [create an issue](https://gitlab.com/keyboardplaying/keyboardplaying.org/issues/new).

For instance, I'd love making the default theme a dark one, but I seem to be unable to create one that doesn't feel oppressive.
I suppose many things about the general layout could be made more ergonomic too.
Any idea to make things better is welcome!


### Keeping the Flame Alive

Right now, I hope I'll be able to maintain a certain frequency in the production of content.
One publication per week would be nice, one per month seems more realist.

Never hesitate to comment.
Knowing that I have interested readers is a potent drive!

---
date: 2019-12-31T19:11:21+02:00
title: Une revue de mes lectures en 2019
slug: revue-lectures-2019
description: Nous devons nous maintenir à jour pour rester pertinents. Voici une rapide revue de 2019 pour moi sur ce sujet.
banner:
  src: janko-ferlic-specialdaddy-sfL_QOnmy00-unsplash.jpg
  alt: Photo d'une bibliothèque couverte de livres le long d'un couloir éclairé par des ampoules vintage, avec une lumière extérieure visible en arrière-plan.
  by: 🇸🇮 Janko Ferlič
  link: https://unsplash.com/photos/sfL_QOnmy00
  license: Unsplash

author: chop
categories:
  - general
tags:
  - management
---

La fin de l'année est propice aux bilans.
« Qu'ai-je appris cette année ? » est toujours un bon sujet de réflexion.
L'informatique évolue sans cesse et il nous faut rester à jour pour ne pas nous noyer.
Dans notre culture, la plupart des connaissances sont stockées et partagées sous forme écrite, donc une partie de la question ci-dessus devient : « Quelles lectures éclairantes ai-je eues cette année ? »

<!--more-->

## Mes lectures de 2019

### Livres

Les livres sont la première chose à laquelle nous pensons lors de ce type de revue et j'en ai effectivement lu quelques-uns sur le sujet de l'informatique et du numérique cette année, bien que la majorité d'entre eux n'est pas _si_ technique.


#### _97 Things Every Software Architect Should Know_

<img src="97-things-every-software-architect-should-know.png" class="float-left" alt="Couverture du livre « 97 Things Every Software Architect Should Know »" title="97 Things Every Software Architect Should Know">

2018 fut un défi professionnel pour moi.
J'ai participé à un projet avec de beaux enjeux et beaucoup de pression, et c'était la première occasion de me confronter réellement à mon rôle d'« [architecte technique]({{< relref path="stories/2019/11/29-challenge-metaphore" >}}) ».
Le problème ne portait pas sur la partie « technique » --- c'est ce qui m'a amené à faire ce travail en premier lieu --- mais je n'étais pas certain de faire ce qu'il fallait d'un point de vue managérial.

Quelques entretiens avec mes équipiers et supérieurs m'ont rassuré, mais ce livre a réveillé ma curiosité et mon envie d'élargir ma perspective sur ce rôle.
Il a en effet été écrit comme une collection de courts articles --- qui font 5 pages pour les plus longs --- écrits par des architectes logiciels expérimentés.
Chaque article donne un conseil précis et l'explique.

Je recommande ce livre à tout « architecte » ou personne souhaitant le devenir.
Il n'est pas purement technique, loin de là.
Plusieurs fiches nous rappellent d'ailleurs qu'un architecte _ne peut pas_ avoir une approche purement technique, car cela aboutirait à des solutions trop chères ou ne répondant pas au besoin initial.
D'autres mettent en exergue la nécessité de garder une vue d'ensemble, donnent des conseils sur le comportement à adopter avec votre équipe --- un architecte doit être un leader dans ma perception...
De nombreux conseils peuvent paraitre du bon sens, mais comme souvent dans ce cas, le voir expliqué nous aide à réaliser que _c'est_ une chose à faire et _pourquoi_ c'est le cas.

Cliquez [ici](http://shop.oreilly.com/product/9780596522704.do) pour davantage d'informations et une possibilité d'achat[^oreilly].

[^oreilly]: Si vous travaillez dans le numérique, vous devriez vérifier avec votre employeur.
Je sais que plusieurs sociétés ont des accès gratuits à une sélection de livres O'Reilly.


#### Sobriété numérique : les clés pour agir

<img src="bordage--sobriete-numerique.jpg" class="float-left" alt="Couverture du livre « Sobriété numérique&nbsp;: les clés pour agir »" title="Sobriété numérique&nbsp;: les clés pour agir">

Un nombre grandissant d'individus semble s'intéresser à la planète.
Pourtant, beaucoup d'entre nous, notamment les générations les plus jeunes, ne peuvent passer une journée sans smartphone, ces petits appareils de 120 g construits à partir de 70 kg de matériaux bruts, qui ont nécessité une extraction et une transformation polluantes.
La même chose s'applique aux écrans et télévisions, toujours plus grands.

Ce livre s'adresse à tous, pas uniquement aux personnes travaillant dans le numérique.
Il se divise en deux parties :
- La première est un lexique des termes liés à l'écoconception et autres pensées environnementales.
Chaque mot ou expression est clairement défini et souvent accompagné d'un exemple.
Le lexique peut parfois sembler répétitif ou redondant, mais c'est souvent le prix à payer pour bien assimiler une information.
- La seconde partie du livre est bien plus courte et plus directe.
Elle donne des conseils pour agir et explicite quelques fausses bonnes idées [^fbi].
C'est un véritable manuel pour ceux qui souhaitent réduire leur empreinte environnementale par le numérique et aider la planète.

[^fbi]: La plupart des fausses bonnes idées dans ce contexte ne sont pas des _mauvaises_ idées, mais elles n'ont pas l'impact que l'on pourrait espérer d'elles.

Cliquez [ici](http://www.buchetchastel.fr/sobriete-numerique-frederic-bordage-9782283032152) pour davantage d'informations et une possibilité d'achat.


#### Écoconception web : les 115 bonnes pratiques

<img src="bordage--ecoconception-web-115-bonnes-pratiques.png" class="float-left" alt="Couverture du livre « Écoconception web&nbsp;: les 115 bonnes pratiques »" title="Écoconception web&nbsp;: les 115 bonnes pratiques">

Les enjeux sont certainement un peu plus clairs avec la présentation de _Sobriété numérique_ ci-dessus.
Les deux livres sont du même auteur et leurs sujets sont liés.
Nous savons que le numérique a un impact environnemental, ce qui signifie qu'il en est de même pour les technologies de l'information et le logiciel --- j'aborderai ce sujet plus en détail très prochainement.

Ce livre se concentre sur le développement web et suggère de bonnes pratiques pour réduire l'empreinte d'un site web.
Elles sont présentées sous forme de fiches qui résument la pratique, une explication et un exemple, et une mesure de la difficulté et de l'impact de l'adoption de cette pratique (sur le processeur, la mémoire, la bande passante, etc.).

Cliquez [ici](https://www.greenit.fr/2019/05/07/ecoconception-web-les-115-bonnes-pratiques-3eme-edition/) pour davantage d'informations et une possibilité d'achat.
Si vous souhaitez seulement avoir un aperçu des bonnes pratiques sans les détails, elles sont disponibles publiquement [ici](https://collectif.greenit.fr/ecoconception-web/).

Je vous invite à noter que le format « fiches mémo » n'est pas réellement confortable à lire sur en EPUB.
Je recommande plutôt les éditions PDF ou papier.


### Livres blancs, articles et études

Les livres ne font pas tout.
De nombreuses études et livres blancs sont disponibles gratuitement pour vous illuminer sur de nombreux sujets insoupçonnés.


#### Empreinte environnementale du numérique mondial

Cette étude donne une vue d'ensemble plutôt complète des impacts de l'univers numérique sous toutes ses formes : ordinateurs, smartphones, écrans, internet des objets et centres informatiques.
Elle comprend une analyse de l'empreinte de la fabrication, l'utilisation et la fin de vue de tous ces produits.
Elle présente également l'évolution de cet univers au cours de la dernière décennie et une prévision de ce que pourrait être la prochaine.

Par exemple, elle estime que nous sommes {{< numfmt 4.1 1 >}} milliards à utiliser les 34 milliards d'appareils existants, ce qui signifie que chacun d'entre nous dispose en moyenne de 8 appareils.
Elle nous indique aussi que, bien qu'ils n'existaient pratiquement pas il y a dix ans, les appareils connectés seraient aujourd'hui au nombre de 19 milliards.

Bien entendu, des études à cette échelle ne sont pas parole d'évangile : la quantité de données à trouver et compiler rend presque impossible la production de chiffres exacts, et les nombres sont différents de ce que vous donneront d'autres sources.
Elle reste néanmoins intéressante à prendre en compte.

Vous pouvez lire l'étude complète [ici](https://www.greenit.fr/empreinte-environnementale-du-numerique-mondial/)


#### Lexique Green IT

Il s'agit d'un petit lexique court et synthétique pour les personnes souhaitant se lancer dans l'écoconception.

Disponible [ici](https://alliancegreenit.org/lexique-green-it).


#### Pour un numérique plus responsable --- Sensibilisation grand public

On retrouve dans cette publication l'objectif derrière _Sobriété numérique_, avec une présentation de l'impact du numérique et des conseils pour le limiter.
Les choses y sont présentées de façon plus synthétique et visuelle, et de petits quizz sont là pour renforcer l'aspect ludique de l'apprentissage.

Très intéressant pour les personnes ne maitrisant pas les détails techniques du numérique mais souhaitant réduire leur empreinte.

Disponible [ici](https://alliancegreenit.org/2019-sensibilisation-grand-public).


### Blogs

Une fois de plus, je ne retrouve plus ma source, mais un récent sondage indiquait que la principale source sur laquelle se reposent les développeurs pour se tenir à jour est constituée des blogs de leurs confrères --- raison pour laquelle il [est toujours frustrant]({{< relref path="blog/2019/11/11-cost-of-time" >}}) de découvrir que la société a bloqué blogspot.
En recréant mon blog, j'ai aussi rafraichi la liste de blogs que je surveille.
Ce travail n'est pas terminé, mais je partagerai à ce sujet plus tard.


## Ce que j'espère lire en 2020

Ne vous arrêtez jamais à ce que vous connaissez déjà, il y a toujours plus à découvrir.
Je vous propose ci-dessous une liste non exhaustive de ce que j'espère lire l'an prochain --- elle s'insère entre mes listes de jeux vidéo à terminer et de lectures non techniques.

- [_The Robert C. Martin Series_](https://securityinnovationeurope.com/blog/page/40-blogs-every-software-developer-should-be-reading).
Peut-être pas toute la série d'un coup, mais je suis tout particulièrement intéressé par _Clean Code_, _Clean Architecture_ et _The Software Craftsman_.
En lire au moins deux de ceux-là en 2020 serait un bon début.

- [_High Performance Java Persistence_](https://vladmihalcea.com/books/high-performance-java-persistence/) de Vlad Mihalcea est certainement une mine d'or au vu de ce qu'offre déjà [son blog](https://vladmihalcea.com/blog/).
J'ai passé quelques années sans utiliser JPA car mes clients préféraient connaitre les requêtes SQL exécutées, mais JPA est maintenant sur le devant de la scène avec Spring Data et on se rend compte que [les développeurs ne savent pas toujours l'utiliser de façon appropriée et efficace]({{< relref path="blog/2019/12/27-know-technology-not-framework" >}}).
Je souhaite renforcer mes connaissances de ses subtilités pour m'appuyer pleinement dessus et aider mes équipiers à en faire de même.

- [_Numérique et impact social_](https://alliancegreenit.org/numerique-durable-gouvernance-et-equite-sociale-partie-1) est un livre blanc publié en plusieurs modules, une fois par mois.
Le titre est explicite et les chapitres déjà disponibles comprennent _Relation employeur-employé_, _Qualité de vie au travail_, _Santé et sécurité au travail_...

Ce ne sont que quelques idées et j'espère lire davantage au cours de cette nouvelle année.


## Pour conclure (ce billet et cette année)

2019 se termine.
J'ai redémarré ce blog et, bien qu'il soit plutôt calme à l'heure actuelle, j'adore partager.
J'espère que certains de ces conseils et pensées seront utiles.

En tous les cas, je vous souhaite une **excellente nouvelle année !**

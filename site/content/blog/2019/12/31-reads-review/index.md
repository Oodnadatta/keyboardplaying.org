---
date: 2019-12-31T19:11:21+02:00
title: A Review of My 2019 Readings
slug: review-readings-2019
description: We need to read to stay relevant and up to date. Here's a quick review of 2019 for me.
banner:
  src: janko-ferlic-specialdaddy-sfL_QOnmy00-unsplash.jpg
  alt: Photo of bookshelves with turned on, vintage lightbulbs, with a vision of outdoor light at the end of the corridor.
  by: 🇸🇮 Janko Ferlič
  link: https://unsplash.com/photos/sfL_QOnmy00
  license: Unsplash

author: chop
categories:
  - general
tags:
  - management
---

The end of the year is a good time for assessments.
A good one is, "what have I learned this year?"
IT is a field that keeps moving, and we need to stay up to date if we don't want to drown.
In our culture, most of our knowledge is stored and shared through writing, so a part of my question becomes, "what have I read that was enlightening this year?"

<!--more-->

## What I Read in 2019

### Books

Books are the first thing that comes to mind when we make this kind of review, and I indeed read some IT-related ones this year, though the majority was not _that_ technical.

I'll provide you with a link to each book if you're interested, but this article is not sponsored.


#### 97 Things Every Software Architect Should Know

<img src="97-things-every-software-architect-should-know.png" class="float-left" alt="Cover of the book “97 Things Every Software Architect Should Know”" title="97 Things Every Software Architect Should Know">

2018 was hard on me, professionally speaking.
I worked on a project with high stakes, a lot of pressure and it was the first time I was really getting into my "[technical architect]({{< relref path="stories/2019/11/29-challenge-metaphore" >}})" shoes.
I'm ok with all the technical parts---I mean, that's what I've always loved---but I wasn't so sure I did all that was expected from me on the management side.

In the end, everything appeared to be fine, but when I discovered this book, I was curious to broaden my perspective about my role.
This book was indeed written as a collection of short articles---up to 5 pages, I guess---written by actual software architects.
Each article gives a specific bit of advice and explains it.

I'd recommend this book for anyone in the "architect" role or thinking of following this path.
It's not purely technical, far from it.
Actually, several entries remind us that an architect _can't_ take a purely technical approach, lest their solution is unfit or too expensive to realize within budget/time limits.
Others highlight that we should be able to keep a bird's-eye view, give you advice about how to behave with your team---an architect is a leader and a manager in my world...
Much of it is common sense, but like most common sense, seeing it explained helps us realize it _is_ a good thing and _why_ it is.

Click [here](http://shop.oreilly.com/product/9780596522704.do) for more information and buying options[^oreilly].

[^oreilly]: If you work in IT, you should check with your company.
I know several of them have free access to at least some O'Reilly books.


#### _Sobriété numérique : les clés pour agir_

<img src="bordage--sobriete-numerique.jpg" class="float-left" alt="Cover of the book “Sobriété numérique&nbsp;: les clés pour agir”" title="Sobriété numérique&nbsp;: les clés pour agir">

I didn't find an English version of this book.
It was published last September in France.
Its title could translate to _Digital Sobriety: the Keys to Action_.

People seem interested in saving the planet more than ever.
Yet, many of us, especially the younger generations, can't live a day without a smartphone.
We can't realize that those 120 g are built from 70 kg of raw material, which required a polluting extraction and transformation.
Same thing goes for all the displays, screens, televisions that go ever bigger.

This book is addressed to anybody, not only people working in the IT field.
It's divided into two parts:
- The first part is a lexicon of terms related to the field of ecological design and thoughts.
Each word or phrase is clearly explained and often illustrated with an example.
It may seem repetitive on some aspects, or a bit lengthy, but it's still interesting.
- The second part of the book is much shorter and more to the point.
It gives ways to act, reduce our digital footprint.
It includes giving advice and highlighting false good ideas[^fgi].
It's a real call to action for those who wish to help the planet.

[^fgi]: Most of the false good ideas highlighted here are not _bad_ ideas, but they're solutions that don't yield as much result as you'd hope.

Click [here](http://www.buchetchastel.fr/sobriete-numerique-frederic-bordage-9782283032152) for more information and buying options.


#### _Écoconception web : les 115 bonnes pratiques_

<img src="bordage--ecoconception-web-115-bonnes-pratiques.png" class="float-left" alt="Cover of the book “Écoconception web&nbsp;: les 115 bonnes pratiques”" title="Écoconception web&nbsp;: les 115 bonnes pratiques">

I didn't find an English version of this book either.
This is the 3<sup>rd</sup> edition and was published last May.
Its title could translate to _The 115 best practices for web ecological design_.

The stakes should be a bit clearer with the presentation of _Sobriété numérique_ just above.
Both books are indeed from the same author and the topics are related.
We know digital has an environmental impact, which indirectly means IT and software have a footprint too---I'll write more about this subject very soon.

This book focuses on the web development and suggests best practices to reduce the footprint of the website you'll develop.
Those are presented as flashcards, with the practice, an explanation and example, and evaluations of the difficulty and impact of adopting this practice (on the CPU, memory, network load and so on).

Click [here](https://www.greenit.fr/2019/05/07/ecoconception-web-les-115-bonnes-pratiques-3eme-edition/) for more information and buying options.
If you just wish to have a look at the best practices without the explanations, they are publicly available [here](https://collectif.greenit.fr/ecoconception-web/) (in English too, this time).

Please note that, due to the "flashcard" format of this book, it's not really comfortable to read as an epub.
I'd rather recommend the PDF or paper editions.


### White papers, Articles & Studies

Still, books are not everything.
Knowledge filters through all cracks, and some studies and white papers are waiting freely to give you more insight on many unsuspected topics.
I've read some interesting ones, but I'll keep only the one available in English on this page.
If you also wish to see the French one, please refer to [the French version of this page]({{< ref path="blog/2019/12/31-reads-review" lang="fr" >}}).


#### Environmental Footprint of the Digital World

This study gives a great summary of the impacts of the digital world, in all its forms: computers, smartphones, displays, IoT and data centers.
It includes an analysis of the footprint of the fabrication, use and end of life of all these products, and gives a wide overview.
It also includes an analysis of the evolution over the last decade and a prevision of what the next one may look like.

For instance, it estimates that we are {{< numfmt 4.1 1 >}} billion people using 34 billion devices, meaning each user has in average 8 digital devices.
It also tells us that, though they did not exist ten years ago, connected devices may amount to 19 billion now.

Of course, studies on that scale cannot be taken as gospel truth: the magnitude of data to find and crunch makes it hard to be exact, and numbers differ from what you may find on other sources.
Still, I think it should be taken into account.

You can read the full study [here](https://www.greenit.fr/environmental-footprint-of-the-digital-world/).


### Blogs

I can't find my source again but a recent survey highlighted that the first support for sharing and gaining knowledge for developers is still blogs like this one really---which is why having blogspot blocked by your company policy [is always frustrating]({{< relref path="blog/2019/11/11-cost-of-time" >}}).
Starting my blog from scratch, I also refreshed my blog list a bit.
This is still a work in progress and I'll share it later.


## Things I Hope to Read in 2020

Never settle for what you already know, there's always more to discover.
Below are some things I hope to find the time to read next year---this goes between my videogames-to-finish and non-technical-to-read lists.

- The [_Robert C. Martin Series_](https://securityinnovationeurope.com/blog/page/40-blogs-every-software-developer-should-be-reading).
Well, maybe not the whole series at once, but I'm especially interested in _Clean Code_, _Clean Architecture_ and _The Software Craftsman_ to begin with.
Reading at least two of those in 2020 would be a great start.

- [_High Performance Java Persistence_](https://vladmihalcea.com/books/high-performance-java-persistence/) by Vlad Mihalcea should be a goldmine if it's what [his blog](https://vladmihalcea.com/blog/) promises it to be.
I've had a few years without using JPA because my clients preferred explicit SQL, but now Spring Data JPA is everywhere and [developers don't always know how to use it properly or efficiently]({{< relref path="blog/2019/12/27-know-technology-not-framework" >}}).
I wish to reinforce my knowledge of its subtleties so that I use its features the correct way and can help my teammates doing so.

- [_Numérique et impact social_](https://alliancegreenit.org/numerique-durable-gouvernance-et-equite-sociale-partie-1) (_Digital and Social Impact_) is a white paper that is published in separate modules, once a month.
The title is self-explanatory, and the chapters already available include _Employer-Employee Relationship_, _Quality of Life at Work_, _Health and Security at Work_...

These are just some ideas and I hope to read a bit more than that next year.


## To Conclude (This Post and Year)

2019 is coming to an end.
I've rebooted this blog, and though it's rather calm now, I love sharing.
I hope some of these thoughts and advice will help someone.

In any case, I wish you a very **happy new year!**

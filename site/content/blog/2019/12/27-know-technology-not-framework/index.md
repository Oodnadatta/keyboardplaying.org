---
date: 2019-12-27T08:59:42+02:00
title: Learn the Technology, not the Framework
#subtitle: Would you rather spend that money now or later?
slug: learn-technology-not-framework
description: It seems developers nowadays learn frameworks rather than technologies. I would recommend doing the opposite...
#banner:
author: chop
categories:
  - programming
tags:
  - java
  - spring-boot
---

I've discussed with several people, this year.
Technical leads, technical supervisors, architects...
You get the gist.
Among those discussions, I heard a recurring complaint, which I previously feared to be my demanding nature expressing itself.

But no! Other people noticed it too, and it basically boils down to something like this: we're facing a new generation of developers, who like to do things fast and don't care much about how things work deep down, and we're living in an era proposing a new framework to help them go faster and not understand.
The two together won't make for great developers. Good developers, maybe, but not great ones.

<!--more-->


## What We've Observed

Jobs seem to be ever more specialized.
When I started working, it was common to see a developer in charge of realizing the analysis of what he would next program.
Nowadays, we have business analysts who know barely anything about technical aspects but do the whole pre-realization stuff.
In some cases, it's justified and it brings some new added value---though that's not the topic of this post.
In the case of development, however, I'm more doubtful.

I don't believe in specialized coders.
I don't think having developers only capable of working on front-end and others dedicated to back-end is a good idea.
It's my understanding that, in an Agile team, any developer should be able to perform any task on the project.
Of course, each individual has their own strengths, but, according to me, every software creator should be able to intervene on any layer of the solution they're working on.

<aside><p>Some developers learn only a framework rather than a language.</p></aside>

But now, not only do developers wish to specialize on only one aspect---ok, we all have things we don't like to do---they begin learning a framework rather than a language.
Who could blame them?
I mean, employers are looking for people specialized in Angular, React, Spring Boot...
Yeah, companies are in on it too.

By now, you may think I'm against frameworks.
That's just not true, and I heavily use Spring Boot at work and I'm testing Quarkus on my latest personal projects.
But I think it's important to remember what a framework is---and what it isn't.


## What a Framework Is

In any language, you've heard of libs and of frameworks.

To keep it short, **a library is some code written by another developer, that solves a common problem**, and that you can include in your project to save time---and hopefully some bugs because it was tested in many other cases than your own.
It should, of course, come with documentation so that you know what you can do and how you can do it.

<aside><p>A framework is a big, badass library, complete with documentation and a code of conduct.</p></aside>

A framework is the step beyond: it's a big, badass library, that solves many problems and saves you much time, but it's so big and badass that a mere documentation is not enough.
In addition, you need guidelines.

A framework isn't something you should use randomly.
It was designed to solve specific problems and, to do so efficiently, it should be used accordingly to the way it was conceived.
In using a framework, you agree to respect its code of conduct, resulting in a more streamlined development experience.


## Why Learning Only the Framework Is Not Enough

So, basically, **a framework or a library is something built _upon another technology_, offering you a new abstraction layer**.
It allows you to write less code, or write it faster, but it relies on something else.
If you don't know that something else, you may not understand the framework's limitations, or how to overcome them.

<aside><p>The framework won't solve <em>all</em> your project's problems.</p></aside>

More importantly, this something was built to solve one specific problem with some hypotheses.
I strongly suspect that your project won't fill all the conditions---and if it does, I'm sad for you, because exceptions are what makes our work challenging, and challenges are what makes it so enjoyable.

Finally, frameworks are a trap for your ego.
Everything seems easy at first: you follow a tutorial and you've got working solutions up in a few hours.
It's just amazing!
You're a wonderful developer and everything seems soooo easy!

<aside><p>Does knowing Angular make you a web developer?</p></aside>

So, do you feel confident saying you're a web developer because you're an Angular expert?
Will you be at ease with an HTML canvas or CSS3 animations?
Are you fully convinced you're an experimented Java developer because you've done some Spring Boot?

Let me explain with an image.
Let's say you need to move on a road and you study the framework "drive a car."
Your framework gives you some basic tools---so a basic car---and rules that help you driving it---the Driver's Manual.
You've learned to use this framework, so you got your driver's license.
Of course, there are things common to all the possible solutions based on the technology "drive"---all vehicles should respect the Driver's Manual.
However, if you confuse the framework "drive a car" with the technology "drive," you'll be expected to be able to repair and upgrade your car, drive it very fast, on an ice circuit, while remaining on two wheels, or even to drive other vehicles, like a truck or a motorbike.
Do you feel up to the challenge?


## A Real-Life Example

Here's an anecdote from the project I worked on last year.
We needed to implement a job that would synchronize about {{< numfmt 6000 >}} records daily, with a quite elaborate data model.
As always in a project, it was an emergency and we had little time to make it a reality.

No matter, Spring Data JPA is a great tool to add an abstraction between the DB and the code.
A member of our team took the problem in his hands and made something that answered the requirements.
He did some preliminary tests with the data sample we had and all went as expected.

A few days later, the client tested the new job against a data set that had greatly grown.
The job had to be stopped after three days of syncing, having imported fewer than a thousand records.
Needless to say, the client was anything but satisfied.

<aside><p>Experience makes the difference, but it doesn't come with the framework.</p></aside>

I reviewed my teammate's work.
That's when experience helped.
I realized he had done the best he could, but he had never faced the need to model or optimize a database himself, and wasn't really familiar with the use of JPA[^jpa] for that matter.
He did his best with what he had learned from the tutorial and solved some problems with StackOverflow.

[^jpa]: JPA: Java Persistence API. Read [this](https://en.wikipedia.org/wiki/Java_Persistence_API) if you don't know what I'm talking about.

Luckily for me, these are things I've had to do in the past.
So, I adapted his data model a bit---I moved a column or two, added an index and an integrity constraint---and reviewed the job structure so that it would process operations in batches rather than doing each insertion separately.
The result was immediate: **the job's execution time went down from more than 3 days to 3 minutes**.

So yes, the framework had allowed my teammate to accomplish his mission quickly, but with unsatisfying results due to a limited knowledge of how the underlying technology works.
That's why knowledge and experience are useful.
The framework won't teach you how it works behind the scenes, but you need to learn that.

Concerning this particular anecdote, I have some pieces of advice about using JPA:

- If anything above seems to be unclear, I suggest you study a little.
- You should design a data model, not only classes.
You must be able to represent your data model as an entity diagram _and_ your objects as a class diagrams.
Those schemas will look alike but won't be identical, and you should know and be able to explain the differences.
- Don't let your ORM[^orm] tool create your database for you.
Write an SQL script for this.
If you don't know how to do it, that means you need to learn more about the database management system.
- When you need to perform several operations on the database in a row, try to execute them as batch rather than separately.
If you don't, you'll be spending more time opening and closing connections than you'll be updating the data.

[^orm]: ORM: Object-relational mapping. Again, if you don't know what it is, have a read [here](https://en.wikipedia.org/wiki/Object-relational_mapping).


## Do You Know the Foundations?

If anything about the framework seems to be magic, you don't.
Whenever you have a doubt, ask yourself this question: **would I be able to do this myself, from scratch?**
Depending on your answer, you can estimate your level of knowledge.

- _Duh, why would I care doing this from scratch? I've got the framework doing it!_ &rightarrow; You've obviously not understood the point of this post. No matter, you'll understand sooner or later.
- _I know the theory of how it works._ &rightarrow; This is the very minimum, yes.
- _I've done a proof of concept._ &rightarrow; That's where things get interesting.
- _I've done it before/I'm the developer of the framework._ &rightarrow; So you know how things work and have a good vision of what you could enhance if need be.
- _I've used several approaches to solve similar problems._ &rightarrow; Good! So you know of alternatives to your solution. You've got nothing left to learn. Any tip for me?


## What You Should Do When Learning a Framework

First, learn the framework properly.
Building something quickly with it is not enough.
As with any other tool or technology, you need to do it properly.
For this reason, you need to **understand its philosophy** and respect it as much as possible.
If you go against it, you will encounter problems sooner or later---rather sooner.

So yes, you'll start with some tutorials to discover the basics and get the hang of it, but never assume they're enough---they're not.
You'll need to use some features that are not shown on the tutorials.
**You _will_ have to dig in the documentation**.

Sometimes, even the documentation won't be enough.
Or the framework won't do what the doc says.
You may ask questions forums won't answer.
Be prepared to dig into the framework's code, see how it is built.
You may discover and solve a bug.
In any case, you'll have learned something valuable.

Also be sure to understand the technology underneath your framework.
Make sure you know how it works behind the scenes.
Best practices are not only about the framework; they are about all the layers underneath.
And they are the best way to build solid, efficient solution.
I invite you to **build proofs of concept**---minimal projects making a basic demonstration of how your framework works.
That _will_ help you.
Trust me, you'll feel more comfortable about what you're doing.

By the way, never misunderstand the promise of a framework: it isn't here so that you can avoid getting into the depth of things, or to do the work in your stead.
It's only there to help you be efficient while you do things you should already know.

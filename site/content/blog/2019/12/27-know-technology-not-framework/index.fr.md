---
date: 2019-12-27T08:59:42+02:00
title: Maitrisez la technologie, pas le framework
#subtitle: Would you rather spend that money now or later?
slug: maitrisez-technologie-pas-framework
description: Il semble que les développeurs de nos jours apprennent des frameworks plutôt que des langages. J'aurais tendance à conseiller le contraire...
#banner:
author: chop
categories:
  - developpement
tags:
  - java
  - spring-boot
---

Cette année m'a donné l'occasion de discuter avec des personnes d'horizons différents.
Parmi celles-là, des responsables techniques, des encadrant·e·s techniques, des architectes...
Vous voyez l'idée.
Au cours de ces échanges, j'ai entendu une remarque récurrente, dont je craignais jusque-là qu'elle ne soit l'expression de ma propre nature exigeante.

Mais non ! D'autres l'ont remarqué et partagent mon avis.
Celui-ci se résume de la façon suivante : une nouvelle génération de développeur·e·s est arrivée sur le marché.
Ils aiment faire les choses rapidement et le fonctionnement profond des choses ne les intéresse pas.
Ceci se coordonne avec une époque où les frameworks fleurissent, nous promettant de développer mieux, plus vite et avec plein de magie qui nous évite de devoir comprendre ce qui se cache derrière ces astuces.

<!--more-->


## Le constat

Les postes semblent de plus en plus spécialisés.
Quand j'ai rejoint le monde professionnel, il était courant de voir un·e développeur·e réaliser l'analyse de ce qu'il allait programmer par la suite.
De nos jours, il est plus courant de se reposer sur une équipe d'analystes métier qui ne connaissent pratiquement rien aux aspects techniques, mais sont responsables de tous les aspects précédant la réalisation.
Dans certains cas, c'est justifié et cela apporte une nouvelle valeur ajoutée, mais ce n'est pas l'objet de ce billet.
Dans le cas du développement, je reste dubitatif.

Je ne crois pas aux codeur·e·s spécialisé·e·s.
Je ne pense pas qu'avoir des développeur·e·s versé·e·s uniquement dans le développement _front_ et d'autres qui ne font que du _back_ soit la meilleure façon de faire avancer un projet.
Il me semble d'ailleurs qu'au sein d'une équipe agile, on s'attend à ce que chaque membre soit capable de réaliser n'importe quelle histoire.
Bien entendu, chaque individu a ses propres forces, mais selon moi, chaque créateur logiciel devrait pouvoir intervenir sur n'importe quelle couche de la solution dont il participe à la réalisation.

<aside><p>Certains développeur·e·s apprennent un framework plutôt qu'un langage.</p></aside>

Mais maintenant, non contents de se spécialiser sur un seul aspect --- d'accord, nous avons tous nos bêtes noires --- les développeurs commencent à apprendre des frameworks plutôt que des langages.
Qui pourrait leur en vouloir ?
Les offres d'emploi demandent des spécialistes Angular, React, Spring Boot...
Oui, les entreprises ont leur part de responsabilité.

Si vous m'avez lu jusqu'ici, vous devez vous dire que je suis un réac' qui hait les frameworks.
Non, ce n'est pas le cas.
J'utilise énormément Spring Boot professionnellement et je fais des essais avec Quarkus pour mes derniers projets personnels.
Je veux en revanche rappeler ce qu'est un framework --- et ce que ce n'est pas.


## Ce qu'est un framework

Dans tous les langages informatiques, vous avez certainement entendu parler de librairies et de frameworks --- que les plus puristes de la langue française appelleront « bibliothèques » et « cadriciels ».

Pour faire court, **une bibliothèque est un ensemble de code écrit par un autre développeur, qui résout un problème habituel**, et que vous pouvez inclure dans votre projet pour gagner du temps --- et, espérons-le, vous épargner quelques bugs car elle a été testée dans beaucoup d'autres cas que le vôtre.
Elle doit bien entendu être accompagnée de sa documentation afin que vous sachiez ce que vous pouvez faire et comment vous devez le faire.

<aside><p>Un framework est une grosse bibliothèque avec de la gueule, avec une grosse documentation et un code de conduite.</p></aside>

Le framework est l'étape suivante : c'est une grosse bibliothèque plutôt badass, qui résout de nombreux problèmes et vous fait gagner beaucoup de temps.
Malheureusement, elle est si grosse et si badass qu'une simple documentation ne fait plus l'affaire.
Il vous faut maintenant des lignes directrices.

Un cadriciel n'est pas quelque chose que l'on utilise au petit bonheur la chance.
Il a été conçu pour résoudre des problèmes spécifiques d'une façon précise.
Pour l'utiliser efficacement, vous devez connaitre cette façon et vous y plier.
En choisissant un framework, vous acceptez implicitement de respecter son code de conduite, ce qui aura entre autres l'avantage de normaliser vos méthodes de développement.


## Pourquoi on ne peut pas se contenter d'apprendre le framework

Si on reprend, fondamentalement, **un framework ou une bibliothèque est un outil construit _sur base d'une autre technologie_, facilitant votre travail par le biais d'une couche d'abstraction supplémentaire**.
En l'utilisant, vous réduisez la quantité de code ou le temps nécessaire à son écriture, mais cela s'appuie sur autre chose.
Si vous ne connaissez pas cet autre chose, vous ne saurez peut-être pas les limites de votre framework ni comment les contourner.

<aside><p>Un framework ne résoudra pas <em>tous</em> les problèmes de votre projet.</p></aside>

Un autre point à ne pas perdre de vue est que cet outil a été construit pour résoudre un problème spécifique, en prenant des hypothèses.
Je suspecte que votre projet ne cochera pas toutes les cases --- et je vous plains si c'est le cas, car ce sont ces exceptions qui créent les défis et les défis qui rendent notre travail si plaisant.

Enfin, les frameworks sont un piège pour votre égo.
Tout semble simple au début : vous suivez un tutoriel et vous avez des solutions de travail en quelques heures.
C'est merveilleux !
Vous êtes un développeur hors du commun et tout vous semble tellement facile !

<aside><p>Connaître Angular fait-il de vous un·e développeur·e web ?</p></aside>

Alors, êtes-vous confiant lorsque vous vous déclarez développeur web après quelques semaines à vous familiariser avec Angular ?
Serez-vous à l'aise lorsque vous devrez jouer avec un canvas HTML ou des animations CSS3 ?
Êtes-vous pleinement convaincu d'être un développeur Java expérimenté après un ou deux projets Spring Boot ?

Permettez-moi une image.
Imaginons que vous devez vous déplacer sur une route.
Pour cela, vous comptez utiliser le framework « conduire une voiture ».
Le cadriciel vous donne quelques outils de base --- une petite citadine --- et des règles pour vous guider sur la façon correcte de les utiliser --- le code de la route.
Vous avez appris à utiliser ce framework et avez donc obtenu votre permis de conduire.
Bien sûr, certains éléments sont communs à toutes les solutions basées sur la technologie « conduire » --- tous les conducteurs doivent respecter le code de la route.
Cependant, si vous confondez le framework « conduire une voiture » avec la technologie « conduire », on attendra de vous la capacité de réparer et améliorer votre voiture, de la conduire à grande vitesse, sur un circuit verglacé, tout en restant sur deux roues, ou même de conduire d'autres véhicules, comme un camion ou une moto.
Le défi vous semble-t-il adapté ?


## Un exemple personnel

L'an dernier, nous devons réaliser pour mon projet un job qui synchroniserait quotidiennement environ {{< numfmt 6000 >}} enregistrements, avec un modèle de données assez élaboré.
Comme dans tous les projets, la tâche était urgente et nous avions peu de temps pour en faire une réalité.

Qu'importe ! Spring Data JPA est un excellent outil qui nous offre une abstraction entre la base de données et le code.
La mise en œuvre serait simple.
Un membre de l'équipe s'est donc chargé du problème et créé une solution répondant aux exigences fonctionnelles.
Il a réalisé quelques tests sur base de notre échantillon de données et tout était au vert.

Quelques jours plus tard, c'est le client qui a testé cette solution.
L'échantillon de données avait mangé beaucoup de soupe.
L'exécution a été interrompue après trois jours.
Moins d'un millier d'enregistrements avait été importé.
Inutile de préciser que le client était tout sauf satisfait.

<aside><p>L'expérience fait la différence, mais elle n'est pas livrée avec le framework.</p></aside>

J'ai passé en revue le travail de mon coéquipier.
C'est là que l'expérience a aidé.
J'ai réalisé qu'il avait fait de son mieux, mais il n'avait jamais eu à modéliser ou optimiser une base de données lui-même, et il n'était pas vraiment familier avec l'utilisation de JPA[^jpa] d'ailleurs.
Il a fait de son mieux avec ce qu'il avait appris du didacticiel et a résolu certains problèmes avec StackOverflow.

[^jpa]: JPA : Java Persistence API. Lisez [ceci](https://fr.wikipedia.org/wiki/Java_Persistence_API) si vous ne savez pas de quoi je parle.

Heureusement pour moi, ce sont des choses que j'ai dû faire dans le passé.
J'ai donc adapté un peu son modèle de données --- une colonne ou deux qui changent de table, un index et une contrainte d'intégrité en plus --- et j'ai revu la structure du job afin qu'il traite les opérations par lots plutôt que de faire chaque insertion séparément.
Le résultat a été immédiat : **le temps d'exécution du job est passé de plus de 3 jours à 3 minutes**.

Alors oui, le framework avait permis à mon coéquipier de développer accomplir sa mission rapidement, mais avec des résultats insatisfaisants en raison d'une méconnaissance de la technologie sous-jacente.
C'est pourquoi j'insiste sur les connaissances et l'expérience.
Le cadriciel ne vous expliquera pas comment il fonctionne en arrière-plan, mais c'est un effort d'apprentissage que vous devez fournir.

Concernant cette anecdote, j'ai quelques conseils sur l'utilisation de JPA :

- Si quelque chose dans cette histoire vous semble chinois, je vous suggère d'étudier un peu.
- Vous devriez concevoir un modèle de données, pas seulement des classes.
Vous devez pouvoir représenter votre modèle de données sous forme de diagramme d'entité _et_ vos objets sous forme de diagrammes de classes.
Ces schémas se ressembleront, mais ne seront pas identiques, et vous devriez connaitre et pouvoir expliquer les différences.
- Ne laissez pas votre outil ORM[^orm] créer votre base de données à votre place.
Écrivez un script SQL pour cela.
Si vous ne savez pas comment le faire, cela signifie que vous devez en apprendre davantage savoir plus sur votre système de gestion de base de données.
- Lorsque vous devez effectuer plusieurs opérations consécutives sur la base de données, essayez de les exécuter par lot plutôt que séparément.
Sans ça, vous passerez plus de temps à ouvrir et fermer des connexions qu'à mettre à jour les données.

[^orm]: ORM : Object-relational mapping. Encore une fois, si vous ne savez pas ce que c'est, lisez [ceci] (https://fr.wikipedia.org/wiki/Mapping_objet-relationnel).


## Connaissez-vous les bases ?

Si quelque chose au sujet du framework semble être magique, la réponse est non.
Chaque fois que vous avez un doute, posez-vous cette question : **pourrais-je le faire moi-même, en partant de zéro ?**
En fonction de votre réponse, vous pouvez estimer votre niveau de connaissance.

- _Et pourquoi voudrais-je faire ça à partir de zéro ? J'ai le framework qui le fait !_ &rightarrow; Vous n'avez manifestement pas compris l'objet de ce post. Tant pis. Vous le comprendrez tôt ou tard.
- _Je connais la théorie._ &rightarrow; Vous avez atteint le minimum syndical.
- _J'ai fait une preuve de concept._ &rightarrow; C'est là que les choses deviennent intéressantes.
- _Je l'ai déjà fait/je suis le développeur du framework._ &rightarrow; Vous savez donc comment les choses fonctionnent et avez une bonne vision des limitations et des contournements envisageables.
- _J'ai utilisé plusieurs approches pour résoudre des problèmes similaires._ &rightarrow; Bien ! Vous connaissez donc des solutions alternatives. Vous êtes au sommet de la pyramide du savoir. Des astuces pour moi ?


## Que faire lorsque vous apprenez un framework

Tout d'abord, apprenez-le bien.
Construire quelque chose rapidement ne suffit pas.
Comme pour tout autre outil ou technologie, vous devez le faire correctement.
Pour cette raison, il est nécessaire de **comprendre sa philosophie** et la respecter autant que possible.
Si vous allez à son encontre, vous rencontrerez des problèmes tôt ou tard --- généralement plutôt tôt.

Alors oui, vous commencerez certainement par quelques tutoriels simples pour découvrir les bases et vous familiariser, mais souvenez-vous que ce ne sera pas suffisant.
Vous devrez utiliser des fonctionnalités que les didacticiels ne couvrent pas.
**Vous _devrez_ fouiller dans la documentation**.

Dans certains cas, même la documentation ne sera pas suffisante.
Ou le cadriciel ne se comportera pas comme l'indique la doc.
Vous pourrez poser des questions auxquelles les forums ne pourront pas répondre.
Soyez prêts à fouiller dans le code du framework, voir par vous-même comment il est construit.
Vous pourriez découvrir et résoudre un bogue.
Dans tous les cas, vous aurez appris quelque chose.

Assurez-vous également de comprendre la technologie sous votre framework.
Sachez comment cela fonctionne en arrière-plan.
Les bonnes pratiques ne concernent pas seulement le cadriciel ; elles concernent toutes les couches en dessous.
Et elles sont le meilleur moyen de construire une solution solide et efficace.
Je vous invite à **construire des preuves de concept** --- des projets minimaux faisant une démonstration de base du fonctionnement de votre framework.
_Cela vous aidera_ et vous vous sentirez plus à l'aise dans ce que vous faites.

Soit dit en passant, ne vous méprenez jamais sur la promesse d'un framework : il n'est pas là pour que vous puissiez éviter d'entrer dans le fond des choses ou pour faire le travail à votre place.
Il n'est là que pour vous aider à être efficace en vous évitant de refaire des choses que vous devriez déjà savoir faire.


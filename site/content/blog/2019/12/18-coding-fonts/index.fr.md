---
date: 2019-12-18T18:36:43+02:00
title: Choisir une police pour coder
#subtitle: Would you rather spend that money now or later?
slug: police-ecriture-pour-codage
description: Les développeurs doivent regarder des caractères toute la journée. Autant choisir une police d'écriture agréable et efficace.
banner:
  src: marcus-depaula-tk7OAxsXNL0-unsplash.jpg
  alt: Caractères d'imprimerie appartenant à des fontes variées.
  by: Marcus dePaula
  link: https://unsplash.com/photos/tk7OAxsXNL0
  license: Unsplash
author: chop
categories:
  - developpement
tags:
  - typographie
  - java
---

Les développeur·e·s font généralement très attention aux outils qu'elles ou ils choisissent : [leur ordinateur]({{< relref path="blog/2019/11/11-cost-of-time" >}}), leur éditeur...
Pourtant, nous ne nous posons que rarement la question de savoir si la police d'écriture proposée par défaut est adaptée.

Voici un peu de matière à réflexion sur ce sujet, et quelques-unes de mes fontes préférées pour le développement.

<!--more-->

## L'importance de la fonte dans le codage

Plutôt que de nous lancer dans de longs débats, je vous propose un exemple où une police inadaptée pourrait vous ralentir dans le pistage d'un bug.
L'exemple est en Java.

```java
System.out.println(2147483647 + 101); // Imprime "-2147483548"
System.out.println(2147483647 + 10l); // Imprime "2147483657"
```

Ces deux lignes ne retournent pas le même nombre.
Contre toute attente, la première ligne imprime un nombre _négatif_.

Si vous collez ce code dans votre éditeur et êtes incapables de voir la différence immédiatement, la police que vous utilisez n'est pas la plus adaptée à votre travail.

Je vais prendre une minute pour expliquer la solution de cet exemple.
Si vous avez immédiatement compris ou n'êtes pas intéressé, je vous invite à passer directement à [la suite](#criteres-selection-police).

Reprenons : qu'est-ce que {{< numfmt 2147483647 >}} ?
<!-- XXX use KaTeX to pint the equation -->
C'est la valeur de 2<sup>31</sup> − 1, le plus grand entier pouvant être stocké dans un `int` en Java (Java utilise 32 bits pour ce type, mais le premier est réservé pour le signe).

Dans les deux cas, nous ajoutons un nombre à cet `int`, mais le résultat est différent.
Dans le premier cas, la somme est négative ({{< numfmt -2147483548 >}}) alors que, dans le second, elle est supérieure au nombre initial ({{< numfmt 2147483657 >}}).
La différence tient bien évidemment au nombre que nous ajoutons.

Sur la première ligne, nous ajoutons 101 (avec le chiffre 1 à la fin).
Ceci cause un débordement numérique (« _numeric overflow_ ») : le résultat est supérieur à ce que peut contenir un `int`.
Le bit le plus à gauche change de valeur, mais, malheureusement pour nous, il s'agit du bit de signe.
En conséquence, le résultat est interprété comme un nombre négatif lorsque la JVM doit l'afficher.

Sur la seconde ligne, on ajoute 10, suivi par un L minuscule[^l-convention] pour déclarer que ce nombre est de type `long`.
Par inférence, Java en déduit que la somme des deux nombres est également un `long`.
Le résultat bénéficie par conséquent de davantage de bits pour stocker sa valeur, et le débordement numérique est évité.

[^l-convention]: C'est notamment pour faciliter la lecture et éviter ce type d'erreur que les conventions Java recommandent habituellement d'utiliser un L majuscule.

Et voici le premier critère pour choisir une police d'écriture pour coder : tous les caractères doivent être facilement reconnaissables.


## Quelques critères pour choisir une police adaptée

Comme nous l'avons vu, certains problèmes de débogage découlent essentiellement de difficultés de lecture.
Nous utilisons de nombreux glyphes, dont certains peuvent se ressembler, et nous devons être capables de les distinguer aisément.
C'est d'autant plus vrai dans la programmation où, contrairement à la littérature, notre cerveau ne peut pas toujours facilement déduire les corrections à partir du contexte.

Selon moi, si vous cherchez une police pour coder, vous devriez au moins prendre en compte les critères suivants :

* Il vous faut une **police à chasse fixe** (« _monospaced_ » : tous les glyphes occupent la même largeur).
Cela semble évident puisqu'à peu près tous les outils manipulant du code (éditeurs, terminaux...) utilisent de telles fontes.
Mais il est toujours bon de rappeler ce que l'on pense su de tous.
C'est la première chose que l'on oublie sans ça.
Une police à chasse fixe sera d'une grande assistance lorsque vous chercherez une erreur dans de longs extraits de code.
Et puis, pouvez-vous imaginer l'indentation avec une fonte à chasse variable ?
Pour peu qu'espaces et tabulations soient mélangées[^espace-feminin][^mix-indentation], cela devient rapidement un enfer !

* Il est bien évident que **les glyphes ambigus sont à bannir**.
Certains glyphes sont connus pour leurs similitudes.
Pour de la lecture, notre cerveau est très doué pour filtrer et déduire.
Pour le code cependant, c'est une tout autre histoire et ces filtres peuvent rapidement devenir un obstacle plutôt qu'une aide (« Plein de chiffres, donc c'est un nombre. Je ne vois pas les lettres qui se sont camouflées au milieu. »)
C'est pourquoi votre fonte devrait distinguer clairement les glyphes pouvant se ressembler, notamment :
  * le chiffre _1_, le _L_ minuscule et le _i_ majuscule (`1`, `l`, `I`) ;
  * le chiffre _0_ et le _o_ majuscule (`0`, `O`) ;
  * plus rare mais possible, le chiffre _5_, le _s_ majuscule et le symbole du dollar (`5`, `S`, `$`).

* **Évitez les empattements** (sérifs)[^serif-definition].
Je me souviens avoir déjà lu que les fontes avec empattements étaient plus adaptées au papier tandis que les fontes sans (fontes linéales) étaient idéales pour les écrans[^serif-meilleur-sur-papier].
Comme toujours, ne tenez pas tout ce que dit Internet pour acquis : ceci est un critère plus subjectif.
Je préfère personnellement les polices légères.
Pour cette raison, exit _Courier New_ !

* **La police doit vous plaire**.
Non, vraiment ! Ce critère est complètement objectif.
Si vous êtes un développeur professionnel ou passionné, vous allez passer des heures face à ces glyphes.
Vous avez plutôt intérêt à être sûr·e· qu'ils ne vous sont pas désagréables, sans quoi votre boulot/passion va vite se changer en enfer.

[^espace-feminin]: Non, je ne me suis pas trompé dans l'accord de mon adjectif. [Espace est féminin](https://www.cnrtl.fr/definition/espace) lorsqu'il désigne le caractère.
[^mix-indentation]: Personne n'oserait mélanger espaces et tabulations, n'est-ce pas ? Hormis les éditeurs qui n'en font qu'à leur tête...
[^serif-definition]: Si vous ignorez ce qu'est l'empattement, [cet article (en anglais)](https://www.freecodecamp.org/news/how-typography-determines-readability-serif-vs-sans-serif-and-how-to-combine-fonts-629a51ad8cce/) résume bien et donne quelques conseils que je n'ai pas suivis lors de la conception de la pile de fontes de ce site.
[^serif-better-on-paper]: [Cet article](https://www.insidermonkey.com/blog/7-easiest-fonts-to-read-on-screen-and-paper-520113/?singlepage=1) confirme cette tendance, bien que l'on constate des exceptions dans les deux sens.


## Ma sélection

OK, j'ai un peu réfléchi aux fontes, j'en ai essayé quelques-unes et j'ai fait ma propre sélection.
Je vous propose mes favorites ci-dessous, par ordre de préférence descendante.

Pour l'anecdote, il s'agit de la pile de fontes utilisée pour afficher des extraits de code sur ce site.
Cela signifie que, si l'une de ces fontes est installée sur votre poste, elle sera utilisée pour afficher le code.


### Fira Code

Fira Code répond à l'ensemble des critères que j'ai proposé plus haut.
Elle tente également de répondre à un problème que les développeurs ne sont pas conscients d'avoir.
Traduit de la documentation de la police :

> Les développeurs utilisent de nombreux symboles, souvent encodés à l'aide de plusieurs caractères.
> Pour le cerveau humain, des séquences comme `->`, `<=` ou `:=` sont des éléments logiques simples, même s'ils sont constitués de deux ou trois caractères à l'écran.
> Votre œil passe une énergie non nulle à lire, interpréter et joindre plusieurs caractères en un élément logique unique.
> Idéalement, tous les langages de programmation devraient être conçus avec des symboles Unicode comme opérateurs, mais ce n'est pas encore le cas.

Pour répondre à cette problématique, l'auteur est parti de  [Fira Mono](https://mozilla.github.io/Fira/), proposée par Mozilla, et a inclus un ensemble de [ligatures](https://fr.wikipedia.org/wiki/Ligature_(%C3%A9criture)) pour les opérateurs de programmation les plus courants.
Dans les éditeurs supportant les ligatures, ces séquences sont ainsi remplacées par des caractères uniques.

{{< figure src="fira-code.png" alt="Exemple de code avec Fira Code" >}}

C'est joli.
Je ne suis pas certain que l'interprétation soit plus aisée pour les développeurs habitué·e·s aux suites de caractères, mais cette fonctionnalité peut se désactiver dans la plupart des éditeurs.

Un autre détail sympathique de cette fonte est la présence de [variantes stylistiques](https://github.com/tonsky/FiraCode#stylistic-sets).
Par exemple, il est possible de choisir entre deux styles pour le symbole du dollar, le « et » commercial, l'arobase, le chiffre 0...
Toutes ces variantes sont reprises ci-dessous.

{{< figure src="https://raw.githubusercontent.com/tonsky/FiraCode/master/showcases/stylistic_sets.png" alt="Visual preview of available stylistic sets" link="https://github.com/tonksy/FiraCode" >}}

Website
: https://github.com/tonsky/FiraCode

Repo
: https://github.com/tonsky/FiraCode

License
: [SIL Open Font License v1.1](https://scripts.sil.org/ofl)

Documentation
: [Conseils d'installation](https://github.com/tonsky/FiraCode/wiki) et [configuration des variantes stylistiques](https://github.com/tonsky/FiraCode/wiki/How-to-enable-stylistic-sets)


### Monoid

Monoid est une autre fonte avec des ligatures que j'ai découverte il y a quelques années.
Elle est différente, car l'auteur a choisi un style semi-condensé, ce qui donne une impression de...
Laissons l'exemple parler pour lui-même.

{{< figure src="monoid.png" alt="Exemple de code avec Monoid" >}}

Comme pour Fira Code, des variantes stylistiques sont disponibles, et même plus nombreuses, bien que la documentation soit très maigre.

J'ai découvert cette police en 2015.
Vers la même époque, j'ai également découvert les polices d'icônes.
L'auteur a eu l'idée de créer des ligatures pour chaque icône de Font Awesome, donnant naissance à [Monoisome](https://github.com/larsenwork/monoid/tree/master/Monoisome).
J'ignore quelle version de la fonte iconique est utilisée, la variante de Monoid n'ayant pas été mise à jour depuis 2018.

Website
: https://larsenwork.com/monoid/

Repo
: https://github.com/larsenwork/monoid

License
: [MIT License](https://opensource.org/licenses/MIT) et [SIL Open Font License v1.1](https://scripts.sil.org/ofl) 

Documentation
: [README](https://github.com/larsenwork/monoid#readme)


### Ubuntu Mono

Ceci sera la dernière police que je vous proposerai.
Il y a au moins un aspect positif à travailler sur une nouvelle installation Ubuntu : à chaque fois qu'une fonte à chasse fixe est requise, c'est Ubuntu Mono qui est utilisée.

Cette police n'a pas de fonctionnalité élaborée ou de ligatures, mais elle est légère et propre, et correspond à tous mes critères de sélection, ainsi que vous pouvez le constater dans l'exemple ci-dessous.

{{< figure src="ubuntu-mono.png" alt="Exemple de code avec Ubuntu Mono" >}}

Ubuntu Mono est une superbe solution si vous cherchez quelque chose de simple et efficace !

Website
: https://design.ubuntu.com/font/

License
: [Ubuntu font license v1.0](https://ubuntu.com/legal/font-licence)

Documentation
: [Documentation wiki](wiki.ubuntu.com/Ubuntu_Font_Family)

---
date: 2019-12-18T18:36:43+02:00
title: Choose Your Font for Coding
#subtitle: Would you rather spend that money now or later?
slug: coding-fonts
description: As a programmer, you're probably looking at characters all day long. Let's look for a font that prints them in a pleasant way.
banner:
  src: marcus-depaula-tk7OAxsXNL0-unsplash.jpg
  alt: Metal block letters from various fonts.
  by: Marcus dePaula
  link: https://unsplash.com/photos/tk7OAxsXNL0
  license: Unsplash
author: chop
categories:
  - programming
tags:
  - typography
  - java
---

We developers often spend a great deal of attention choosing our tools: [computer]({{< relref path="blog/2019/11/11-cost-of-time" >}}), editors...
Yet, we often fail to see the gain of choosing an appropriate font.

Here is some food for thoughts on this topic, and some of my favorite coding fonts.

<!--more-->

## Why a Good Font Matters When Coding

Rather than long debates, let me give you one example where the wrong font may hinder you from finding the cause of a bug (example is in Java):

```java
System.out.println(2147483647 + 101); // Prints "-2147483548"
System.out.println(2147483647 + 10l); // Prints "2147483657"
```

Those two lines don't output the same number.
The first line actually outputs a _negative_ number.

Now, if you paste this in your IDE and can't spot the difference right away, the font you're using may not be the best for your job.

I'll give here the explanation of this brain teaser.
If you spotted it immediately or are just uninterested, just skip to [the next part](#font-selection-criteria).

So, what's {{< numfmt 2147483647 >}}?
<!-- TODO use KaTeX to print the equation -->
It's 2<sup>31</sup> − 1, the largest number that can be stored in a Java `int` (Java's `int` uses 32 bits, but the first one is for sign).

In both cases, we add a number to this `int`, but in the first case, it becomes negative ({{< numfmt -2147483548 >}}), while in the second, it becomes a higher number ({{< numfmt 2147483657 >}}).
The difference is, of course, the number we add.

On the first line, we add 101 (ending with the digit 1), causing a numeric overflow: the sum is higher than what the `int` can contain.
The leftmost bit is changed, but that's the sign bit.
As a consequence, the JVM incorrectly interprets the result as a negative number when printed.

On the second line, we add 10, followed by a minus L[^l-convention] to change this number's type to `long`.
Java then deduces that the sum of the two numbers is a `long` too, thus benefitting of more bits to store its value and avoiding the numeric overflow error.

[^l-convention]: Yes, conventions often recommend using a capital L to avoid this issue.

And that's a criterion for choosing a good font for coding: all characters should be easily recognizable.


## What You Should Look for in a Font When Coding {#font-selection-criteria}

As you've seen, some debugging problems come from reading difficulties.
We use several symbols that may look like each other, and we need to distinguish them easily.
This is especially important when coding since, unlike with literary language, our brain can't always deduce easy corrections from context.

In my opinion, you should at least ensure these criteria when looking for a coding font:

* It should be **monospaced**, which means all glyphs should have the same width.
That sounds obvious since almost anything code-based (editors, terminals...) uses such a font.
Yet, remembering the basics is always useful.
A monospaced font will help you when you're looking for an error in long pieces of code.
Plus, can you imagine indentation in a variable-width font?
If tabs and spaces are mixed, you're on the highway to hell![^indentation-mix]

* It should obviously **avoid ambiguous symbols**.
Some characters are known to be easily confused for other.
When it comes to reading, our brain is quite powerful at filtering and deducing.
For code, however, that's a whole other story and those biases can become an obstacle rather than help ("Many digits, so that's a number. I won't see the letters that hide among them.").
It's why your font should help you disambiguate similar-looking characters, such as:
  * the digit _1_, the minus _L_ and the capital _i_ (`1`, `l`, `I`);
  * the digit _0_ and the capital _o_ (`0`, `O`);
  * rarer but possible, the digit _5_, the capital _s_ and the dollar sign (`5`, `S`, `$`).

* **Avoid serifs**[^serif-definition].
I once read that fonts with serifs are great for paper, but sans serif fonts are better for screens[^serif-better-on-paper].
As always, don't trust everything you see on the Internet, as this criterion is more of a personal preference.
I personally like clean, not overloaded fonts (or design more generally, for that matter).
So, goodbye _Courier New_!

* **You should like it**.
No, really, that's an objective criterion.
If you're a professional or passionate coder, you'll spend hours confronted with this font.
You'd better make sure it doesn't get under your skin or your job/passion will just become a bit more hellish.

[^indetation-mix]: But nobody except your dumb editor would dare mixing tabs and spaces, right?
[^serif-definition]: If you don't know what serifs are, [this article](https://www.freecodecamp.org/news/how-typography-determines-readability-serif-vs-sans-serif-and-how-to-combine-fonts-629a51ad8cce/) is great and gives advice I didn't follow when designing this website's font stack.
[^serif-better-on-paper]: [This article](https://www.insidermonkey.com/blog/7-easiest-fonts-to-read-on-screen-and-paper-520113/?singlepage=1) confirms this, not as a general truth, but as a trend with its exceptions.


## My Selection

OK, I've given fonts a bit of thought and I've made my selection.
I'll give my favorites below, by order of decreasing preference.

As a matter of fact, this is the font stack I chose for displaying code excerpts on this website.
This means that, if one of these fonts is installed on your computer, your browser will use it to display code.


### Fira Code

Fira Code matches all the supplied criteria above.
It also tries to answer a problem coders don't know they have. From their documentation:

> Programmers use a lot of symbols, often encoded with several characters.
> For the human brain, sequences like `->`, `<=` or `:=` are single logical tokens, even if they take two or three characters on the screen.
> Your eye spends a non-zero amount of energy to scan, parse and join multiple characters into a single logical one.
> Ideally, all programming languages should be designed with full-fledged Unicode symbols for operators, but that’s not the case yet. 

So, as a solution, the author took [Fira Mono](https://mozilla.github.io/Fira/), developed by Mozilla, and included a set of [ligatures](https://en.wikipedia.org/wiki/Orthographic_ligature) for the most common programming combination.
In editors supporting ligatures, those sequences are thus replaced with single characters.

{{< figure src="fira-code.png" alt="Example of code using Fira Code" >}}

That looks nice.
I don't know if that's easier to parse than characters sequences when you're used to the usual way of writing and reading it.
If you don't like it, you can still deactivate the ligatures and yet use the font.

Another nice feature of this font is that it provides some [stylistic variants](https://github.com/tonsky/FiraCode#stylistic-sets).
For instance, you can customize the way you display the dollar sign, the ampersand, the "at" sign, whether the 0 is barred or dotted...
All these variants are represented below.

{{< figure src="https://raw.githubusercontent.com/tonsky/FiraCode/master/showcases/stylistic_sets.png" alt="Visual preview of available stylistic sets" link="https://github.com/tonksy/FiraCode" >}}

Website
: https://github.com/tonsky/FiraCode

Repo
: https://github.com/tonsky/FiraCode

License
: [SIL Open Font License v1.1](https://scripts.sil.org/ofl)

Documentation
: [Installation instructions](https://github.com/tonsky/FiraCode/wiki) and [stylistic sets enabling instructions](https://github.com/tonsky/FiraCode/wiki/How-to-enable-stylistic-sets)


### Monoid

Monoid is another coding font with ligatures that I discovered some years ago.
It's a bit different from Fira Code because it's semi-condensed and that's...
Well, let's the example speak for itself.

{{< figure src="monoid.png" alt="Example of code using Monoid" >}}

Like with Fira Code, stylistic alternates are available, offering even more choice, though the documentation is scarcer.

I discovered this font in 2015.
Around that time, I'd also discovered iconic fonts such as Font Awesome.
The author had the idea of creating ligatures for each Font Awesome icon.
Thus was born [Monoisome](https://github.com/larsenwork/monoid/tree/master/Monoisome).
I don't know which version of Font Awesome was used, though, since this variant of Monoid has not been updated since 2018.

Website
: https://larsenwork.com/monoid/

Repo
: https://github.com/larsenwork/monoid

License
: [MIT License](https://opensource.org/licenses/MIT) and [SIL Open Font License v1.1](https://scripts.sil.org/ofl) 

Documentation
: [README](https://github.com/larsenwork/monoid#readme)


### Ubuntu Mono

This will be the last font I'll propose you.
I love at least one thing about working on a fresh install of Ubuntu: each time you expect a monospaced font, Ubuntu Mono is the one.

It does not come with elaborate ligatures or other features, but it's clean and it matches all my selection criteria, as you can see in the example below.

{{< figure src="ubuntu-mono.png" alt="Example of code using Ubuntu Mono" >}}

Ubuntu Mono's a great go-to solution if you're looking for something simple and efficient!

Website
: https://design.ubuntu.com/font/

License
: [Ubuntu font license v1.0](https://ubuntu.com/legal/font-licence)

Documentation
: [Wiki documentation](wiki.ubuntu.com/Ubuntu_Font_Family)

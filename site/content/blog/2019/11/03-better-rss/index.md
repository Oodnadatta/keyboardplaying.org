---
date: 2019-11-03T21:10:05+02:00
title: Making the RSS Experience better on KP
subtitle: One news aggregator to rule them all
slug: better-rss-experience
description: Some find RSS are old school. Those who still use it often face the frustration of having to open the website to get the full content. _Keyboard Playing_ won't impose this constraint.
author: chop
categories:
  - news
tags:
  - ux
---

Many people I know have abandonned RSS and Atom feeds and prefer to use social network to keep up-to-date.
For those who haven't, I know no system is perfect but I can think of at least one frequent irritation I don't want to reproduce on this site.

<!--more-->

## The Thing I Hate in RSS Feeds

I still use an RSS agregator to follow the news and some blogs.
I love being able to sort my feeds in categories, see what I haven't read yet, and keep it for later or discard it because the title is enough to tell I won't be interested.

What I don't love, which has become common practice especially in the press, is reading an enticing title and open the entry, only to see that it only contains the first three lines or a summary.
True, it might further tell me if the news is of interest to me, but it also means that, to get the full content, I have to click and open the website.


## What's to Hate with This

<aside><p>Being unable to read the full article from the news agregator disrupts user experience.</p></aside>

What I don't like in that can summarized in the following sentence: it disrupts user experience.

If you are an editor, put yourself in your reader's shoes: they access everything in their news application or agregator.
Then they want to read one of your articles and they have to open your website.

First disruption: they have to change application---though most news apps will embed the browser---or website.

This may seem like nothing to you, but the visual identity and information organization are differente.
That's something the brain has to process in order to adapt, even though we're often not aware of it.

Moreover, you'll often make them load a plethora of elements they don't need or want: a Disqus or comments feed, advertisements, some heavy, non-optimized CSS and page, images ...
Not only are those elements unwanted, but they imply a lag in getting the information.
That means you disrupt the experience from a temporal point of view, too.

Furthermore, if your user is in an area where they don't have access to a data network, they won't be able to get your content, though their news application may have cached the content of the RSS feed.
All they can now access are ~~teasers~~ summaries.
Too bad!

Some readers may just stop following you for that.
I know I already have.
Some websites made the change from "full articles" to "summaries," and their content was not satisfying enough for me to wait on all this crap to load in my browser and consume bandwidth uselessly.

As far as I'm concerned, I made the choice of paying subscription fees that allow press sites to produce quality content without the pollution.


## The Rationale Behind This Choice

I'm no expert of this field, but I know of at least two reasons why editors want people to open their websites.

First and foremost, they make money through advertisements.
Some of them live off it.
Ads don't appear in RSS feeds, or at least not in a measurable way, and therefore cannot be made profitable, so that's obviously a no-go if this revenue source is an important part of your business model.

Another important reason for forcing readers to come to the site is that an editor wants to be able to produce figures about their site.
They need to know how many visitors they have, how many of them are faithful/returning visitors, what pages attracts the most people and so on.
The cause for this need is often the same as previously: how do I make more money from it?


## Why You Won't Have to Suffer This

These rationales don't apply to _Keyboard Playing_.
It doesn't make any revenue.
The first purpose of this site is to share, and sharing won't be efficient if it's a constraint for the reader.

You may already have noticed there are no ads on this site and there's no plan for it in the future, so this reason to force you to get out of your news app isn't relevant.

As for getting to know what interests you the most, well ...
I'll have statistics one day, and it'll be missing those of you who, like me, prefer to read all their news on the agregator, but I'll make do with that.
There are other ways to tell what's interesting to the readers. :)

And now that making the reading experience comfortable[^explanation-why-work] has been taken care of, I will start focusing on some real content ...

[^explanation-why-work]: I had to make some development, as this was not the default behavior of this blogging stack, but maybe more on that topic later.

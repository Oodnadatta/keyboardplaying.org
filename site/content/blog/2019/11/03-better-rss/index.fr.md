---
date: 2019-11-03T21:10:05+02:00
title: Une meilleure expérience RSS pour KP
subtitle: Un agrégateur pour les gouverner tous
slug: meilleure-experience-rss
description: Certains trouvent les RSS vieux jeu. Les autres sont souvent frustrés de devoir ouvrir un site web pour lire l'intégralité du contenu. _Keyboard Playing_ n'imposera pas cette contrainte.
author: chop
categories:
  - nouvelles
tags:
  - ux
---

Plusieurs de mes connaissances ont abandonné les flux RSS et leur préfèrent les réseaux sociaux pour rester à jour.
Pour ceux qui n'ont pas fait ce choix, je sais que la solution parfaite n'existe pas, mais il y a au moins une frustration récurrente que je souhaite ne pas reproduire sur ce site.

<!--more-->

## Ce que je hais avec les flux RSS

Je fais partie de ceux qui suivent encore les actualités et blogs _via_ un agrégateur de flux RSS.
J'adore pouvoir les classer en catégories, avoir un suivi de ce qui est lu et de ce qui ne l'est pas, et le garder pour plus tard ou le rejeter parce que le titre seul me suffit à dire que c'est une perte de temps pour moi.

Ce que je n'aime pas, mais qui semble devenu pratique courante notamment pour la presse, c'est de lire un titre attirant et ouvrir l'article pour constater qu'il ne contient que les trois premières lignes ou un résumé sommaire.
Certes, cela peut me permettre d'affiner ma sélection avant de décider si je veux vraiment lire ce contenu ou non, mais cela veut aussi dire que je serai contraint d'ouvrir le site pour voir l'intégralité de l'article.


## Pourquoi cela me déplaît

<aside><p>L'incapacité de lire un article dans son intégralité depuis un agrégateur RSS est disruptif pour l'expérience utilisateur.</p></aside>

Cela me déplaît pour plusieurs raisons, qui peuvent néanmoins se résumer à la suivante : c'est une pratique disruptive en termes d'expérience utilisateur.

Si vous êtes un éditeur, prenez les deux minutes suivantes pour vous mettre à la place de votre lecteur : ils accèdent à toutes leurs nouvelles depuis leur application de nouvelles ou leur agrégateur RSS.
Puis ils tombent sur l'un de vos artivles et ont à ouvrir votre site web.

Première disruption : ils changent d'application --- même si la plupart utilisent un navigateur intégré --- ou de site web.

Cela vous semble négligeable, mais l'identité visuelle et l'agencement de l'organisation sont différents.
Nous en sommes rarement conscients, mais ceci demande de l'énergie à notre cerveau afin de nous adapter.

Par ailleurs, vous allez souvent leur faire charger pléthore d'éléments qui n'intéressent pas vos lecteurs : un flux de commentaires Disqus ou autre, des publicités, un CSS et des pages non-optimisés et lourds, des images...
En plus de ne pas être souhaités, ils vont induire une latence dans l'obtention de l'information.
Cela signifie que vous introduisez également une rupture temporelle.

De plus, si votre utilisateur est dans une zone sans accès à un réseau de données, ils ne pourront pas lire votre contenu, même si son application avait mis en cache tous le contenu de ses flux RSS.
Tout ce qu'ils pourront consulter sera une collection de ~~_teasers_~~ résumés.
Dommage...

Certains lecteurs pourraient arrêter de vous suivre pour ça.
Je l'ai déjà fait.
Lorsque certains sites passent de l'approche « articles complets » à « résumés seulement », leur contenu a intérêt à être suffisamment intéressant pour supporter ces ruptures et tous les déchets qui occupent de la bande passante pour arriver inutilement dans mon navigateur.

En ce qui me concerne, j'ai choisi de payer des abonnements pour permettre à des sites de presse de produire du contenu de qualité sans m'assomer de toute leur pollution.


## Les raisons de ces choix

Ce n'est pas mon domaine d'expertise, mais je connais au moins deux raisons pour lesquelles des éditeurs veulent que les internautes ouvrent leur page.

La première et plus évident : ils gagnent de l'argent grâce à la publicité.
Certains ne vivent que grâce à cela.
Les publicités n'apparaissent pas dans les flux RSS, ou tout du moins pas de façon mesurable, et donc pas de façon monétisable.
Les articles complets dans les flux RSS ne sont donc pas envisageables lorsque la publicité est une source essentielle de revenus.

Une autre raison importante pour forcer les lecteurs à se déplacer est qu'un éditeur veut pouvoir avoir des chiffres.
Ils ont besoin de savoir combien de visiteurs ils ont, combien d'entre eux sont des habitués, quelles pages les attirent, _et cetera_.
La cause derrière ce besoin est la même que précédemment : comment puis-je générer davantage d'argent ?


## Pourquoi vous n'aurez pas à subir ceci

Ces problématiques ne s'appliquent pas à _Keyboard Playing_.
Il n'y a aucun revenu à prendre en compte.
Le but premier du site est de partager, et le partage ne prendra pas s'il est trop contraignant.

Vous avez peut-être déjà remarqué qu'il n'y a aucune publicité sur le site.
Il n'y a aucun plan d'en inclure non plus.
Par conséquent, cette raison pour vous faire sortir de votre application n'a pas lieu ici.

Pour ce qui est d'apprendre à connaître ce qui vous intéresse le plus...
J'aurai des statistiques un jour.
Même s'il y manquera ceux qui, comme moi, préfèrent lire depuis leur agrégateur, je me débrouillerai avec ces informations.
Il y a d'autres moyen de dire ce qui intéresse les lecteurs. :)

Et maintenant que nous avons travaillé à améliorer votre expérience de lecture[^explication-travail], je vais m'atteler à la tâche de produire un véritable contenu...

[^explication-travail]: Cela a demandé un peu de développement car ce n'est pas le comportement par défaut de cette plateforme de blog.

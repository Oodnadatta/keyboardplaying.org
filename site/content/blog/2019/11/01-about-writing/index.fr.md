---
date: 2019-11-01T21:15:47+02:00
title: Au sujet de l'écriture
subtitle: _Coding extends Writing_
slug: histoires
description: J'ai toujours rêvé d'avoir un talent artistique, mais un tel talent vient parfois (souvent ? toujours ?) d'un travail acharné. J'ai commencé à m'entraîner et je partage mes premiers résultats.
banner:
  src: coding-extends-writing.jpg
  alt: Une page de carnet avec un stylo plume. Sur la page, on peut lire « public class Coding extends Writing ».
  by: chop
  license: CC BY 4.0
author: chop
categories:
  - nouvelles
tags:
  - ecriture
---

J'ai toujours eu envie d'avoir un talent artistique.
Des amis m'ont qualifié d'« artiste latent ».
En vérité, je ne me suis jamais trouvé de don, mais c'est peut-être parce qu'il n'y a pas de don et que le talent vient du travail.

<!--more-->

## L'admiration des autres et leurs coulisses

Le savoir-faire et la maîtrise d'une compétence m'ont toujours fascinés.
C'est vrai dans tous les domaines, mais cela s'accompagne parfois d'une pointe d'envie lorsqu'il s'agit d'arts créatifs.

Nous avons tous cet ami qui peut dessiner n'importe quoi et ressort généralement d'une heure de cours de réunion ou de travail ennuyeuse avec une page à vous faire baver.
Ou bien celui qui peut jouer presque n'importe quel air de musique sans avoir eu besoin de l'étudier auparavant.
Ces compétences semblent souvent innées et il m'est arrivé de me dire qu'il était injuste que certaines personnes naissent avec autant de talent.

<aside><p>Le talent n'est pas inné, il est le fruit du travail.</p></aside>

Heureusement, j'ai un peu mûri depuis.
J'ai également eu l'opportunité de faire connaissance de quelques dessinateurs et musiciens, entre autres.
Cela m'a permis de découvrir qu'il ne s'agit pas de quelque chose d'inné.
Comme toute compétence, les gens peuvent avoir une affinité naturelle, mais il faut toujours l'aiguiser à travers beaucoup de travail et d'entraînement avant de les voir atteindre leur plein potentiel.

Après avoir réalisé ceci, je pense que j'ai vraiment cru ne jamais pouvoir développer un talent créatif.


## À la recherche de mon truc

Au contraire de tous ces artistes que j'admire, je ne me suis jamais trouvé ce genre de facilité.
Mes dessins se limitent le plus souvent à des bonhommes en bâtons ou des schémas basiques.
J'ai de la change que les amis m'ayant entendu chanter acceptent toujours de me voir.
J'ai tenté ma chance avec des instruments --- j'ai acheté une guitare que je n'ai jamais utilisée et j'ai voulu apprendre à jouer sur [l'orgue électronique](https://en.wikipedia.org/wiki/Combo_organ)[^smartass-jouer-clavier], juste pour découvrir qu'il ne fonctionnait plus.
Je monte de temps à autre des vidéo assez simple en dilettante, et le résultat est généralement apprécié, mais c'est réellement chronophage et je sais que je ne créerai jamais mes films.

[^smartass-jouer-clavier]: Jouer sur un clavier devrait être plus simple pour moi, non ?

Non, je pense qu'aucun de ces arts n'est pour moi.
Alors, avec quelle compétence ai-je une affinité sur laquelle je pourrais construire quelque chose ?
Et l'idée m'est venue : l'écriture.


## L'écriture dans mon passé et mon présent

L'écriture est quelque chose qui m'a toujours attiré.
Plus jeune, j'étais un lecteur avide --- et je le reste dans une certaine mesure --- et les mots ne me laissaient pas indifférents.
J'ai commencé à écrire quelques poèmes au cours de mon adolescence.
Les résultats semblent puérils et insatisfaisants aujourd'hui, mais j'en étais plutôt fier à l'époque.

Plus récemment, ma meilleure amie m'a demandé à plusieurs occasions de relire certains de ses écrits, et elle me dit souvent que mon style est « excellent » --- il s'agit de ses mots, non des miens.
D'autres amis et collègues m'ont fait des remarques dans ce sens depuis.

Je n'ai jamais écrit d'œuvre de fiction longue.
Il y a eu des brouillons et des idées listées sur du papier, mais rien qui ait jamais abouti.
L'écriture requiert trois choses : du temps, de l'énergie et de l'inspiration.
Le plus dur à trouver pour moi a toujours été la dernière.
J'ai des morceaux d'univers que je voudrais développer, mais j'ai besoin d'une histoire à y placer.

L'entraînement pourrait aider.
Les compétences doivent être aiguisées.
Il me faut donc m'exercer.


## Un entraînement inattendu

Il y a quelques années, j'ai réalisé que j'écrivais déjà pour gagner ma vie.
Pensez-y !

Réduit à son état le plus simple, l'écriture consiste à prendre des mots et de la ponctuation, et à les assembler en un tout cohérent, en se basant sur une grammaire partagée et compréhensible par d'autres.
Parfois, on part de rien.
À d'autres occasions, on doit reformuler ou traduire le travail d'autres.

Il y a des façons de maltraiter le langage et pourtant produire un horrible résultat capable de véhiculer vos idées.
Il y a aussi plusieurs manières correctes d'écrire la même chose.
Et bien entendu, il y a une ou quelques belles façons, celles que recherchent les auteurs.

<aside><p>Développer, <em>c'est</em> coder.</p></aside>

Quand on réfléchit en ces termes, développer, _c'est_ écrire.
Dans une autre langue, avec une grammaire différente, mais j'écris _déjà_ tous les jours.
La différence dans mon travail est que je n'ai pas besoin de chercher mes idées : on me les donne.
Il ne me reste qu'à les écrire de la meilleure façon possible en fonction des contraintes que l'on m'impose.


## Entraînement et partage

Pour transformer une affinité en talent, rien de tel que le travail.
J'ai rejoint un groupe d'aspirants écrivains --- et dessinateurs --- avec le même état d'esprit.
Nous nous retrouvons sous le nom de _Communauté du Stylo_.
Le but est de nous motiver mutuellement et de partager des conseils et critiques constructives.

Nous avons établis deux catégories de challenges récurrents :

- un défi hebdomadaire, dont le but est d'écrire quelque chose de très court mais qui peut nous faire sortir de notre zone de confort ;
- un défi bimestriel[^defi-bimestriel], où il nous faut écrire quelque chose de plus long avec quelques contraintes (le plus souvent portant sur le thème).

[^defi-bimestriel]: Bimestriel : une fois tous les deux mois. En vrai, le deuxième a duré cinq mois à force de prolongations successives.

<aside><p>Mes créations sont disponibles dans la section <a href="/stories/">Histoires</a>.</p></aside>

Je ne participerai certainement pas à tous les défis --- nous avons tous un temps limité à notre disposition --- mais je partagerai ici certaines de mes créations réalisées dans ce contexte --- ou pour le plaisir.
C'est pourquoi la section [Histoires](/stories/) est aujourd'hui disponible sur le site.
J'espère qu'elle vous plaira !


## M'aider à m'améliorer

L'entraînement est une chose, mais il est inutile si je vais dans la mauvaise direction.
Un feedback constructif est bienvenu et nécessaire.

Je n'ai pas besoin de savoir quand quelque chose est bien --- bien que ce soit toujours plaisant --- mais je _dois_ être conscient de mes erreurs ou de ce qui pourrait être améliorer.
Signalez-moi ces cas.
Il y a deux façons de le faire :

- écrivez un commentaire sur la page concernée ;
- ou fourchez le repo --- un lien est disponible en bas de chaque contenu --- et soumettez une demande de _merge_.


## Quelques remerciements pour terminer

Pour conclure, j'aimerais remercier les membres de la _Communauté_ pour leur soutien et leurs conseils :
{{< author "tony" >}},
{{< author "lorine" >}},
{{< author "sancho" >}},
{{< author "mart1n" >}},
{{< author "ngorzo" >}},
{{< author "sweepincomics" >}},
{{< author "kaisuke" >}},
{{< author "gio" >}},
{{< author "vinzouille" >}} et
{{< author "captain-jahmaica" >}}.

J'aimerais également remercier {{< author "a-so" >}}, qui m'a toujours soutenu et m'a toujours fait un parfait feedback, bien avant que je ne découvre la communauté.


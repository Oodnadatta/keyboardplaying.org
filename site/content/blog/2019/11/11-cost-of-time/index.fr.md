---
date: 2019-11-11T20:06:16+02:00
title: Le coût du temps
subtitle: Préférez-vous dépenser cet argent maintenant ou plus tard ?
slug: cout-temps
description: "En utilisant le poste de développement comme exemple, ce billet vise à montrer comment les économies à court terme peuvent se retourner contre un projet dans la longueur."
banner:
  src: hourglass.jpg
  alt: Le sablier Windows verse du sable qui s'écoule et couvre le fond d'écran Windows
  by: chop
  license: CC BY-SA 4.0
author: chop
categories:
  - developpement
tags:
  - management
  - ordinateur
aliases:
  - /blog/2015/04/10/cost-time/
---

Plus d'une fois au cours de ma carrière, pourtant pas si longue, j'ai dû travailler sur des machines qui n'étaient pas adaptées à mes besoins pour le développement.
Ceci résultait le plus souvent d'une politique de la société, conçue pour maintenir les coûts des ordinateurs à un niveau raisonnable, mais qui ne prend pas en compte le cas spécifique des développeurs.
Pourtant, il s'agit souvent d'argent jeté par les fenêtres à (pas si) long terme.

<!--more-->

## Les origines de cet article {#origines}

J'ai écrit une première version de ce billet il y a 4 ans et demi, après une journée de frustration intense.
Cela faisait six mois que je travaillais chez ce client, sur la machine du précédent occupant de mon rôle.

J'éteins généralement mon ordinateur chaque soir avant d'attraper mes affaires et rejoindre mon domicile.
Ceci explique que je comprenne pourquoi la plupart de mes pairs ne le font pas.

<aside><p>Il m'a fallu environ 15 min pour allumer mon PC et démarrer les logiciels dont j'ai besoin pour travailler.</p></aside>

J'ai pris pour habitude d'aller directement à mon bureau en arrivant le matin.
Je salue mes collègues sur mon chemin, m'arrête à mon ordinateur et appuie sur le bouton.
Seulement à ce moment puis-je enlever ma veste et aller dire bonjour aux autres.

Ce matin-là, quand je suis revenu après avoir fait le tour, le système d'exploitation était encore en train de charger.
J'ai patienté pour que Windows m'invite à me connecter.
À ce moment, j'ai saisi mes identifiants et j'ai attendu encore un peu.
Quelque chose comme une ou deux minutes, juste le temps pour toutes les routines de sécurité de démarrer et ralentir encore un peu plus l’ensemble.

Cela a pris un peu de temps, mais mon clavier et ma souris ont fini par répondre.
J'ai lancé les logiciels dont j'ai besoin : Outlook pour lire les messages que j'aurais pu recevoir, Chrome pour consulter les nouveautés dans JIRA et, bien sûr, l'EDI qu'utilisait notre équipe, Eclipse.
Puis j'ai attendu encore un peu.
J'aurais pu aller faire un tour à la machine à café et revenir avec une tasse chaude avant que ce soit terminé.

Si on fait le total, du moment où j'ai poussé le bouton ce matin-là au moment où mon environnement était prêt, quinze minutes s'étaient écoulées.
Un quart d'heure avant même de pouvoir commencer à travailler.

<aside><p>Mon ordinateur personnel démarre en moins de deux minutes. Combien de temps perdons-nous chaque jour en voulant économiser sur les machines ?</p></aside>

Ce même jour, en rentrant chez moi, j'ai démarré mon ordinateur pour travailler sur un projet personnel.
Windows était prêt en moins de 2 min, l'ouverture de session a semblé instantanée et chaque application démarrait en quelques secondes.
Soyons honnêtes : ma machine a été conçue pour être puissante, mais elle avait déjà trois ans.[^still-alive]

[^still-alive]: J'utilise toujours cet ordinateur. Seuls les composants victimes de leur âge ont été remplacés --- la carte graphique et le disque dur. Cette machine tourne toujours de façon plus agréable et rapide que la tour toute neuve que mon nouveau client m'a installée il y a quelques mois.

Cela m'a fait réfléchir : combien de temps perdons-nous chaque jour parce que nous essayons d'économiser quelques euros sur un ordinateur ?
Si nous traduisons ça en termes d'argent, est-ce que cette économie en valait vraiment la peine ?


## Le prix du temps d'un développeur

Afin de rendre cet exemple parlant, il nous faut pouvoir le chiffrer.
Je vais donc m'appuyer sur quelques exemples pour tenter d'estimer et comprendre les pertes.
Préparons donc quelques chiffres ! [^randall-munroe-what-if]

[^randall-munroe-what-if]: Je me sens comme [Randall Munroe](https://en.wikipedia.org/wiki/Randall_Munroe) préparant un article pour [_What If?_](https://what-if.xkcd.com)

Notre contexte hypothétique sera bâti sur mon expérience.
Mon employeur est une ESN[^esn].
Fondamentalement, ses clients paient pour que je mette mon expérience et mes compétences au service de leurs projets.
Combien ?
Une journée d'un développeur standard coûte environ 500 €.

[^esn]: ESN : Entreprise de Services du Numérique.

<aside><p>Un développeur coûte environ 62,5 € par heure.</p></aside>

Je travaille au Luxembourg ; une journée de travail y dure 8 h.
Ceci signifie que notre développeur coûte 62,5 € par heure, soit environ 1 € par minute.


## Calcul du temps perdu

### Extinction de l'ordinateur

Supposons que votre société impose la consigne habituelle d'éteindre les ordinateurs chaque soir.[^usure]
C'est une bonne chose pour l'environnement !
Pour vos factures d'électricité aussi, d'ailleurs...

[^usure]: Vous pourriez être inquiet que cela accélère l'usure de vos machines, mais [ces craintes ne sont plus d'actualité](https://www.computerhope.com/issues/ch000390.htm) sur les machines actuelles.

Par expérience, la plupart des développeurs ne suivent pas cette instruction et se contentent de verrouiller leur poste avant de partir.
Je reviendrai sur certaines des raisons par la suite, mais l'idée principale est que les développeurs aiment pouvoir travailler dès leur arrivée, en retrouvant leur environnement tel qu'ils l'ont laissé la veille.

Supposons à présent que vos développeurs soient suffisamment disciplinés pour obéir à cette règle.
Comme dit plus haut, vous économisez de l'électricité.
Imaginons que les postes soient des [machines puissantes consommant 200 Watts](https://michaelbluejay.com/electricity/computers.html).
Vos développeurs vous aiment : ils arrivent à 8h et partent à 19h.
Cela correspond à une journée de 10 h s'ils s'accordent une heure de pause pour déjeuner, et cela laisse les ordinateurs éteints pendant 13 h.

Le principal fournisseur d'électricité [la vend environ 0,1 €/kWh la nuit](https://electricite.lu/fournisseurs/enovos#tarifs).

La consigne d'extinction des machines vous fait donc économiser 0,2 kW × 13 h × 0,1 €/kWh = **0,26 € par ordinateur et par nuit** !
C'est génial !
Mais changer la machine n'aura pas vraiment d'impact sur ce point, réduisant presque cette partie à une digression.

Apparemment, cette politique n'a pas d'effet négatif sur le travail de notre développeur : il ordonne juste à son ordinateur de s'éteindre et n'a même pas besoin d'attendre avant de s'en aller.
Si ce n'est qu'éteindre une machine implique nécessairement...


### Démarrage de l'ordinateur

L'[introduction](#origines) résume plutôt bien cette partie.
À cette occasion, le démarrage de Windows n'a pas été le pire.
Les bons jours, l'écran de connexion pouvait s'afficher en environ 45 s.
La validation des identifiants ne prenait que quelques secondes également.
Comptons une minute depuis l'appui sur le bouton.

C'est maintenant que les vraies lenteurs commencent : avec la session démarrent plusieurs tâches de fond.
Synchronisation des profils itinérants --- bien que nous utilisions toujours la même machine et évitions de stocker des fichiers dans la zone synchronisée --- et lancement de plusieurs logiciels garantissant le respect des politiques de sécurité et d'entreprise.
Tout ceci est logique d'un point de vue managérial, mais la consommation de ressources n'en diminue pas moins les capacités de travail.

Notre développeur connait les limitations de sa machine.
Il se contente donc de lancer le strict minimum dont ils ont besoin :

- Le client de messagerie (MS Outlook) est requis pour échanger avec l'équipe.
- Un navigateur web sera utilisé pour les recherches et la documentation.
L'entreprise n'autorise qu'IE et Firefox.
Un développeur étant allergique à IE par principe, il choisira Firefox, même si celui-ci souffre d'un script de démarrage personnalisé qui le rend étonnamment lent à se lancer.
- Un développeur serait perdu sans son fidèle EDI.
Ici encore, l'entreprise dicte le choix : ce sera Eclipse.

En me basant sur ma pire expérience pour ce scénario, le démarrage dure 15 min.
Avec un environnement plus raisonnable, on peut imaginer que le tout prenne 5 min, voire moins.
Cela paraît peu, mais cela signifie qu'**au moins 10 min (10 €) sont perdues chaque matin**.


### Les lenteurs machine, première partie

Dans le cas de machines particulièrement sous-dimensionnées ou infestées de boufficiel, la plus simple des tâches devient pénible.
Ouvrir une nouvelle fenêtre de l'explorateur peut prendre du temps --- j'ai dû faire face à ce scénario sur mon portable d'entreprise tout neuf, et ce n'est pas un problème matériel.
Par le passé, j'ai eu à attendre pour l'ouverture d'un nouvel onglet du navigateur.
J'ai eu à attendre pour ouvrir un fichier dans mon EDI.
J'ai même eu à attendre pour des actions plus simples, comme changer d'onglet dans le navigateur, sauver le fichier courant...

Lorsque j'ai dû subir ce genre de latence, elle durait généralement de 2 à 15 s.
Parfois plus, surtout au début de la journée, pendant que la politique de sécurité s'applique, ou pendant l'heure du déjeuner, pendant que l'antivirus d'entreprise exécute son analyse quotidienne.
Ce sont des tâches basiques qu'un développeur exécute plusieurs centaines de fois dans une journée, et souvent plusieurs dizaines de fois dans une heure. 

Tout ce temps d'attente causé par le matériel a une conséquence : l'humain va ajouter sa propre latence.
Quand tout va bien, il presse <kbd>Ctrl</kbd> + <kbd>T</kbd> et commence à taper sans même attendre ou regarder l'écran.
L'ordinateur a peut-être une fraction de seconde de retard, mais la personne sait qu'il la rattrapera.
Mais s'il _sait_ qu'il faut quatre ou cinq secondes juste pour ouvrir l'onglet, parce qu'il a déjà eu à recommencer sa saisie par le passé, il va attendre de _voir_ l'onglet ouvert avant de mouvoir ses doigts sur le clavier.
Ceci ajoute un temps mort d'une seconde ou deux.

Selon moi, voir l'humain attendre l'ordinateur est mauvais signe.
Le second est censé accélérer le travail du premier, pas le ralentir.

<aside><p>Le développeur perd 22 min chaque jour pour des latences mineures.</p></aside>

Soyons optimistes et limitons le nombre d'actions affectées par des latences à 30 par heure, avec un temps mort moyen de 5 s et une attente humaine supplémentaire de 0,5 s : **le développeur perd 165 s en moyenne chaque heure**.
Cela représente 2 min 45 s.

Puisqu'il a déjà sacrifié 10 min au début de la journée, il ne lui reste que 7 h et 50 min à travailler.
Sur la journée, **la somme de ces latences est de 21 min 30 s**.


### Les lenteurs machine, seconde partie

Jusqu'ici, nous ne nous sommes intéressés qu'aux petites latences dans les tâches courantes.
Parfois, il faut lancer un logiciel consommateur dédié à une tâche spécifique (comme [Gimp](https://gimp.org) pour une adaptation d'un élément graphique ou SQL Developer pour tester une requête).
Chacun de ceux-ci peut prendre plusieurs minutes à démarrer, surtout dans un contexte tel que celui que je dépeins ici.

Les plantages sont également une crainte quotidienne.
Alors que vous utilisez votre EDI normalement, un voile blanc apparait soudainement parce que vous avez formaté ou sauvé ou organisé les imports de votre fichier.
Vous levez les yeux vers la barre de titre et voyez le honni _(Not responding)_.

Outlook est aussi un spécialiste, même lorsque vous ne l'utilisez pas.
Vous l'affichez parce que vous avez un message à envoyer, mais toute couleur le quitte lorsque vous cliquez sur un bouton du ruban.

{{< figure src="windows-not-responding.png" link="windows-not-responding.png" alt="Une fenêtre de message Windows informant que l'explorateur Windows ne répond pas.">}}

Il semble raisonnable d'imaginer que **le développeur perd 10 min par jour** pour ce type de ralentissements, mais j'ai connu bien pire.


### La compilation

N'importe quel développeur travaillant sur des projets volumineux le sait : la compilation prend du temps.
_Beaucoup_ de temps.

Avez-vous déjà travaillé sur un projet [GWT](http://gwtproject.org) ?
C'est lent.
Votre Java est transpilé en plusieurs versions de JavaScript afin de fonctionner sous plusieurs navigateurs.
Si vous ajoutez une surcouche comme [GXT](https://www.sencha.com/products/gxt/) ou [Smart GWT](https://www.smartclient.com/product/smartgwt.jsp), qui sont les pendants GWT d'[ExtJS](https://www.sencha.com/products/extjs/) et [SmartClient](https://www.smartclient.com/product/smartclient.jsp), la complexité s'en trouve grandement accrue et, logiquement, il en est de même pour le temps de compilation, qui devient _péniblement_ long.

En 2010, ma propre expérience d'un projet GWT + GXT utilisait une configuration Maven optimisée pour construire le projet et le déployer immédiatement sur JBoss.
Chaque exécution durait 15 min.

{{< figure src="/img/xkcd/compiling.png" link="https://xkcd.com/303/" alt="XKCD comic : deux personnes passent en jouant devant le bureau d'un manager, qui s'énervent. Ils répondent simplement « Ça compile ! » et le manager leur dit de continuer." caption="_L'excuse n<sup>o</sup> 1 du développeur pour tirer au flanc « Mon code compile. »<br/>(**Manager** --- Hé ! Retournez au travail ! **Développeur** --- Ça compile ! **Manager** --- Oh. Continuez.)_">}}

L'architecte qui avait posé les fondations de ce projet a par la suite travaillé sur un projet GWT + Smart GWT.
Malgré ses travaux pour minimiser le temps de construction en développement, chaque compilation durait 4 min.
Ce fut un argument pour redéfinir le standard des machines de ce projet : cœurs i7, 16 Gio de RAM et SSD (nous étions en 2012).
Après ces changements, la compilation ne durait plus que 20 s.

Prenons cet exemple pour base : le projet de notre développeur _pourrait_ compiler en 20 s sur une machine plus puissante, mais il lui patienter 4 min à chaque fois.
Ce sont **3 min et 40 s perdues à chaque compilation**.

L'humain est impatient et ce développeur attend d'avoir terminé le développement de sa fonctionnalité avant de s'infliger cette compilation.
Supposons qu'il construit le projet une fois par heure.
Le temps total perdu lors de compilations est de **28 min 43 s chaque jour**.

<aside><p>Des compilations longues entrainent une baisse de qualité du code.</p></aside>

Il est intéressant de noter qu'un délai de compilation accru entraine une perte de qualité, qui résulte en davantage de temps gaspillé.
Puisque le développeur ne veut pas patienter à chaque fois qu'il change une ligne, il ne le fait pas.
Il travaille aveuglément aussi longtemps qu'il le peut et, lorsqu'il pense avoir terminé, il compile et teste.
Il détecte un bug mineur et sait de quelle ligne il provient.
Il corrige la ligne, mais il ne va pas retester pour une si petite correction.
C'est ainsi qu'il passe à côté du vrai bug qui se cachait juste derrière...

Par la suite, quand un incident est signalé et doit être corrigé, il affronte l'enfer !
Il ne peut pas utiliser un débogueur parce que &lt;_insérer ici la spécificité du projet_&gt;, donc il ajoute simplement plein de logs.
Parce que chaque changement et test de correction implique une attente de 4 min.

Ainsi, comme sur tous les projets souffrant de problèmes de qualité, les bugs sont découverts trop tard et, en prime, l'environnement est un obstacle à leur résolution, ajoutant aux délais de livraison et réduisant la satisfaction des utilisateurs/clients.
Me permettré-je d'ajouter que ces corrections ne seront jamais aussi satisfaisantes qu'elles le devraient ?


### Le total

Jusqu'ici, en ne prenant en compte que le temps perdu à cause d'une machine non appropriée, nous avons découvert que notre développeur hypothétique perd chaque jour :

* **10 min** pour démarrer son environnement de travail ;
* **31,5 min** pour les différentes latences et plantages de la machine ;
* **28,7 min** en compilation.

<aside><p>Une machine sous-dimensionnée peut faire perdre plus d'une heure à un développeur chaque jour.</p></aside>

Il s'agit de _pertes_ de temps.
Ce n'est pas le temps total passé à démarrer l'ordinateur ou compiler le projet, mais seulement la différence entre notre situation hypothétique et une se basant sur une configuration plus puissante.

Le total de ces pertes est de **1 h 10 min 12 s perdues chaque jour**.
Ceci signifie **14,63 %** de ce que vous payez votre développeur chaque jour est perdu en _attente_ et pourrait être économisé avec une politique de machines plus adaptée.
Cela représente **73,13 € perdus chaque jour**.

{{< figure src="proportions.fr.png" link="proportions.fr.png" alt="Sur une barre représentant 100 %, mise en valeur de l'équivalent de 15 %, soit le temps perdu quotidiennement." >}}


### Oui, mais...

On pourrait opposer que les développeurs devraient utiliser un environnement similaire à celui sur lequel la solution sera utilisée.
C'est vrai. Pour les _tests_.
Mais on ne construit pas une voiture sur la route parce que c'est là qu'elle sera conduite.
On la construit dans une usine et _ensuite_ on la déplace vers un centre de tests pour s'assurer qu'elle remplit bien les spécifications.

Les développeurs Android chez Google testent leurs développements sur des téléphones de génération précédente, mais ils travaillent sur des ordinateurs.


## Avez-vous vraiment fait des économies sur la machine ?

Il est à présent temps d'examiner une autre hypothèse : vous avez acheté une nouvelle machine au lieu de réutiliser un poste de bureautique monté il y a quatre ans.
Poussons le raisonnement au bout de façon surréaliste : supposons que vous avez retenu une machine surdimensionnée pour vos besoins et que vous n'avez aucun prix de gros auprès de votre revendeur informatique.

<aside><p>Le coût d'achat d'une machine est absorbé en moins de 35 jours.</p></aside>

Je suppose qu'une configuration de joueur à 2500 € serait un beau modèle.

Si vous perdez 73 € par jour, au bout de combien de jours le temps perdu coûte-t-il plus cher que le nouvel ordinateur ?

{{< figure src="graph.fr.png" link="graph.fr.png" alt="Un graphique comparant le prix fixe d'achat d'un PC et le coût croissant du temps perdu. Les lignes se croisent à lorsque l'abscisse vaut 34 jours.">}}

**Il faut 34 jours pour que les lignes se croisent.**

Si vous ne faites pas confiance à mon tracé à main levée, nous pouvons résoudre l'inéquation.
Les mathématiques ne mentent pas.

<div style="text-align:center">
73.125 × <em>d</em> &gt; 2,500<br/>
<em>d</em> &gt; 34.2
</div>

En d'autres mots, **le coût d'achat d'une machine appropriée au développement est un investissement rentable en moins de deux mois**.


## D'autres impacts difficiles à évaluer

Cet article s'est concentré sur ce qui est facile à mesurer, mais nous savons aussi que les interruptions nuient à la concentration d'un individu.
Nous avons également entendu parler de « la zone », cet état de concentration mythique dans lequel un développeur atteint des performances élevées et dans lequel il ne devrait jamais être dérangé.

{{< figure src="/img/monkeyuser/focus.png" link="https://www.monkeyuser.com/2018/focus/" alt="Monkeyuser comic : Un développeur conçoit un algorithme élaboré. Il est interrompu par une question et ne se comprend absolument plus ce qu'il faisait quand il veut reprendre son travail." caption="_Concentration<br/>(**Voix off** --- Hé, tu aurais une minute ? Non en fait, c'est bon. **Développeur** --- Qu'est-ce que je faisais ?)_">}}

Les (pas toujours) minuscules et désagréables interruptions permanentes que nous avons vues plus haut ne permettront à aucun développeur d'atteindre « la zone », ou elle l'en ferait sortir en moins de dix minutes.
Vos développeurs ne pourront pas être aussi efficaces, bien que vous ne puissiez pas réellement le mesurer.

On peut aussi parler de votre volonté à ne pas investir sur cette longue et pénible procédure d'installation.
Il y a quelques semaines, en suivant une procédure détaillée, j'ai passé 2,5 jours à tenter de faire fonctionner notre application sur mon poste.
Aucun des membres de l'équipe n'a pu me dire ce que j'avais raté et j'ai abandonné pour travailler directement sur notre serveur de développement.
Vous conviendrez que ce n'est pas idéal.
Des solutions existent pourtant et il est possible d'investir une fois pour gagner du temps toutes les suivantes.

Nous avons juste effleuré la problématique des économies à court terme résultant à des pertes à long terme, mais la machine n'est ni le pire ni le plus commun dans ce domaine.
La qualité est aussi un point récurrent.
On n'a pas envie de dépenser davantage pour faire du binômage, ou _pair programming_ --- le livre blanc [_Culture Code_](https://www.octo.com/publications/culture-code/)[^culture-code] d'OCTO Technology s'appuie sur plusieurs études et estime que cela représente une hausse du coût de développement de 15 %, avec d'autres bénéfices --- ou dans les tests.
On _sait_ pourtant, et cela a été démontré plusieurs fois, que de tels investissements ne sont pas en perte.

[^culture-code]: N'hésitez pas à le lire, en tant que développeur ou manager, si la qualité logicielle a la moindre valeur à vos yeux.

Au final, on peut réduire tout cela à l'engagement et la motivation des développeurs.
Ils sont les acteurs principaux de la création logicielle.
Ils sont passionnés et suivent vos instructions, mais si vos politiques les empêchent de faire de leur mieux, ils seront frustrés.
La frustration n'est jamais bonne pour vos employés : des insatisfaits ne feront jamais de leur mieux et, en fin de compte, ils quitteront certainement le lieu générant cette frustration, emportant avec eux un savoir et une connaissance de votre projet que vous ne serez jamais sûr de pouvoir remplacer.
Par ailleurs, si la plupart de vos employé sont mécontents, vous aurez du mal à séduire des talents.


## Ce qu'il faut retenir

Une vision à court terme n'est pas suffisante lorsque l'on cherche à créer un produit avec une large base d'utilisateurs en lui espérant de durer dans le temps.

Une vision à long terme implique de vous appuyer sur vos atouts.
Dans les projets informatiques, les développeurs et leurs connaissances en sont un précieux.
On ne peut rien faire sans eux.

Leur donner des outils appropriés n'est jamais une perte d'argent.
**C'est un investissement** qui sera remboursé en quelques semaines.

Leur _refuser_ ces outils peut avoir l'air d'une économie, mais celle-ci se paie souvent bien plus cher sur le long terme.
Par ailleurs, cette approche gêne le travail des développeurs.
Si vous êtes un obstacle à ce que vous leur demandez, comment vos employés peuvent-ils se sentir considérés ?
Et s'ils ne se sentent pas considérés, pourquoi vous donneraient-ils une part d'eux-mêmes --- leur temps, leur énergie, leur passion, leur motivation, leur concentration ?

Si vous voulez que vos développeurs soient impliqués et efficaces, écoutez-les et résolvez leurs problèmes quand vous le pouvez.
Traitez-les simplement comme vous aimeriez l'être à leur place.


> **Les clients ne sont pas la priorité. Les employés sont la priorité**.\
> Si vous prenez soin de vos employés, ils prendront soin des clients.
> <footer>Richard Branson (en lire plus --- en anglais --- <a href="https://www.thehrdigest.com/richard-branson-clients-do-not-come-first-employees-come-first/" rel="nofollow">ici</a> ou <a href="https://nzbusiness.co.nz/article/why-you-should-put-employees-not-customers-first" rel="nofollow">ici</a>)</footer>



## Remerciements

L'image de couverture a été conçue sur base d'une [photo de Simon Goldin](https://en.wikipedia.org/wiki/Bliss_(image)#/media/File:Bliss_location,_Sonoma_Valley_in_2006.jpg) ([CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0)).
Celle-ci figure le même point de vue que [le fond d'écran par défaut de Windows XP](https://en.wikipedia.org/wiki/Bliss_(image)), dix ans plus tard.

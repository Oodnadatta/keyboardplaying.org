---
date: 2019-11-11T20:06:16+02:00
title: The Cost of Time
subtitle: Would you rather spend that money now or later?
slug: cost-time
description: "Using the developer's machine as an example, this post aims at demonstrating that short-term savings are not always (rarely?) a good thing in the long run."
banner:
  src: hourglass.jpg
  alt: Windows's sandglass cursor is spilling sand in front of the Windows wallpaper
  by: chop
  license: CC BY-SA 4.0
author: chop
categories:
  - programming
tags:
  - management
  - computer
aliases:
  - /blog/2015/04/10/cost-time/
---

More than once in my---no-so-long---career, I've had to work on machines that were not appropriate to my development needs.
These were most often the result of company policies designed to reduce the cost of machines to a reasonable level, but that doesn't take the specific case of developers into account.
In the (not-so-)long run, it's actually often money sent down the drain nonetheless.

<!--more-->

## The Origin Story of This Post {#origin-story}

I wrote a first version of this post 4.5 years ago, after a day of intense frustration.
I had been working at this client's office for six months, on the machine of this role's previous occupant.

I usually shut my computer down every night before grabbing my stuff and going home.
Which, of course, allows me to understand firsthand why most of my fellow developers don't.

<aside><p>It took me about 15 minutes to power up my PC and start the software I needed to work.</p></aside>

I've taken the habit, when arriving in the morning, to go straight to my desk.
I greet colleagues on my way, stop at my computer and press the power button.
Only then can I take off my jacket and go on to say "hello" to coworkers who are seated farther.

That morning, when I came back after meeting them, the OS was still loading and I waited for Windows to invite me to log in.
Once it did, I typed my credentials in and waited a bit more.
Something like one or two minutes, just the time for all the security routines to start up and slow the whole thing down a bit more.

Eventually, it was done and my keyboard and mouse pointer started responding to my commands.
I started the software I need to work: Outlook so that I could read the email I might have received, Chrome to see what was new in JIRA, and, of course, our team's IDE, Eclipse.
Then I waited some more.
I could have gone to the coffee machine and come back with a hot mug before it was ready.

All in all, from the moment I pushed the power button on the computer to the moment all my software was ready to work, 15 minutes had gone.
A quarter of an hour before I even began working.

<aside><p>My home computer boots up in less than two minutes. How much time and money do we lose because of saving on the machines?</p></aside>

That same day when I went home, I booted my computer to work on some personal project.
Windows was up in less than 2 minutes, the login felt instantaneous and each application started up in a few seconds.
Admittedly, it was a killer machine when it was built, but it already was three years old back then.[^still-alive]

[^still-alive]: Actually, I still use the same machine. Only the graphic card and hard disk drive have been changed due to old age, but the computer still runs more smoothly and efficiently than the brand new one my current client installed me a few months ago.

This got me thinking: how much time do we lose each day because we tried to save some dollars on a computer?
And if we translate this time in money, was the economy really worth it?


## Converting Developer Time Into a Price

For this example to be demonstrative, we need to use some figures.
So, I'll take some examples to help estimate and understand the loss.
Let's start crunching numbers! [^randall-munroe-what-if]

[^randall-munroe-what-if]: I feel like [Randall Munroe](https://en.wikipedia.org/wiki/Randall_Munroe) preparing a [_What If?_](https://what-if.xkcd.com/) article.

Our hypothetical context will be built on my experience.
My employer is an IT consulting company, so clients pay my employer for having me work on their projects.
How much, do you ask?
A day of an average developer costs about €500 ($550).

<aside><p>A developer costs about €62.5/$70 per hour.</p></aside>

I work in Luxembourg; a work day there is 8-hour long.
This means our developer costs €62.5 ($70) per hour, or grossly €1/$1 per minute.


## Calculating the Lost Time

### Powering Off

Let's imagine your company issues the usual directive to turn off computers every night.[^wear-down]
It's a good thing to do for the planet!
And for your electricity bills, by the way...

[^wear-down]: You may be worried this would wear your computer parts down faster, but [those worries don't apply anymore](https://www.computerhope.com/issues/ch000390.htm).

In my experience, most developers don't follow this instruction and just lock their computer before leaving.
You'll find some of the most common reasons below.
Developers want to be able to immediately start working when they arrive in the morning, with their workspace in the state it was when they left on the previous evening.

Let's now imagine your developers are disciplined enough to comply with this rule.
As I said, you save electricity.
For the sake of argument, [let's imagine it's a high-end computer that uses 200 Watts](https://michaelbluejay.com/electricity/computers.html).
Your developers love you: they come at 8 and leave at 19, which is a 10-hour-long day if they take a lunch break for one hour.
That's a 13-hour-long night for their powered-off machines.

Luxembourg's main electricity provider [sells it about €0.1/kWh at night](https://electricite.lu/fournisseurs/enovos#tarifs).

The power-off rule made you save 0.2 kW × 13 h × €0.1/kWh = **€0.26 ($0.29) per computer and per day**!
That's great!
But changing the machine won't have much of an impact here, making this paragraph mostly a digression.

Apparently, this policy does not have any adverse effect on your developer's work: they just tell the computer to shut down and don't even have to wait before leaving.
Except powering off inevitably means...


### Booting the Computer

The ["origin story"](#origin-story) sums up the booting episode pretty well.
In that occurrence, booting Windows was not the worst part and, on good days, the login screen showed up in 45 seconds or so.
The credential validation is only a few seconds long too, so one minute from pressing the button.

Now, the slowing down begins: with the session start many background processes.
Synchronization of roaming profiles---though we always used the same machines and avoided storing files in the synchronized spaces---and starting up of the various pieces of software ensuring the company policy and security is respected.
All of those are logical from a management point of view, but they consume resources that cannot be used for working.

Our developer knows the limitations of their machine, so they launch the bare minimum they need to:

- The mail client (MS Outlook) is required to communicate with the team.
- A web browser will be used for documentation and searches.
By company policy, only IE and Firefox are authorized.
They're a developer so they'll choose Firefox, but this one is plagued with a customized launch script that makes it desperately slow to start.
- An IDE is the essential tool for any developer.
Once again, company policy dictates the one to be used: Eclipse.

I'll use my worst experience for this scenario, so let's imagine the boot and starting up the workspace takes 15 minutes.
In a more reasonable environment, it's around 5 minutes or even less.
It doesn't look like much, but it's  **at least 10 minutes (€10, or $11) lost each morning**.


### Waiting on the Machine, First Part

In cases of particularly undersized or bloated computers, the most menial task becomes a pain.
Opening a new explorer window can take time---I've had to face this on my brand new company laptop, and that's not a hardware problem.
In the past, I've had to wait for a new browser tab.
I've had to wait more to open a file in an IDE.
I've even had to wait for simpler actions, like switching browser tab, saving changes...

In my experience, when there's a lag on such actions, it can go from two to fifteen seconds.
Even more at the beginning of the day, while the computer is warming up and the security policy is applying, or at lunch, when the company antivirus software runs its daily analysis.
These are only basic tasks a developer performs several hundred times a day, often several dozen times an hour.

There's a consequence to that material-induced waiting time: the human element adds their own lag.
When everything's fine, you press <kbd>Ctrl</kbd>+<kbd>T</kbd> and you begin typing, without even waiting or looking at the screen.
The computer may be a fraction of a second behind you, but you know it'll catch up.
Now, when you _know_ the browser will take four or five seconds just to open the tab, because you've already had to begin typing your request anew in the past, you will wait to _see_ the tab is ready before beginning typing.
So, there's an additional lag of a second or two.

In my---probably not-so-humble---opinion, seeing the human wait on the computer is a bad sign.
The latter is supposed to be a tool to accelerate the work of the first, not slow it down.

<aside><p>The developer loses 22 minutes to various minor lags each day.</p></aside>

Let's be conservative and keep it to 30 lagging actions each hour, with an average lag of 5 seconds, and 0.5 seconds of human-induced additional wait time: **the developer loses 165 seconds each hour**.
That's 2&nbsp;3/4 minutes.

Since they already lost 10 minutes at the beginning of the day, they still has to work 7 hours and 50 minutes.
Throughout the day, **the sum of these lags amounts to 21 minutes and a half**.


### Waiting on the Machine, Second Part

So far, we only considered the little lags in usual tasks.
Sometimes, you also want to launch a dedicated, more consuming program for a specific task (e.g., [the Gimp](https://gimp.org) to change a visual element or SQL Developer to test a query).
Each of those can take several minutes to start, especially on a machine such as our hypothetical one.

Freezes are also to be feared.
You just use your IDE as you usually do and suddenly, out of the blue, because you saved or formatted or organized imports, a white veil appears on Eclipse and the title bar displays _(Not responding)_.

Outlook is also a specialist, even when you're not using it.
You just switch windows because you need to send a message, but all color drains from its face when you click a button in the ribbon.

{{< figure src="windows-not-responding.png" link="windows-not-responding.png" alt="A window indicating that Windows Explorer is not responding.">}}

Let's say our developer loses **10 minutes a day** to such freezes, though I've experienced some dramatic cases.


### Compiling

Every developer having to compile large projects knows it: it takes time.
_A lot_ of time.

Have you ever been compiling a [GWT](http://gwtproject.org) project?
It is long.
Your Java gets transpiled to several versions of JavaScript, which should work on many browsers.
If you add an overlay like [GXT](https://www.sencha.com/products/gxt/) or [Smart GWT](https://www.smartclient.com/product/smartgwt.jsp), which really are [ExtJS](https://www.sencha.com/products/extjs/) and [SmartClient](https://www.smartclient.com/product/smartclient.jsp) ported to GWT, the complexity is highly increased and, hence, so is the compilation time.
It becomes _painfully_ long.

In fact, in 2010, my own experience of a GWT+GXT project, with an optimized Maven pipeline that built and deployed to JBoss directly, implied 15-minute-long build.

{{< figure src="/img/xkcd/compiling.png" link="https://xkcd.com/303/" alt="XKCD comic: two people fool around and a manager is getting angry. They just say 'Compiling!' and everything is fine with the manager.">}}

The architect that laid down the foundations for this project later moved onto another GWT+Smart GWT project.
Though he minimized the time of development build, each compilation on a corporate computer took 4 minutes (at the start of the project).
This was an argument they used to define a new standard for the machines on this project: i7 cores, 16 Gio of RAM and SSDs (this was 2012).
The compilation time was cut down to 20 seconds.

Let's take this example as a basis: our developer's project _could_ compile in 20 seconds on a more powerful machine, but they has to face a 4-minute long build.
That's **3 minutes and 40 seconds lost on each compilation**.

Having to wait is awful, so they don't compile until they think the development of some feature is finished developing.
Let's say it's once an hour.
The total time lost on compilation each day is **28 minutes and 43 seconds each day**.

<aside><p>Long compilation times induce a code quality loss.</p></aside>

Note that long compilation times implies quality loss, which ultimately results in yet more time wasted.
Since you don't want to have to wait every time you change a line, well... You just don't.
You develop blindly for a time and, when you suppose you're finished, you build and test.
And you see a minor bug.
You think you know what line of code caused it, so you fix it.
But hey, you won't build it another time, right?
And you won't see that another, more important bug was hidden just a step further...

When an incident comes in and you have to debug, it's just hell!
You can't go into debug mode because &lt;_insert the specificity of your project here_&gt;, so you just add many logs.
Because each change and testing if the bug is fixed implies a 4-minute wait.

So, as on all projects with quality problems, you discover bugs later than you should and, as a bonus, your whole environment makes it hard to fix them, implying delays in deliveries and unsatisfied users/customers.
Dare I add the fixes will never be as satisfactory as they should be?


### The Grand Total

So far, just taking into account machine-unappropriateness-related time losses, we discovered that, each day, our hypothetical developer loses:

* **10 minutes** starting up the workspace;
* **31.5 minutes** due to the machine's different lags and freezes;
* **28.7 minutes** compiling.

<aside><p>Because of an undersized machine, a developer can spend more than one hour waiting each day.</p></aside>

These are time _losses_.
It is not the total time booting up the computer or building the project, only the difference between our hypothetical situation and one with a more powerful configuration.

When summed up, these amount to **1 hour, 10 minutes and 12 seconds lost each day**.
This means **14.63%** of what you pay a developer each day is only for _waiting_ and could be saved with a more appropriate machine and policy.
That is **€73.13 ($81) going to waste each day**.

{{< figure src="proportions.png" link="proportions.png" alt="On a bar split into ten, highlight of three sections, representing approximately the time lost daily." >}}


### Yes, But...

You may think developers should use an environment similar to the one the solution will be used in.
That's true. For _testing_.
But you don't build a car on a road because that's where you'll drive it.
You work in a factory and _then_ you take your product to a testing facility to make sure it's what you expected it to be.

Google's Android developers test their developments on previous-generation phones, but they work on computers.


## Did You Really Save Money on That Machine?

Now, let's imagine you bought a new machine instead of reusing an old one, with some unrealistic characteristics.
First, it's completely overkill for development.
We also forget that you get wholesale prices because you negotiated a contract with your computer reseller.

<aside><p>The cost of buying a new machine is absorbed in less than 35 worked days.</p></aside>

I suppose a €2,500 gaming PC would be a nice machine.

If you lose €73 a day, how long is it before the cost of lost time exceeds the cost of the new machine?

{{< figure src="graph.png" link="graph.png" alt="A graph comparing the stable price of buying a machine and growing cost of lost time. The lines cross at 34 days.">}}

**It takes about 34 days for both lines to cross.**

You may not trust my approximative outline, but we can do the maths.
Maths doesn't lie.

<div style="text-align:center">
73.125 × <em>d</em> &gt; 2,500<br/>
<em>d</em> &gt; 34.2
</div>

In other words, **buying an efficient machine is an investment that pays itself off in less than two months**.


## Other, Difficult-to-Measure Impacts

This post took only into account what is easy to measure.
But we also know that interruptions break any individual's focus.
We also know of "the zone," the mythical state of focus in which a developer achieves high performance and from which they should never be disturbed.

{{< figure src="/img/monkeyuser/focus.png" link="https://www.monkeyuser.com/2018/focus/" alt="Monkeyuser comic: A developer is creating an elaborated algorithm. Somebody interrupts him by asking a question, and the developer is at a total loss about what he was doing when he gets back to his work.">}}

Well, those pesky, tiny, permanent interruptions won't let a developer reach "the zone," or it will get them out in less than ten minutes.
Your developers won't be able to be as efficient, though you can't measure it.

There's also this wish not to invest on automating that long, painful installation procedure.
A few weeks ago, following a detailed procedure, I spent 2.5 days trying to make our application work on my computer.
None of the other developers on the team could tell what I had missed and I gave up and worked on our development server instead.
Not great, right?
Solutions exist and you could invest once to save time on all the next occurrences.

We scratched the surface of the short-term savings resulting in long-term losses, but the machine is neither the worst nor the most common problem in this area.
Quality is also a recurring pain point.
We don't want to spend more money on pair programming---the well-documented and well-presented [_Culture Code_](https://www.octo.com/en/publications/culture-code/) white paper by OCTO Technology[^culture-code] estimates it's a 15% increase in development cost, with other benefits---or testing.
We _know_, though, and it has been demonstrated again and again, that such investments are greatly rewarding in the long run.

[^culture-code]: Please, don't hesitate to read it, as a developer or a manager, if software quality is any worth to you.

Finally, it boils down to developers' engagement and motivation.
They are the main actors of software creation.
They are passionate and work at your command, but if your policies prevent them from doing their best, they will get frustrated.
Frustration is never good in your employees: discontented people never give their best and, eventually, they leave the place generating the frustration, taking with them some knowledge about your project that you may never replace.
And, if most of your employees seem disgruntled, you will have a hard time attracting talents.


## The Takeaways

Short-term vision is not sufficient if you're aiming at creating a product with wide acceptance and long life.

Long-term vision implies to capitalize on your assets.
When working on an IT project, developers and their knowledge are a precious one.
Nothing will work without them.

Giving them appropriate tool is not a loss of money, **it's an investment** that will pay itself back in a matter of weeks.

_Denying_ them those tools may seem like an economy, but you often pay much more for it in the long term.
Furthermore, by doing so, you are hindering their work.
If you are an obstacle to what you are yourself asking them to do, how can they feel considered?
And if they don't feel considered, why would they give you part of themselves---their time, their energy, their passion, their motivation, their focus---for you?

If you want your developers to be invested and efficient, listen to them and solve their problems if you can.
To keep it short, just treat them as you'd like to be treated.


> **Clients do not come first. Employees come first**.\
> If you take care of your employees, they will take care of the clients.
> <footer>Richard Branson (read more <a href="https://www.thehrdigest.com/richard-branson-clients-do-not-come-first-employees-come-first/" rel="nofollow">here</a> or <a href="https://nzbusiness.co.nz/article/why-you-should-put-employees-not-customers-first" rel="nofollow">here</a>)</footer>



## Credits

The cover image was designed using a [photo by Simon Goldin](https://en.wikipedia.org/wiki/Bliss_(image)#/media/File:Bliss_location,_Sonoma_Valley_in_2006.jpg) ([CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0)).
It was taken from the same spot as [Windows XP's default wallpaper](https://en.wikipedia.org/wiki/Bliss_(image)), ten years later.

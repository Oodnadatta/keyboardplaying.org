---
date: 2020-01-05T07:18:30+02:00
title: The Role of Software Creators
#subtitle: Would you rather spend that money now or later?
slug: intro-sustainable-digital/role-software-creators
#description: As a programmer, you're probably looking at characters all day long. Let's look for a font that prints them in a pleasant way.
#banner:
#  src: marcus-depaula-tk7OAxsXNL0-unsplash.jpg
#  alt: Metal block letters from various fonts.
#  by: Marcus dePaula
#  link: https://unsplash.com/photos/tk7OAxsXNL0
#  license: Unsplash
author: chop
#categories:
#  - programming
#tags:
#  - java
draft: true
---

**DEADLINE: 2020-01-20**

- [ ] Example: Fairphone -> but we're _software_ creator, we don't decide anything about hardware
- [ ] Example: Chrome + RAM, fatware
- [ ] Define ecodesign and provide concrete examples
- [ ] No unique or absolute answer, but a need to know the consequences of one's choices
- [ ] Include some data about the studies for languages consumptions

---
date: 2020-01-05T07:18:30+02:00
title: Other Ethical Considerations
#subtitle: Would you rather spend that money now or later?
slug: intro-sustainable-digital/other-ethical-considerations
#description: As a programmer, you're probably looking at characters all day long. Let's look for a font that prints them in a pleasant way.
#banner:
#  src: marcus-depaula-tk7OAxsXNL0-unsplash.jpg
#  alt: Metal block letters from various fonts.
#  by: Marcus dePaula
#  link: https://unsplash.com/photos/tk7OAxsXNL0
#  license: Unsplash
author: chop
#categories:
#  - programming
#tags:
#  - java
draft: true
---

**DEADLINE: 2020-02-03**

- [ ] Our biases and how they propagate to society (FB + total transparency, Snapchat dysmorphia)
- [ ] The amplification through IA
- [ ] Black Mirror: the future is already here

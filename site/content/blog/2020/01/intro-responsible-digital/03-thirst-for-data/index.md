---
date: 2020-01-05T07:18:30+02:00
title: Data Is the New Oil
#subtitle: Would you rather spend that money now or later?
slug: intro-sustainable-digital/data-is-the-new-oil
#description: As a programmer, you're probably looking at characters all day long. Let's look for a font that prints them in a pleasant way.
#banner:
#  src: marcus-depaula-tk7OAxsXNL0-unsplash.jpg
#  alt: Metal block letters from various fonts.
#  by: Marcus dePaula
#  link: https://unsplash.com/photos/tk7OAxsXNL0
#  license: Unsplash
author: chop
#categories:
#  - programming
#tags:
#  - java
draft: true
---

**DEADLINE: 2020-01-27**

Take from my notes for conference

- [ ] Importance of remaining the master of your own data (CCPA[^ccpa], GDPR)
- [ ] Genetics
  - [ ] French doctors
  - [ ] https://www.privateinternetaccess.com/blog/2019/12/as-public-fears-mount-over-online-surveillance-and-lack-of-control-advertising-industry-gets-privacy-religion-sort-of/
  - [ ] http://www.thierryvallatavocat.com/2018/05/un-fichage-genetique-generalise-des-la-naissance-et-votre-adn-vendu-au-prive-bienvenue-a-gattaca.html
- [ ] Comfort over privacy?
  - [ ] https://idlewords.com/talks/internet_with_a_human_face.htm
  - [ ] Lose control of what'll happen to the data next
  - [ ] Should we trust those giants? (password stored in clear, data forgotten on servers and accessible)


[^ccpa]: https://www.numerama.com/politique/596130-le-rgpd-californien-est-sur-le-point-de-devenir-realite.html

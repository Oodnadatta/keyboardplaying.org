---
date: 2020-01-13T08:24:22+01:00
title: The Footprint of the Digital World
subtitle: But Grandmother! What big feet you have!
slug: intro-sustainable-digital/footprint-digital
description: Everything's digital today, but we fail to see what impacts this has on the environment. And sometimes, on us humans.
banner:
  src: ikit-digital-pollution.banner.png
  alt: "A drawing showing several actors of the digital: from user to data center, from builder to dumpster, with the energy plant in the center feeding them all..."
  by: ikit
  link: ikit-digital-pollution.jpg
  socialSrc: ikit-digital-pollution.jpg
author: chop
categories:
  - sustainable-digital
---

Last month, I had the opportunity to talk about a topic I hold dear: how to make our job part of something sustainable.
This is something I've planned to share and I thought the period of the new year's resolutions may be a great time to do it.

So, before going any further, let's begin with why the idea of sustainable development should even be a question in the digital field.

<!--more-->

_This post is part of a series:_
1. _Why make digital sustainable?_ **The Footprint of the Digital World**


## The Digital Universe {#digital-universe}

When studying the footprint of the digital universe, it's common to distinguish three main categories:
- the data centers, the big bad guys that concentrate so much hatred;
- the users, that is, ourselves and the devices we play daily with;
- the network, that links both of those together.

<aside><p>The universe of digital is made of 34 billion devices.</p></aside>

I like how [GreenIT.fr's study][greenit-environmental-footprint] summed up the digital environment with actual figures.
Relying on those, across these three categories, **34 billion devices** are now in the possession of 4.1 billion users.
This is **an average of 8 devices per user**.


### The Data Centers

Let's begin with the big bad wolves.

The data centers host about 67 million servers.
Of course, this number will be increasing thanks to all we wish AI to do for us, but that's [a topic for later](#electric-bill).

Ok, that's about it for data centers.


### The Network

The network is made of about 1.3 billion devices, divided as follows:
- 1.1 billion DSL boxes, the type your ISP provides you;
- 10 million antennas, from 2G to 5G;
- about 200 million more miscellaneous WAN or LAN equipment.


### The Users

Now for the fun part: us!

Ten years ago, computers were the most prominent cause of the environmental footprint of the digital world.
Today, there are about 3.2 billion of them.

There are also 3.5 billion smartphones, and 3.8 billion faxes and other phones.
The displays are having a growing impact too, especially due to their increasing size and definition.
There are about 3.1 billion of them, including computer screens, TVs and video projectors.

Any of these categories comprises already more devices than the data centers and network united, but the most astounding figure is still to come: though they did not exist ten years ago, it is estimated that the Internet of Things may consist of 12 to 19 billion[^figure-variation] smart or connected devices (speakers, watches, thermostats, lighting...).
That number alone accounts for about or more than half of the existing digital devices.

[^figure-variation]: The figure depends on the source, e.g., [Negaoctet][negaoctet] or [GreenIT.fr's study][greenit-environmental-footprint].

### Summary

In a graphical form, this is how digital devices are divided (gray-blue is for users, dark red is for the network, and the tiny little orange corner is for data centers).

{{< figure src="panorama-of-digital.mini.png" link="panorama-of-digital.png" alt="A graphic representation of the figures given above" >}}

All these devices **weigh 223 million tons**.
That's as much as 179 million average sedans, which is **5 times the number of cars in France**.

But that's a sheer number.
What does it mean in terms of environment?


## The Environmental Point of View

I'm a [digital native].
I always have my laptop and smartphone with me.
Like most of my siblings, however, I tend to forget that what I use so frequently has a devastating effect on our planet.
**Digital _and_ software _do_ have an environmental footprint**.


### Why the Code Impacts our Environment

{{< figure src="matrix-screens.jpg" alt="The operator is seen from behind, looking at several screens covered in green glyphs." attr="Credits: _The Matrix_ (1999)" >}}

I expect most readers of this blog to be coders.
As one of them, I speak of experience when I say, we rarely think of the environmental impact when we program.
After all, the code does not exist in the material world.
It's only bits in my machine, so it doesn't have any consequence on the environment, right?
**Wrong!**

<aside><p>The digital universe generates 4% of the humanity's carbon footprint.</p></aside>

Of course it has an impact.
Let's forget the footprint of merely storing your code.
You're not typing for the pleasure of it---though it can be quite enjoyable, that's not the point---you're typing because you're building something.
That something will run on servers, computers, smartphones or connected things.

It's become common to measure the carbon footprint of things.
It's estimated that **this digital world accounts for 3.8% of humanity's carbon footprint**.
In 2015, Gartner estimated it was only 2%, the same participation as civil aviation.
They predicted the part of digital would increase in the upcoming years.
They were obviously correct.

But how comes the digital wears shoes so huge?


### Fabrication {#fabrication}

{{< figure src="matrix-zero-one-factory.jpg" alt="Machines are being built on an assembly line in Zero-One, the city of machines." attr="Credits: _The Animatrix_ (2003)" >}}

Those devices have to be built.
This is obvious, and we know building electronic components is not transparent.
[Negaoctet][negaoctet] tells us that **a smartphone is made from 70 kg of materials**.
Those are varied and numerous.

<blockquote>
Of the 83 stable (nonradioactive) elements, at least 70 of them can be found in smartphones! That’s 84% of all of the stable elements.
<cite><a href="https://www.acs.org/content/acs/en/education/resources/highschool/chemmatters/past-issues/archive-2014-2015/smartphones.html" lang="en"><em>Smartphones: Smart Chemistry</em></a>, ACS, Brian Rohrig</cite>
</blockquote>

<aside><p>We threaten the environment and human health to extract materials that won't get renewed.</p></aside>

For instance, electronic components require many minerals, among which [rare earths](https://en.wikipedia.org/wiki/Rare-earth_element) are present.
Despite their name, those are relatively plentiful in Earth's crust, but they are dispersed.
**The extraction and refining processes are destructive**---the biodiversity is jeopardized by the destruction of the habitat---and dangerous to the human and animal health---these processes leave a non-negligible amount of pollution.

<blockquote>
You have to inject seven or eight tons of ammonium sulfate into the soil to extract a ton of oxide, these toxic liquids will reside for a long time and the consequences would be appalling if the groundwater were polluted.
<cite><a href="http://french.people.com.cn/Economie/7782297.html">Su Bo, China's Vice-Minister of Industry and Information Technologies, 2012</a></cite>
</blockquote>

The fabrication itself is quite polluting, too.

<blockquote>
Of the 5S's 70 kg carbon footprint, 81% will be emitted during production and 12% during the phone's "career".
<cite><a href="https://theecologist.org/2013/sep/17/carbon-credentials-smartphones" lang="en"><em>The Carbon Credentials of Smartphones</em></a>, The Ecologist, David Thomas</cite>
</blockquote>

Another cause for worry is that those resources are abiotic: they don't grow, they don't reproduce.
Which means **they don't get renewed**.
The increased demand related to the ever-growing production of digital devices already has raised the question of a potential shortage in years to come.
This risk is heightened by the fact that those materials don't benefit much from recycling, but [I'll come back to that](#end-of-life).

[This article from TechRadar](https://www.techradar.com/news/phone-and-communications/mobile-phones/our-smartphone-addiction-is-costing-the-earth-1299378 "Our smartphone addiction is costing the Earth") sums up the most preoccupying facts of mineral extraction for our digital devices.

In case you're wondering, yes, smartphones and displays are the electronic devices with the most polluting fabrication, and it essentially concerns users, due to the sheer number of devices we use.


### Energy Consumption {#electric-bill}

{{< figure src="matrix-field.jpg" alt="Towers of pods containing humans convert bioelectricity into energy for the machines." attr="Credits: _The Matrix_ (1999)" >}}

Those devices will consume energy. A whole lot of it.
Each time it's plugged into a wall or each time it drains its battery, it will suck greedily on power.
[It's said][negaoctet] that **Internet represents 10% of the world's electric bill**.

If we added the consumption of all digital devices end to end and compared it to the consumption of the most energy-demanding countries, we'd get this:[^fn-electric-environmental][^fn-electric-bill]

[^fn-electric-environmental]: Electricity consumption is not a relevant environmental indicator.
I'll still give some information about it because it's one thing we have no trouble understanding.

[^fn-electric-bill]: The [electricity consumption of countries](https://photius.com/rankings/2019/energy/electricity_consumption_2019_0.html) is actually from 2016, as I could find no more recent source.
The consumption of digital is the one evaluated by [GreenIT.fr's study][greenit-environmental-footprint].
This data concurs with [a research paper from 2013](https://www.researchgate.net/publication/255923829_Emerging_Trends_in_Electricity_Consumption_for_Consumer_ICT).

{{< figure src="electric-consumption-of-digital.png" alt="Bar chart showing the electric consumption of, in decreasing order, China, United States, digital, India, Japan, Russia and France." >}}

We can see that **the electric bill for all the digital devices in the world would be about a third of that of the United States**, or three times that of France.
Digital, which at its source is merely a tool to make our lives easier, can be placed on the same scale as a country in itself.
But [we'll come back to that](#techplomacy).

Now, the repartition between the actors is more balanced than for the fabrication:
- 24% of the electricity consumed by digital devices is billed to data centers;
- 32% is required for the network to work;
- the final 44% the users' devices.

{{< figure src="repartition-electric-consumption.png" alt="Donut chart showing the repartition of electric consumption between users, data centers and network." >}}

It's interesting to note the [Jevons effect](https://en.wikipedia.org/wiki/Jevons_paradox) will soon be visible in this field.
It occurs when the increase of efficiency of a resource can't compensate the rate of consumption of that resource due to increasing demand.

<aside><p>The gains in energy efficiency will be counterbalanced by the increase in demand those gains permit.</p></aside>

One great example is the smart assistants and domotic possibilities to optimize the heating of buildings, which allow for energy savings.
But because those are multiplying, more data centers will be required to supply the artificial intelligence to handle all those inputs and variables.

Another example that is great for gamers is the cloud gaming.
Basically, those offers promise you what Netflix brought to video: great games being playable in the cloud, without having to buy the latest machine.
That's great because players get to mutualize their hardware, thus reducing the number of machines to be built.
Except it's expected this will make gaming more accessible, which may in turn attract new gamers, and therefore totally nullify the original expected gain.


### End of Life {#end-of-life}

{{< figure src="matrix-zion-last-stand.jpg" alt="Machines are penetrating the roof of Zion, the last human city, and are being destroyed by mecha armors defending the city." attr="Credits: _The Matrix Revolutions_ (2003)" >}}

One day, all those devices will stop working.
Or they'll no longer be supported.
Or people'll buy a new, more beautiful, more powerful, more filled-with-features-you-don't-need-and-don't-use version of it.

But what happens to them when they're thrown away?
They become e-waste---short for "electronic waste," or WEEE for "Waste Electrical and Electronic Equipment."
A [United Nations study](https://news.un.org/en/story/2017/12/639312-electronic-waste-poses-growing-risk-environment-human-health-un-report-warns) reported that 44.7 million tons of e-waste was discarded in 2016, but only 20% of it was recycled.

[We saw that electronics devices contain many substances](#fabrication), several of which are toxic, such as lead, mercury, cadmium, polybromainated flame retardants, barium and lithium, which can affect nearly every system in the human body.
[A study about the electronic pollution in the USA](https://envibrary.com/electronic-pollution/) revealed that **the average computer screen has 5 to 8 pounds of lead, representing 40% of all the lead in the US landfills**.

If not handled properly, those chemicals can accumulate in the soil, water and food.
Health risks also result from the inhalation of toxic fumes.
The effects damage to the brain, heart, liver and skeletal system, and they can lead to birth defects.

At least some of these substances can be recycled.
Apple says it recaptured 2204 pounds of gold from recycled devices in 2015.
That's $40 million worth.

[Rare earths are another problem](http://www.europarl.europa.eu/RegData/bibliotheque/briefing/2013/130514/LDM_BRI(2013)130514_REV1_EN.pdf).
Right now, there are no (economically) viable solution to separate and purify those metals from e-waste, though the field is of interest for many reasons---not only economical.



## Social Considerations

### Coltan, the New Blood Minerals {#coltan-blood-mineral}

We still could say much about the minerals and ores that are required to build all our electronic devices.
You may have already heard of blood diamonds.
Wikipedia introduces them as follows:

<blockquote>
<strong>Blood diamonds</strong> (also called <strong>conflict diamonds</strong>, <strong>war diamonds</strong>, <strong>hot diamonds</strong>, or <strong>red diamonds</strong>) are diamonds mined in a war zone and sold to finance an insurgency, an invading army's war efforts, or a warlord's activity. The term is used to highlight the negative consequences of the diamond trade in certain areas, or to label an individual diamond as having come from such an area.
<cite><a href="https://en.wikipedia.org/wiki/Blood_diamond"><em>Blood diamond</em></a>, Wikipedia</cite>
</blockquote>

You may also have heard of [**coltan**](https://en.wikipedia.org/wiki/Coltan), the short for "columbite-tantalites."
It's an ore from which are extracted niobium and tantalum.
The latter is **used to manufacture capacitors that are required for all those smart electronic devices**.

In 2008, [about 71% of global tantalum supply was newly mined](https://minerals.usgs.gov/minerals/pubs/commodity/niobium/myb1-2009-niobi.pdf) while 20% was obtained from recycling.

Well, due to the high and ever-increasing demand, **coltan has become a new ["blood mineral"](https://en.wikipedia.org/wiki/Conflict_resource)**.
Without getting lost into the details, common issues around the extraction of this ore include the exploitation of workers, who generally don't earn a living wage by Western standards.
Forced and [child labor](https://www.humanium.org/en/child-labor-in-the-mines-of-the-democratic-republic-of-congo/) have been observed.

Since it's become so valuable, smugglers and cartels have also taken an interest into it.

<blockquote>
More than 5 million people are already dead in Congo in a war mostly financed by the electronic industry.
<cite>translated from <a href="http://www.portablesdusang.com/information.php" lang="fr"><em>Du sang dans nos portables</em></a>, Frank Poulsen, 2010
</blockquote>

A note underneath this citation indicates that the number is 6 million since 2014.


### Denmark's Tech Ambassador {#techplomacy}

[We've already compared the digital sector to a country](#electricity-bill) to have an idea of what its electricity bill could represent.
Mid-2017, Denmark went further and created an office for a tech ambassador.

![Office of Denmark's Tech Ambassador](denmark-tech-ambassador.png)

Why, you may wonder, did they do that?
The answer is simple: because tech has become more than the tool it used to be.
Above everything else, it's about recognizing the prominent role technology has gained in our everyday life.

<blockquote>
We've been too naïve for too long about the tech revolution.
We need to make sure that democratic governments set the boundaries for the tech industry&mdash;and not the other way around.
That's where the Danish TechPlomacy initiative comes in.
<cite>Jeppe Kofod, Denmark’s Foreign Minister</cite>
</blockquote>

**Some of today's most obvious societal changes come from technological disruption**: big data and IoT bring new challenges about data privacy and cybersecurity, AI may help augment employees or automate their jobs, cryptocurrency operates outside of existing financial architecture, social media accelerates the communication of (sometimes erroneous) information and has a huge impact on democratic life.

Big companies are driving most of these innovations and have gained an economic and political influence that match or even surpass that of nation states.
In the same time, policy-makers are having a hard time keeping up with the quickly, ever-changing technological landscape.

Those considerations impact the foreign policy and geopolitics in new ways.
These are the foundations for Denmark's TechPlomacy initiative.



## Evolution Over Time

{{< figure src="evolution.svg" alt="Infographics highlighting the multiplication of digital and connected devices from 2010 to 2025." attr="CC BY-NC-ND [Geneviève Van Diest](https://visuelle.be/), [Céline Berthaut](https://celineberthaut.fr/)" >}}

There's much data in this post, but it's not a complete view, and most of all, it's not a snapshot.
Things move and, when it comes to tech, they keep moving faster.

[GreenIT.fr's study][greenit-environmental-footprint] thus estimates that the [digital universe we presented in first part](#digital-universe) will increase fivefold between 2010 and 2025.
**The environmental impacts will double or triple depending on the indicator**.

The main causes are the multiplication of connected devices, the increase of screen sizes, a limitation of energy efficiency we can still hope for and the equipment of emerging countries.

The tensions on raw materials will increase too, reinforcing [the role of ores in the financing of armed conflicts in Africa and Asia](#coltan-blood-mineral).


## What's to Get Out of All This?

Tech is a prominent part of our lives, but **it has an environmental, social and even politic impact**.

Can we, as software creators, do something to mitigate the negative parts of this impact?
Well, maybe we can, but this'll be the topic of next week's post.


[greenit-environmental-footprint]: https://www.greenit.fr/environmental-footprint-of-the-digital-world/
[negaoctet]: https://negaoctet.org/
[digital native]: https://en.wikipedia.org/wiki/Digital_native

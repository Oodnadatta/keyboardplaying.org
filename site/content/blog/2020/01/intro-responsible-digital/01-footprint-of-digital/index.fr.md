---
date: 2020-01-13T08:24:22+01:00
title: L'empreinte du monde numérique
subtitle: Mère-Grand, que vous avez de grands pieds !
slug: intro-numerique-responsable/empreinte-numerique
description: Tout de nos jours est numérique, mais nous ne percevons pas toujours les impacts sur l'environnement. Et parfois, sur nous autres humains.
banner:
  src: ikit-digital-pollution.banner.png
  alt: "Un dessin montrant plusieurs acteurs du numérique : de l'utilisateur au centre informatique, du constructeur à la décharge, la centrale énergétique au centre les alimentant tous..."
  by: ikit
  link: ikit-digital-pollution.jpg
  socialSrc: ikit-digital-pollution.jpg
author: chop
categories:
  - numerique-responsable
---

Le mois dernier, j'ai pu animer une conférence sur un sujet qui m'est cher : comment rendre notre travail plus durable.
J'avais prévu d'en parler ici et j'ai songé que la période des résolutions de Nouvel An serait un moment propice.

C'est pourquoi, avant d'aller plus loin, je vous propose d'examiner la situation afin de comprendre pourquoi cette idée de développement durable a le moindre intérêt dans le domaine du numérique.

<!--more-->

_Ce billet fait partie d'une série :_
1. _Pourquoi s'intéresser au numérique responsable ?_ **L'empreinte du monde numérique**


## L'univers numérique {#univers-numerique}

Lorsque l'on étudie l'empreinte de l'univers numérique, il est courant de distinguer trois catégories principales :
- les centres informatiques, les _data centers_, les grands vilains qui attirent tant de haine ;
- les utilisateurs, c'est-à-dire nous-mêmes et tous les appareils que nous utilisons quotidiennement ;
- le réseau, qui lie les deux précédents.

<aside><p>L'univers digital est constitué de 34 milliards d'appareils.</p></aside>

J'aime la façon dont l'[étude de GreeenIT.fr][greenit-empreinte-environnementale] résume le numérique en quelques chiffres.
D'après ceux-ci, **34 milliards d'appareils sont actifs** aujourd'hui au sein de ces trois catégories, en possession de 4,1 milliards d'utilisateurs.
Cela représente **en moyenne 8 appareils par utilisateur**.


### Les centres informatiques

Commençons par le grand méchant loup.

Les centres informatiques hébergent environ 67 millions de serveurs.
Bien entendu, ce nombre va augmenter grâce à notre volonté de tirer profit de l'intelligence artificielle, mais c'est un sujet [pour plus tard](#facture-electrique).

Et c'est à peu près tout pour cette catégorie.


### Le réseau

Le réseau est constitué d'environ 1,3 milliard d'appareils, répartis comme suit :
- 1,1 milliard de boitiers xDSL ou fibre, comme celles que vous fournit votre FAI ;
- 10 millions d'antennes, allant de la 2G à la 5G ;
- environ 200 millions d'autres équipements réseau local ou global divers.


### Les utilisateurs

Et à présent, la partie intéressante : nous !

Il y a dix ans, les ordinateurs représentaient la cause majeure de l'empreinte environnementale du monde numérique.
Aujourd'hui, ils sont environ 3,2 milliards.

On compte également 3,5 milliards de smartphones et 3,8 milliards de fax et autres téléphones.
Les dispositifs d'affichage ont un impact grandissant, en particulier en raison de leurs taille et résolution croissantes.
Ils sont au nombre de 3,1 milliards, dont des écrans d'ordinateur, des télévisions et des vidéoprojecteurs.

N'importe laquelle de ces catégories dépasse déjà les centres informatiques et le réseau réunis, mais le nombre le plus écrasant arrive seulement : alors qu'ils n'existaient pas il y a dix ans, on estime que les objets connectés --- enceintes, montres, thermostats, éclairage... --- seraient de 12 à 19 milliards[^variation-chiffres].
Ce nombre seul représente environ ou plus de la moitié des appareils numériques existants.

[^variation-chiffres]: Ces chiffres dépendent de la source, p.ex. [Negaoctet][negaoctet] ou l'[étude de GreenIT.fr][greenit-empreinte-environnementale].


### Résumé

Si on reprend ces chiffres sous forme graphique, voici comment se divise le monde numérique (gris-bleu pour les utilisateurs, bordeaux pour le réseau et le tout petit coin orange pour les centres informatiques).

{{< figure src="panorama-of-digital.mini.fr.png" link="panorama-of-digital.fr.png" alt="Une représentation graphique des chiffres donnés plus haut." >}}

L'ensemble de ces appareils **pèse 223 millions de tonnes**.
Ceci équivaut à 179 millions de berlines, soit **5 fois le parc automobile français**.

Mais c'est un nombre brut.
Que cela signifie-t-il en matière d'environnement ?



## Le point de vue environnemental

Je suis un [enfant du numérique](https://fr.wikipedia.org/wiki/Enfant_du_num%C3%A9rique).
J'ai toujours un ordinateur et un téléphone portables avec moi.
Comme la plupart de mes frères et sœurs cependant, j'ai tendance à oublier que ce que j'utilise si fréquemment a un effet dévastateur sur notre planète.
**Le numérique _et_ le logiciel _ont_ une empreinte environnementale**.


### Pourquoi le code impacte notre environnement

{{< figure src="matrix-screens.jpg" alt="L'opérateur est vu de dos, regardant plusieurs écrans couverts de glyphes verts." attr="Source : _Matrix_ (1999)" >}}

Je suppose que la plupart des lecteurs de ce blogue sont des codeurs.
En étant un moi-même, c'est d'expérience que je peux dire que nous nous soucions bien rarement de notre impact environnemental quand nous développons.
Après tout, le code n'existe pas au sens matériel.
Ce ne sont que des bits dans ma machine, donc cela n'a aucune conséquence sur l'environnement.
N'est-ce pas ?
**C'est faux !**

<aside><p>L'univers digital compte pour 4 % de l'empreinte carbone de l'humanité.</p></aside>

Bien sûr que le logiciel a un impact.
Oublions l'empreinte du simple fait de stocker votre code.
Vous ne tapez pas au clavier pour le plaisir --- bien que ce soit plutôt plaisant --- vous tapez pour construire quelque chose.
Ce quelque chose tournera sur des serveurs, des ordinateurs, des smartphones ou des bidules connectés.

Il est devenu courant de donner un équivalent en gaz à effet de serre (GES) pour mesurer l'empreinte de quelque chose.
On estime que **ce monde numérique est responsable de 3,8 % des émissions de GES de l'humanité**.
En 2015, Gartner estimait cette part à 2 % seulement, la même que celle de l'aviation civile.
Ils prédisaient alors que la part du numérique augmenterait dans les années à suivre.
Ils ne s'étaient pas trompés.

Mais comment se fait-il que les chaussures du numérique soient si grandes ?


### Fabrication {#fabrication}

{{< figure src="matrix-zero-one-factory.jpg" alt="Des lignes d'assemblage fabriquent des machines à Zero-One, la ville des machines." attr="Source : _Animatrix_ (2003)" >}}

Ces appareils doivent être fabriqués.
C'est évident et nous savons que construire des composants électroniques n'est pas une opération blanche.
[Negaoctet][negaoctet] nous dit qu'**un smartphone est fait à partir de 70 kg de matières premières**.
Celles-ci sont nombreuses et variées.

<blockquote>
Des 83 éléments stables (non radioactifs), au moins 70 peuvent être trouvés dans les smartphones ! Cela représente 84 % de tous les éléments stables.
<cite>traduit de <a href="https://www.acs.org/content/acs/en/education/resources/highschool/chemmatters/past-issues/archive-2014-2015/smartphones.html" lang="en"><em>Smartphones: Smart Chemistry</em></a>, ACS, Brian Rohrig</cite></cite>
</blockquote>

<aside><p>Nous menaçons l'environnement et la santé humaine pour extraire des matériaux qui ne seront pas renouvelés.</p></aside>

Par exemple, les composants électroniques nécessitent de nombreux minéraux, dont les [terres rares](https://fr.wikipedia.org/wiki/Terre_rare).
Malgré leur nom, ces dernières sont plutôt abondantes dans la croute terrestre, mais elles ne se trouvent pas en amas conséquents.
**Les processus d'extraction et de raffinages sont invasifs** --- la biodiversité est menacée par la destruction de l'habitat --- et dangereux pour la santé de l'humain et de l'animal --- ces procédés polluent considérablement.

<blockquote>
Il faut injecter sept ou huit tonnes de sulfate d'ammonium dans le sol pour extraire une tonne d'oxyde, ces liquides toxiques vont résider longtemps et les conséquences seraient épouvantables si l'eau souterraine était polluée.
<cite><a href="http://french.people.com.cn/Economie/7782297.html">Su Bo, vice-ministre de l'Industrie et des Technologies de l'information Chinois, 2012</a></cite>
</blockquote>

La fabrication elle-même est polluante.

<blockquote>
De l'empreinte GES du 5S, qui vaut 70 kg, 81 % seront émis pendant sa production et 12 % pendant la « carrière » du téléphone.
<cite>traduit de <a href="https://theecologist.org/2013/sep/17/carbon-credentials-smartphones" lang="en"><em>The Carbon Credentials of Smartphones</em></a>, The Ecologist, David Thomas</cite>
</blockquote>

Une autre source d'inquiétude est que ces ressources sont abiotiques : elles ne poussent pas, elles ne se reproduisent pas.
Ce qui veut dire qu'**elles ne se renouvèlent pas**.
La demande accrue liée à la production croissante d'appareils numériques a déjà soulevé la question d'une potentielle pénurie dans les années à venir.
Le risque est augmenté par le fait que ces matériaux ne bénéficient que peu du recyclage, mais [j'y reviendrai](#fin-de-vie).

[Cet article de TechRadar](https://www.techradar.com/news/phone-and-communications/mobile-phones/our-smartphone-addiction-is-costing-the-earth-1299378 "Our smartphone addiction is costing the Earth") résume les points les plus préoccupants autour de l'extraction de minerais pour nos appareils numériques.

Dans le cas où vous vous poseriez, oui, les smartphones et les dispositifs d'affichage sont les appareils électroniques dont la fabrication est la plus polluante, et ils sont essentiellement la conséquence des utilisateurs et du nombre d'appareils que nous utilisons.


### Consommation énergétique {#facture-electrique}

{{< figure src="matrix-field.jpg" alt="Des tours génèrent convertissent la bioélectricité d'êtres humains en énergie pour les machines." attr="Source : _Matrix_ (1999)" >}}

Ces appareils vont consommer de l'énergie. Beaucoup d'énergie.
À chaque fois qu'ils seront branchés sur une prise ou qu'ils videront une batterie, ils étancheront avidement leur soif de courant.
[On dit][negaoctet] qu'**Internet représente 10 % de la consommation électrique mondiale**.

Si on compare la consommation énergétique de tous les appareils numériques mis bout à bout à celle des pays les plus gourmands, on obtiendrait ceci[^fn-electrique-pas-environnemental][^fn-donnees-electrique] :

[^fn-electrique-pas-environnemental]: La consommation électrique n'est pas un indicateur environnemental pertinent, mais je l'aborde tout de même, car elle a l'avantage d'être facilement compréhensible.

[^fn-donnees-electrique]: Les [consommations électriques nationales](https://photius.com/rankings/2019/energy/electricity_consumption_2019_0.html) datent de 2016, car je n'ai pas trouvé de source plus récente.
La consommation du numérique est celle relevée par [l'étude de GreenIT.fr][greenit-empreinte-environnementale].
Ces données concordent avec [un article de recherche de 2013](https://www.researchgate.net/publication/255923829_Emerging_Trends_in_Electricity_Consumption_for_Consumer_ICT).

{{< figure src="consommation-electrique-du-numerique.png" alt="Graphique représentant les consommations électriques de, par ordre décroissant, la Chine, les États-Unis, le numérique, l'Inde, le Japon, la Russie et la France." >}}

Nous voyons que **la facture électrique pour l'ensemble des appareils numériques dans le monde représente trois fois celle de la France**.
Le numérique, qui n'est à la base qu'un outil pour rendre nos vies plus aisées, peut être comparé à un pays.
Mais [nous y reviendrons](#techplomacy).

La répartition entre les différents acteurs du numérique est plus équilibrée ici qu'elle ne l'est pour les impacts de la fabrication :
- 24 % de la consommation électrique du numérique est le fait des centres informatiques ;
- 32 % sont la conséquence du réseau ;
- les 44 % finaux sont l'alimentation des appareils des utilisateurs.

{{< figure src="repartition-consommation-electrique.png" alt="Camembert montrant la répartition de la consommation électrique entre utilisateurs, réseau et centres informatiques." >}}

Il est intéressant de noter que le numérique rencontre aujourd'hui [le paradoxe de Jevons](https://fr.wikipedia.org/wiki/Paradoxe_de_Jevons).
Celui-ci se manifeste lorsque le gain d'efficience d'utilisation d'une ressource ne parvient pas à compenser la consommation de celle-ci liée à une demande croissante.

<aside><p>Les gains en efficience énergétique sont contrebalancés par la demande accrue que ces mêmes gains permettent.</p></aside>

Un exemple plutôt chouette est le progrès réalisé dans la domotique et les assistants intelligents, avec la possibilité d'optimiser le chauffage d'un bâtiment, ce qui permet des économies énergétiques.
Mais parce que ceux-ci se multiplient, des centres informatiques seront nécessaires pour héberger toute l'intelligence artificielle qui traitera toutes ces données et variables.

Un autre exemple intéressant pour les joueurs est la nouveauté du _cloud gaming_.
Fondamentalement, cette approche promet ce que Netflix a apporté à la vidéo : de grands jeux vidéos, disponibles dans le nuage, sans avoir besoin de la dernière machine.
C'est génial, car les joueurs mutualisent leur matériel, permettant ainsi de réduire le nombre de machines à fabriquer.
Sauf que l'on s'attend à ce que cela rende les jeux vidéos plus accessibles, donc attire de nouveaux joueurs et ainsi annule le gain espéré.


### Fin de vie {#fin-de-vie}

{{< figure src="matrix-zion-last-stand.jpg" alt="Machines are penetrating the roof of Zion, the last human city, and are being destroyed by mecha armors defending the city." attr="Source : _Matrix Revolutions_ (2003)" >}}

Un jour, tous ces appareils arrêteront de fonctionner.
Ou [ils ne seront plus supportés](https://www.nextinpact.com/news/105989-bouton-connecte-niu-nodon-toujours-en-vente-malgre-applications-gestion-plantees.htm).
Ou les gens achèteront un nouveau, plus beau, plus puissant, plus débordant-de-fonctionnalités-que-je-ne-connais-et-n'utilise-pas.

Mais que leur arrive-t-il quand ils sont jetés ?
Ils deviennent des déchets --- des DEEE, pour « déchets d'équipements électriques et électroniques ».
Une [étude des Nations Unies](https://news.un.org/en/story/2017/12/639312-electronic-waste-poses-growing-risk-environment-human-health-un-report-warns) a rapporté que 44,7 millions de tonnes de DEEE ont été générées en 2016, mais seuls 20 % d'entre elles ont été recyclées.

[Nous avons vu que les appareils électroniques contiennent de nombreuses substances](#fabrication), dont plusieurs sont toxiques, comme le plomb, le mercure, le cadmium, le baryum et le lithium, qui peuvent affecter pratiquement chaque système du corps humain.
[Une étude sur la pollution électronique aux États-Unis](https://envibrary.com/electronic-pollution/) a révélé que **l'écran d'ordinateur moyen contient de 2,3 à 3,6 kg de plomb, ce qui représente 40 % de tout le plomb dans les décharges américaines**.

S'ils ne sont manipulés correctement, ces substances peuvent s'accumuler dans le sol, l'eau et la nourriture.
Les risques pour la santé résultent aussi de l'inhalation de fumées toxiques.
Les effets touchent le cerveau, le cœur, le foie et le système squelettique, et ils peuvent causer des anomalies congénitales.

Au moins certaines de ces substances peuvent être recyclées.
Apple dit qu'ils ont récupéré une tonne d'or d'appareils recyclés en 2015.
Cela représente une valeur 36 millions d'euros.

[Les terres rares sont un autre problème](http://www.europarl.europa.eu/RegData/bibliotheque/briefing/2013/130514/LDM_BRI(2013)130514_REV1_EN.pdf).
À l'heure actuelle, il n'y a pas de moyen (économiquement) viable de séparer et purifier ces métaux des déchets électroniques, bien que ce soit un sujet d'intérêt --- pour des raisons pas uniquement économiques.



## Aspects sociaux

### Le coltan, nouveau minéral de conflits {#coltan-mineral-conflits}

Il reste tant à dire sur les minéraux et minerais nécessaires à la construction des appareils électroniques.
Vous avez peut-être entendu parler des diamants de conflits.
Wikipédia les présente comme suit :

<blockquote>
Les <strong>diamants de conflits</strong>, parfois aussi nommés « <strong>diamants de sang</strong> » (<em>blood diamonds</em> en anglais) théorisés par le géographe irlandais Hugo J.H. Lewis, sont des diamants issus du continent africain, et qui alimentent les nombreuses guerres livrées par des rebelles aux gouvernements. Extraits de mines localisées dans des zones où la guerre fait rage, ces diamants sont vendus en toute illégalité et en toute clandestinité, afin de fournir en armes et en munitions les groupes armés qui les exploitent. 
<cite><a href="https://fr.wikipedia.org/wiki/Diamants_de_conflits"><em>Diamants de conflits</em></a>, Wikipédia</cite>
</blockquote>

Vous avez peut-être également entendu parler du [**coltan**](https://fr.wikipedia.org/wiki/Coltan), mot-valise pour « columbite-tantalite ».
C'est un minerai duquel sont extraits le niobium et le tantale.
Ce dernier est **utilisé pour construire des condensateurs et filtres utilisés pour tous nos appareils électroniques intelligents**.

En 2008, [environ 71 % de la production globale de tantale a été extraite de terre](https://minerals.usgs.gov/minerals/pubs/commodity/niobium/myb1-2009-niobi.pdf) tandis que 20 % ont été obtenus du recyclage.

En raison de cette demande toujours croissante, **le coltan est devenu un nouveau « minéral de conflit »**.
Sans nous perdre dans les détails, les points couramment liés à l'extraction de ce minerai incluent l'exploitation de travailleurs, qui ne gagnent généralement pas de quoi vivre d'après standards occidentaux.
Le travail forcé et [celui des enfants](https://www.humanium.org/en/child-labor-in-the-mines-of-the-democratic-republic-of-congo/) ont également été constatés.

Puisqu'il a atteint une telle valeur, les contrebandiers et cartels s'y intéressent également.

<blockquote>
Plus de 5 millions de personnes sont déjà mortes au Congo dans une guerre essentiellement financée par l'industrie électronique.
<cite><a href="http://www.portablesdusang.com/information.php" lang="fr"><em>Du sang dans nos portables</em></a>, Frank Poulsen, 2010
</blockquote>

Une note à côté de cette citation indique que le nombre est de 6 millions depuis 2014.


### L'ambassadeur de la tech au Danemark {#techplomacy}

[Nous avons déjà comparé le secteur du numérique à un pays](#facture-electrique) pour nous faire une idée de ce que représenterait sa facture électrique.
Mi-2017, le Danemark est allé plus loin et a créé un poste d'« ambassadeur de la tech ».

![Office of Denmark's Tech Ambassador](denmark-tech-ambassador.png)

Mais pour quelle raison, vous demandez-vous peut-être, ont-ils fait ça ?
La raison est simple : parce que la technologie a dépassé le statut d'outil qu'elle avait à l'origine.
Plus que tout, il est question de reconnaitre le rôle proéminent qu'elle a aujourd'hui dans nos vies.

<blockquote>
Nous avons été trop naïfs trop longtemps au sujet de la révolution technologique.
Nous devons nous assurer que les gouvernements démocratiques posent les limites pour l'industrie technologique --- et non le contraire.
C'est là que l'initiative TechPlomacy danoise entre en jeu.
<cite>Jeppe Kofod, ministre des Affaires étrangères du Danemark</cite>
</blockquote>

**Certains des changements sociétaux les plus évidents viennent de changements technologiques** : la big data et l'Internet des objets apportent de nouveaux défis sur la vie privée et la cyber sécurité, l'intelligence artificielle pourrait augmenter les employés ou automatiser leur travail, les cryptomonnaies opèrent en dehors de toute architecture financière existante, les médias sociaux accélèrent la circulation de l'information (parfois fausse) et ont un impact non négligeable sur la vie démocratique.

De grandes sociétés propulsent la plupart de ces innovations et ont gagné une influence économique et politique qui égale ou même surpasse celle des états nations.
En parallèle, les législateurs peinent à suivre le rythme de ce paysage technologique en perpétuelle évolution.

Ces considérations impactent la géopolitique d'une nouvelle façon.
Voilà les fondations pour l'initiative TechPlomacy du Danemark.



## Évolution dans le temps

{{< figure src="evolution.svg" alt="Infographie laissant deviner la multiplication des objets numériques et connectés entre 2010 et 2025." attr="CC BY-NC-ND [Geneviève Van Diest](https://visuelle.be/), [Céline Berthaut](https://celineberthaut.fr/)" >}}

Ce billet comprend énormément de données, mais ce n'est pas une vue complète ou détaillée, loin de là.
Ce n'est pas non plus un instantané.
Les choses bougent et, quand il s'agit de technologie, elles vont en accélérant.

[L'étude de GreenIT.fr][greenit-empreinte-environnementale] estime ainsi que l'[univers numérique que nous avons présenté plus haut](#univers-numerique) va quintupler entre 2010 et 2025.
**L'impact environnemental sera doublé ou triplé selon l'indicateur observé**.

Les raisons principales seront la multiplication des appareils connectés, l'augmentation des tailles d'écran, un tassement des gains d'efficience énergétique et l'équipement des pays émergents.

Les tensions sur les matières premières vont également s'accentuer, renforçant [le rôle des minerais dans le financement des conflits armés d'Afrique et d'Asie](#coltan-mineral-conflits).



## Que retenir de tout ceci ?

Le numérique joue un rôle important dans nos vies, mais **il a un impact environnemental, social et politique**.

Pouvons-nous, en tant que créateurs logiciels, réduire la part négative de cet impact ?
Peut-être bien, oui, mais ce sera le sujet du billet de la semaine prochaine.


[greenit-empreinte-environnementale]: https://www.greenit.fr/empreinte-environnementale-du-numerique-mondial/
[negaoctet]: https://negaoctet.org/

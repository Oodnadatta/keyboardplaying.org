---
title: Management
description: Advices to any people having to work with developers and manage them, drawn from my experiences as a developer and a team leader
---

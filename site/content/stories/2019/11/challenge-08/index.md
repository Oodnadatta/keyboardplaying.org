---
date: 2019-11-08T14:37:05+02:00
title: Raining Cats and Dogs ... and More
slug: raining-cats-and-dogs-and-more
author: chop
license: CC BY-NC-ND 4.0

description: 'Micro-story written for a weekly challenge. Imposed theme: the rain.'

challenge:
  type: weekly
  rules:
  - Short.
  - 'Imposed theme: the rain.'
---

"Damn, rotten weather!"

It makes me smile.
I'm likely the only one, but it makes me smile.
People don't like rain.
To me, this is certainly the most tolerable moment in this dreadful August.
What an awful, permanent heat!
If I didn't carry my laptop in my backpack, I'd certainly wouldn't burden myself with the umbrella.

Did I hear something knocking on the road?
Again? And again?
If hail's falling, I'd better get moving.
Home isn't far now.

Wow. Judging by the noise, that was a big one.
The car's alarm's blaring, its owner will probably discover some damage.

WOAOW! What was that fucking thing?!
It fell to my feet and...
F--or crying out loud, I never saw hail making a hole in the concrete.
Holy sh ... It put my umbrella on fire!
So I didn't imagine the heat when it fell down.

Damn, where are those things coming fr ...
Oh, dang ...
I imagine that one up there is the big brother.
Um. Doesn't look like running will be any help anymore ...

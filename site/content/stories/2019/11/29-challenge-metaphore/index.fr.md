---
date: 2019-11-29T19:04:52+02:00
title: C'est quoi, un « architecte technique » ?
slug: c-est-quoi-un-architecte-technique
author: chop
license: CC BY-NC-ND 4.0

description: "Comment expliquer le rôle d'architecte à des personnes qui ne comprennent pas son métier ? Peut-être en parlant de l'autre type d'architecte !"

challenge:
  type: hebdomadaire
  rules:
  - Format court.
  - Écrire une métaphore filée (raté, j'ai utilisé une comparaison).
---

La question fatidique est arrivée.
Tout en étant légèrement différente.

« Et en quoi ça consiste, "architecte technique" ? »

D'habitude, on lui demande plutôt ce qu'il fait dans la vie.
Répondre qu'il travaille dans l'informatique satisfait la plupart de ses interlocuteurs.
Admettons-le : la plupart des gens sont curieux par politesse, mais ne s'intéressent pas vraiment aux autres.
Pour les plus inquisiteurs, il va parfois jusqu'à expliquer qu'il développe des applications et logiciels, mais va rarement plus loin.

Seulement aujourd'hui, son rencart l'a trouvé par son profil LinkedIn plutôt que par un site de rencontre.
Aussi a-t-elle vu son intitulé de poste, et pour qui n'est pas dans la profession, il faut reconnaître qu'il est abscons.

« Tu conçois la structure des bâtiments pour être certain qu'ils vont tenir dans la durée, c'est ça ? »

Il baisse la tête et sourit, réprimant un petit rire, puis relève les yeux et regarde la jeune fille avant de lui répondre.

« Non, ça n'a rien à voir.
Je travaille dans le développement de logiciels.
Alors oui, si on veut, je conçois des plans, mais pour du code.

--- Je ne comprends pas.
Il y a des plans pour des logiciels ? »

Elle est vraiment jolie.
Il ne sait pas si elle est réellement intéressée, mais elle en a l'air, et c'est plutôt flatteur pour lui qui pense généralement que son métier n'a rien de captivant pour qui ne le pratique pas.
Comment l'expliquer à quelqu'un qui ne le connait pas ?
Peut-être en utilisant une image.

« On peut essayer de comparer ça à un architecte dans le bâtiment, si tu veux.
Mettons qu'une entreprise a besoin d'une nouvelle application.
Elle devient le promoteur du projet : elle donne l'argent et les grandes lignes et elle s'attend à ce qu'il soit réalisé dans les délais.
Plusieurs personnes vont travailler sur ce projet.
On a un chef de projet qui va assurer la maîtrise d'ouvrage.
Et on va avoir un architecte, qui va dessiner le bâtiment, avant que tous les corps de métier en commencent la réalisation.
Jusqu'ici, ça va ? »

Elle hoche la tête.
Elle semble absorbée.

« Très bien.
Dans ce schéma, c'est moi qui joue le rôle de l'architecte.
Mais je ne peux pas faire ce que je veux quand je dessine les plans.
Le promoteur m'a donné un budget et des échéances qu'il ne faut pas dépasser, donc je ne peux pas laisser libre cours à mon imagination et créer un bâtiment somptueux et irréalisable.

« Par ailleurs, ce bâtiment va servir un but précis.
Si c'est un bâtiment d'habitation, il doit avoir une porte et des fenêtres, ou les gens n'aimeront pas y vivre.
Ou si ce doit être un hangar et que je ne fais pas une grande porte pour les porte-palettes ou les poids lourds, ça va poser problème !

« Ensuite, il y a un voisinage.
Une application existe rarement de façon complètement isolée.
Elle communique avec d'autres et doit obéir à certaines règles imposées par l'entreprise.
Il faut aussi qu'elle respecte la charte graphique de celle-ci.
C'est un peu comme se dire qu'il faut respecter les règles du voisinage : il faut laisser tant d'espace entre la façade et la rue, les canalisations respectent un standard bien précis, il y a des règles qui concernent l'aspect visuel des bâtiments.
Toutes ces contraintes sont à prendre en compte quand on conçoit la solution.

--- Je crois que je saisis...
Donc tu mets tout ça ensemble, tu dessines tes plans, tu les expliques à ton équipe et tu vas faire la même chose sur un autre projet ?

--- Ha ha !
Non, c'est rarement aussi simple.
En général, une fois que j'ai décidé de comment on allait faire les choses, je pose les fondations pour que les développeurs --- les maçons, si on veut --- puissent construire sereinement.
Et je reste avec eux au moins le temps de la construction afin de pouvoir les aider s'ils rencontrent des problèmes.

--- Je ne comprends plus.
N'est-ce pas le rôle des maçons de faire le gros œuvre ?

--- Ah oui, je vois.
Dans le bâtiment, tu as certainement raison.
Dans le logiciel, c'est un peu différent.
On ne devient pas architecte parce qu'on sort d'une école.
En général, on sort d'école maçon.
On va travailler sur plusieurs projets et voir des choses différentes.
Des fois, on va aider ou remplir le rôle du plombier, de l'électricien, du plâtrier...

« Grâce à tout ça, on se fait une belle expérience, assez variée.
On connait plusieurs techniques pour résoudre un problème et on peut choisir la plus adaptée à une situation donnée.
C'est pour ça que les gens nous font confiance pour faire ce travail.
C'est aussi pour ça que les développeurs nous font confiance : ils n'aimeraient pas avoir quelqu'un qui leur donne des consignes, mais ne sait pas faire les choses.
Là, ils savent que je ne suis pas un petit chef.
Je suis juste quelqu'un comme eux avec plus d'expérience.

--- Donc si je comprends bien, tu es un maçon expert, devenu architecte, avec des connaissances de plombier, électricien, plâtrier, poseur de fenêtres et artiste peintre ?
Ça tombe bien, je voulais me faire construire une maison. »

Il va la reprendre puis remarque son charmant sourire mutin.
Non, inutile de lui rappeler les limites d'une métaphore et le risque de la confondre avec la réalité.

---
date: 2019-11-29T19:04:52+02:00
title: What's a "Technical Architect" anyway?
slug: what-s-a-technical-architect-anyway
author: chop
license: CC BY-NC-ND 4.0

description: "How can he explain the role of an architect to someone who doesn't know anything about his job? Maybe with the other kind of architect!"

challenge:
  type: weekly
  rules:
  - Short.
  - Write an extended metaphor (we declared this attempt off topic).
---

The customary question arrived, though not exactly in its usual form.

"So, what's a 'technical architect' anyway?"

Usually, people ask him what he does for a living, and most of them are satisfied with his vague answer: he works in IT.
Let's face it: most people are curious out of manners, not out of real interest.
For the most inquisitive ones, he sometimes explains he develops software applications, but rarely needs to detail any further.

But today, his date found him via his LinkedIn profile rather than through a dating site.
That's why she's seen his job title, which is abstruse for people not working in the same field.

"I'd guess you design the structure of buildings to make sure they'll last, am I right?"

He lowers his head and smiles, suppressing a little laugh, before looking up at the girl and answering her. 

"Actually, no," he answered, "it's nothing like that.
I work in software development.
So, yes, in a way, you could tell I draw plans, but for code."

"I don't understand.
There are plans for code?"

Damn, she _is_ pretty.
He can't tell if she's really interested, but she sure looks like it.
It's quite flattering, since he usually thinks his job is not that captivating for people without his passion.
Now, how can he explain to someone who knows nothing about it?
Maybe an image could help.

"We can try comparing it to an architect for buildings.
Ok, imagine an enterprise needs a new application.
It's like being the promoter of a project: it spends money, defines the main line---and a deadline it expects the project to be finished by.
Several people will work on that project.
You'll have a project manager to make sure everything is on the rails.
And you'll have an architect, to draw the building, before all other builders can begin making it true.
Are you ok so far?"

She nods.
She's listening attentively.

"Ok.
In this project, _I_ am the architect.
But I can't do everything I wish when I draw my plans.
The promoter gave me an envelope and deadlines I cannot go beyond, so I can't just put my wildest dreams on paper to create a lavish and probably impractical building.

"What's more, this building will have a specific purpose.
If it's a living place, it must have a door and windows, or people won't like living there.
Or if it must be a hangar and that I don't include a wide door for forklifts and trucks, that'll be an issue!

"Then, there's a neighborhood.
We don't build one software app alone.
It communicates with others and must comply with some rules the enterprise decided.
It must also fit the branding guide---the visual identity, if you like.
It's a bit like complying with the neighborhood regulations: you must leave a space between the building and the street, the pipes follow this or that established standard, you cannot paint your facade with a bright pink and so on.
All these are constraints that you must take into account when you design the solution."

"Alright," she exclaims brightly, "I think I get it.
So... You take all that, you put it in a blender, that gives you the ink for your plans.
You draw those, you give them to your team, and you move to another project like a superhero?"

"Ha ha! I sometimes wish it were that simple.
No, most of the time, once I've decided how we'll do things, I lay down the foundations so that the developers---the masons, I guess---can build peacefully.
I also stay with them while they build so that I can help them if they encounter difficulties."

"You won," she says, frowning, "I _don't_ get it.
Isn't it the role of the masons to do the heavy work?"

"Oh yeah, I see.
Yes, in the building sector, you'd be right.
For software, it's a bit different.
See, I'm not an architect because I learned software architecture in school.
What usually happens is, you get out of masonry school.
Then you get to work on a project, and another one, and another one, and... Ok, I stop.
The point is, you get to see various things, various ways to do.
And sometimes, you get to help the plumber, the electrician, the plasterer.

"You build a great, varied experience.
You know several ways to fix an issue and you can pick the most adapted to your current situation.
That's why people trust us with this job.
That's also why developers trust us: they wouldn't want someone to give them orders without knowing how to do what they're asking.
They know I'm not a tinpot dictator, I'm just one of them with a bit more of experience."

"Ok," she answers with a renewed light on her face, "so you're an expert mason, who became an architect, with some knowledge in plumbing, electricity, plastering, installation of windows and artistic painting?
That's perfect! I was thinking of getting me a house built."

He's about to correct her when he notices her charming, mischievous smile.
No, no need to remind her of the limits of a metaphor and the risk of confusing it with reality.

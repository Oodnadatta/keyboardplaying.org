---
date: 2019-08-23T11:43:55+02:00
title: Défi du 2019-08-23
slug: defi-2019-08-23
author: chop
license: CC BY-NC-ND 4.0

description: "Micro-nouvelle rédigée dans le cadre d'un défi hebdomadaire. Phrase à utiliser : « [Il observait des golems éteints défiant des vagabondes mutantes.](https://twitter.com/whatisbot236/status/1163162237378408448) »"

challenge:
  type: hebdomadaire
  rules:
  - Format court.
  - "Phrase à utiliser : « [Il observait des golems éteints défiant des vagabondes mutantes.](https://twitter.com/whatisbot236/status/1163162237378408448) »"
---

Allongé sur son plaid, à l'ombre du grand chêne, il observait le ciel.
Une somnolence s'abattait sur lui et donnait aux nuages des apparences fantastiques.
Là, d'immenses robots, immobiles, tenaient leurs armes levées, menaçant des femmes aux cheveux hirsutes, dotées d'un nombre de membres supérieur à la normale, poussées par le vent dans des directions éparses.
Il observait des golems éteints défiant des vagabondes mutantes.

---
date: 2019-08-23T11:43:55+02:00
title: Challenge (2019-08-23)
slug: challenge-2019-08-23
author: chop
license: CC BY-NC-ND 4.0

description: 'Micro-story written for a weekly challenge. Sentence to use: "[He watched extinct golems defying mutant tramps.](https://twitter.com/whatisbot236/status/1163162237378408448)"'
challenge:
  type: hebdomadaire
  rules:
  - Short.
  - 'Sentence to use: "[He watched extinct golems defying mutant tramps.](https://twitter.com/whatisbot236/status/1163162237378408448)"'
---

Lying on his plaid, in the shade of the big oak, he was watching the sky.
A drowsiness had fallen on him and gave the clouds fantastic appearances.
There, huge robots, motionless, held up their arms, threatening women with shaggy hair and too many members, pushed by the wind in scattered directions.
He watched extinct golems defying mutant tramps.

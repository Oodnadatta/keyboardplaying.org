---
date: 2019-08-21T10:00:03+02:00
title: Le châtiment
slug: le-chatiment
author: chop
license: CC BY-NC-ND 4.0
---

Elle courait aussi vite que ses membres pouvaient la porter.
Le sol était lisse et n'offrait que peu d'accroche ; elle dérapa avant de parvenir à changer de direction pour passer l'angle.
Là ! Un espace pour s'abriter !
Elle se précipita dessous.
Non, elle n'était pas encore à l'abri !
Elle reprit sa course, entendant les rires sur son passage, jusqu'au moment où l'eau arrêta de la mouiller.

<!--more-->

Vexée des ricanements moqueurs, elle se dirigea vers un de ses abris, où elle était rarement dérangée.
Entreprenant de se sécher, elle médita sur la façon dont elle s'était laissée piéger.
Elle avait pourtant été prudente, mais n'avait pas résisté aux fragrances de la poubelle.
L'ouverture du sac était au sol, béante.

Elle connaissait les habitudes.
La plupart du temps, ils ne jetaient que des détritus.
Et la plupart du temps, les poubelles étaient mieux fermées pour s'assurer qu'elle ne puisse pas y accéder.
De temps à autre, cependant, son odorat l'incitait à jeter un œil plus attentif.
Il lui était arrivé de découvrir, au milieu des ordures, des mets raffinés que pouvaient apprécier les connaisseurs comme elle.

Elle s'était assurée que personne ne pouvait la voir.
Ensuite, furtivement, elle s'était abaissée et avait commencé à déplacer discrètement les déchets, à la recherche de l'origine du fumet qui chatouillait ses narines sensibles.
Accroupie, la tête dans le sac, elle savait qu'elle approchait de son objectif lorsque, soudain, quelque chose avait atterri sur son dos.

Elle n'avait pas pris le temps de comprendre de quoi il s'agissait.
Bondissant en arrière par réflexe pour se libérer du sac, elle avait reçu une deuxième giclée d'eau en pleine face.
Levant les yeux pour déterminer son origine, elle vit son serviteur bipède pointer vers elle l'outil d'arrosage qu'il avait de temps à autre l'audace d'utiliser sur elle.

Il fallait agir, pas réfléchir.
C'était une impasse, la seule issue était derrière lui et son engin de torture.
Elle était partie en courant.
Ses pattes avaient dérapé sur le sol lisse avant qu'elle ne puisse réellement démarrer.
Le bruit de ce départ en panique avait déclenché l'hilarité de l'autre laquais, dans la pièce voisine.
Elle avait contourné son larbin, fuyant le bruit du spray et les trombes d'eau qui la poursuivait.

Et ainsi, elle méditait.
Elle réfléchissait à la façon dont elle devrait les discipliner.
Ce n'est pas le serviteur qui décide où peut ou ne peut pas aller le maître !
Peut-être méritaient-ils plus qu'une simple sanction.
Pourquoi ne pas étriper l'un d'eux pendant leur sommeil ?

Elle devait pourtant leur reconnaître des qualités et une certaine volonté d'apprendre.
Ils ouvraient désormais la porte lorsqu'elle s'asseyait devant pour sortir.
Tout au plus avait-elle besoin de les appeler une fois.
De même, elle n'avait plus besoin de leur dire quoi que ce soit lorsqu'elle voulait un massage ; il lui suffisait de s'installer.

Mais d'ailleurs, ceci n'était pas une mauvaise idée !
Elle avait terminé sa toilette.
Son pelage était à présent sec et sa colère était un peu retombée.
Laissons les serviteurs se faire pardonner en prenant soin d'elle !

Après tout, la vie d'un chat est toujours digne de celle d'un pacha...

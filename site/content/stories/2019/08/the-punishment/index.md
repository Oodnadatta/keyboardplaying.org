---
date: 2019-08-21T10:00:03+02:00
title: The Punishment
slug: the-punishment
author: chop
license: CC BY-NC-ND 4.0
---

She ran as fast as she could.
The floor was smooth and offered little grip; she skidded when tried to change direction at the angle.
There! A shelter!
She rushed under it.
No, she was not safe yet!
She resumed her flight, hearing laughter on her way, until the water finally stopped falling on her.

<!--more-->

She was vexed with the mocking sneers and went to one of her hiding places, where she was rarely disturbed.
Beginning to dry herself, she pondered how she had let herself be trapped.
She had been cautious but could not resist the scent of the trash.
The opening of the bag had been gaping on the ground.

She knew the habits.
Most of the time, they only threw out rubbish.
And most of the time, the bins were tightly closed to make sure she could not examine them.
From time to time however, her sense of smell made her take a closer look.
She had already discovered, in the middle of garbage, some refined dishes that only connoisseurs like herself could fully appreciate.

She had made sure no one could see her.
Then, furtively, she had crouched and begun discretely moving the detritus, in search of the origin of the smell that tickled her sensitive nostrils.
Hunkered down, the head in the bag, she knew she was getting near when, suddenly, something had landed on her back.

She had not waited to understand what it had been.
As she reflexively jumped back to get free of the bag, the second squirt of water had hit her square in the face.
Looking up to determine the origin, she had seen her bipedal servant pointing on her the watering tool that he sometimes dared use on her.

She had had to act, not to think.
It was a dead end, the only way out behind him and his torture device.
She had run away.
Her paws had slipped on the smooth floor and she had made several steps before actually moving.
The mere noise of that panicky departure had triggered a roaring laughter from the other lackey, in the next room.
She had bypassed her stooge, fleeing the waterspout that pursued her.

Now, drying up, she was meditating, thinking of the way she would have to discipline them.
The servant does not decide where the master can or can't go!
Maybe they deserved more than a simple sanction.
Why not gut one of them during their sleep?

Yet, she had to recognize them some qualities and a certain will to learn.
They know open doors when she sat in front of one to get out.
In the worst of cases, she only had to call once.
Likewise, she no longer needed to tell them anything when she wanted a massage; settling down was enough.

What a brilliant idea!
She was done with her toilette.
Her fur was now dry and her anger, a little appeased.
Let the servants earn forgiveness by taking care of her!

After all, a cat's life is always worthy of a shah...

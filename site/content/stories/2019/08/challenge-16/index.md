---
date: 2019-08-16T12:09:52+02:00
title: Challenge (2019-08-16)
slug: challenge-2019-08-16
author: chop
license: CC BY-NC-ND 4.0

description: "Micro-story written for a weekly challenge. Theme: last words."

challenge:
  type: weekly
  rules:
  - Short.
  - "Theme: last words."
---

It was over.
He had nowhere left to run to.
At the top of a skyscraper, there was no hope for an exit or refuge.
Fleeing to the top, what a brilliant idea!
He turned to the bang of the door: his pursuers were coming, not in a rush anymore.
No need to hurry: their prey was doomed.
They could take their time to make him pay.
For the chase and everything before.

He however liked it quick.
Faking a last attempt at flight, he drew back, step by step, without taking his eyes off the others.
The back of his thigh hit the edge and he heard sneers around him.
He, on the other hand, smiled.
He hoisted one foot on the wall, then the other.
One of his torturers' grin melted off his face under the effect of an understanding tinged with confusion---or was it the opposite?

Another of his executioners took a plunge to catch him, but it was already too late: he had tilted back and, when he had been almost horizontal, had pushed on his feet.
As the building was passing him by at high speed, like a train seen from the platform, the wind lashing his back, he said, "Well, who'd have thought? So far, it's not that terrible."

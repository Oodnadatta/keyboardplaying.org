---
date: 2019-08-16T12:09:52+02:00
title: Défi du 2019-08-16
slug: defi-2019-08-16
author: chop
license: CC BY-NC-ND 4.0

description: "Micro-nouvelle rédigée dans le cadre d'un défi hebdomadaire. Thème à respecter : les derniers mots d'un condamné."

challenge:
  type: hebdomadaire
  rules:
  - Format court.
  - "Thème : Les derniers mots d'un condamné."
---

C'en était fini.
Il ne pouvait plus leur échapper.
Au sommet d'un gratte-ciel, il n'avait plus issue ni refuge.
Fuir vers le haut, quelle idée stupide !
Le bruit de la porte le fit se retourner : ses poursuivants arrivaient, tranquillement désormais.
Plus besoin de se presser ; leur proie étaient condamnée.
Ils allaient pouvoir prendre le temps de lui faire payer.
La course poursuite et tout le reste.

Mais lui préférait la brièveté.
Mimant une dernière tentative de fuite, il se recula, pas à pas, sans quitter les autres des yeux.
L'arrière de sa cuisse heurta le rebord et des ricanements se firent entendre autour de lui, mais un sourire se forma sur son visage.
Il hissa un pied sur le muret, puis le second.
Le rictus de l'un de ses tortionnaires se liquéfia sous l'effet d'une compréhension teintée de confusion --- ou était-ce l'inverse ?

Un autre de ses bourreaux esquissa un plongeon afin de le rattraper, mais il était déjà trop tard : il s'était laissé basculer en arrière et, presque à l'horizontale, avait poussé sur ses pieds.
Tandis qu'il voyait l'immeuble le passer à grande vitesse, comme un train vu depuis un quai, le vent fouettant son dos, il s'exclama : « Et bah ça va ! Jusqu'ici, ce n'est pas si terrible. »

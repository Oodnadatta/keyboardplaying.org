---
date: 2019-09-13T16:47:42+02:00
title: Challenge (2019-09-13)
slug: challenge-2019-09-13
author: chop
license: CC BY-NC-ND 4.0

description: "Micro-story written for a weekly challenge. Theme: cultural difference."

challenge:
  type: weekly
  rules:
  - Short.
  - "Theme: cultural difference."
  - "Personal rule: write only dialogue, do not include parenthical elements."
---

--- Oh, dammit! My poor head ...

--- Oh, hey, you're awake!
Take it easy, that was quite a fall.
Are you hurt?

--- My knee and head ache.
Ok, a nice bump on the top ...

--- Though no blood when I checked.

--- Indeed.
And down ... Ouch ...
Feels like a sprain.
I'll cobble a splint together.
How long was I out?

--- Not that long.
Barely five minutes, actually.
Here, this bar should be strong enough to brace your leg.

--- Thanks.
What happened to us exactly?

--- We thought we were walking on dirt, but that dirt was on top of a vast underground complex.
A ceiling gave out and we went through.
From what I could see and here, I think the structure was abandoned a while ago.

--- Great! No injury on your side?

--- Some bruises, but I'll recover.
We need to find a way out of here.
Our entry point is out of reach.

--- Ouch!
Well, that should do.
You said the complex is va ...
Oh, yeah, the view is dizzying.
Have you seen any way off this ... well, this walkway?

--- Three doors on this floor.
The black one is not to be considered.
The bridge is rickety on this side and probably wouldn't take my weight.

--- And mine even less when I'd ask you to help me through ...

--- Sooooo we must choose between a what-must-have-been-bright-red door and a faded crimson one.
It's funny.
Metal and leather seem to have been fashionable when this complex was built.
The architecture is something between the European industrial revolution and the steampunk fantasy universe, with touches reminiscent of the Russian military base of the Cold War ...

--- Please, let's stop here the architecture lesson ...

--- Right, sorry for digressing ...

--- ... and let's look for a way out of here.
Open the brown door!

--- Didn't I know you, I wouldn't fathom how fast you fall back into your habits at the slightest annoyance!
I like to be of service to my fellowmen, but I don't have to obey your orders.
I'm no military and you know that!
Well, let's see ...
By the way, why this door rather than the other one?

--- Well, it seems obvious.
The other one is red, and red means danger.

--- What do you mean, "red means danger?"
We don't know anything about that!
We don't know anything about the culture of this planet's inhabitants!

--- We know they're humanoids, and their blood is red.
Pouring blood is a bad sign, so red is a bad sign too, right?

--- Haven't you learned anything from me yet?

--- It seems not, seeing how you react.

--- Red is blood, yes.
But some cultures venerate it as a life force!
In that sense, the door that looked like coagulated blood looks more jeopardizing.

--- Ok, your arguments are legit ...

--- Each culture has its own references and interpretations, just like you and me don't look at something through the same filters, because we did not have the same experiences ...

--- ... We've had some in common, by now ...

--- ... It's something people tend to forget and, as a consequence, you only see things as you're used to; you can't see like others see it.
For instance, do you know why, when the globalization was the purpose of all great companies, some Western hypermarkets almost failed their implantation in China?

--- Because they were the wrong color?

--- Make fun of it if you like, but it's just that!
Our nice, pure white was the color of mourning in China.
Clients only came after local decorators installed red decorations all over.

--- Ah. And what was red to the Chinese?

--- It was a color for celebration.
It was related to the sun.
And, as for the third door, black could be a symbol of strength as well as one of disaster.

--- Ok.
So, according to you, we should forget about this door and focus on the other one?

--- ... No, not necessarily.
Just don't use the color as the sole criterion to choose.
Actually, let's not take it into account at all and be extra careful when opening it.

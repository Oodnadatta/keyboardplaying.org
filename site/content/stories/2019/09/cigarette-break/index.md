---
date: 2019-09-06T14:29:37+02:00
title: Cigarette Break
slug: cigarette-break
author: chop
license: CC BY-NC-ND 4.0

description: "<p>After a stressful job, nothing beats a cigarette to relax, but you'd better carefully choose where you smoke it ...</p><p>Micro-story written for a weekly challenge.</p>"

challenge:
  type: hebdomadaire
  rules:
  - Short.
  - "Theme: an infraction."
---

Gosh that feels good!
I really needed that drag, it was necessary.
All that stress ...
It went down smoothly, all things considered!
This was not wasted time.
But Hell, it feels good to unwind when it's over.

"You are smoking in a public space.
Your offense has been registered.
Please show your identification tattoo."

It made me jump.
I turn around, but I already know too well what's waiting for me, of course.
Everyone instantly recognizes the metallic quality of the police drones' voice.
Sneaky, moving without the slightest sound.
That is, until they find something "reprehensible" to point out to you.
Crap ...
Well, let's go for the ritual rhyme ...
And please let me keep it! I must stay calm.

"I ask for the indulgence of the police for the infraction.
A documentary check will indicate that the premises are not equipped with a standard smoking room, and I am the only occupant of the yard withing fifty meters."

The thinking light blinks; it's downloading info about the place.
In the meantime, it's flying around.
It finally comes back to me with an answer.

"The indulgence is refused.
An office window is open at 15.36 meters and a rat is present on the yard at 49.72 meters.
Please show your identification tattoo."

I don't have any word to describe the shock.
Only colors.
White.
And then, suddenly, red.

"Excuse me, WHAT?
Son of a bitch, it's a damn rat!
A pest, not a kid!
As for the office, they know there's a smoking spot!
The cigarette break takes place here, nowhere else!

--- You uttered an insult.
Your offense was registered as an aggravating circumstance.
Please show your identification tattoo.
Any repetition of this request will be registered as a refusal to comply."

Ok.
I calm down and pull up my sleeve so that sneaky robot can do its job and scan my right wrist.
Let's think a bit.
The law was designed to protect human and especially children.
Let's try to slow things down a little while it records the events in my file.

"Listen, you misheard me.
I didn't insult you.
I was speaking of the sun, the great yellow star in the sky, on a beach, the place with sand and waves."

The thinking light blinks again.
It's replaying the recording in its metallic head.

"Negative.
The recording is clear.
Both pronunciation and intention were explicit.

--- But please! No kid was there to hear ...

--- Psychological analysis indicates an 86.63% probability that this circumstance was irrelevant to your behavior."

Ok.
I'm done.
I'm cracking down, close to tears.
All this work, just to finish there ...

"For pity's sake!
I just performed a dangerous precision operation.
I spent five hours aligning the heads of an injector in a cold fusion reactor, without pausing.
I'm sure you know how trying this is.
I beg the indulgence of the police for the offenses and for my short temper.

--- The indulgence is refused.
The aggravated offense has been recorded.
Please report to the police station of your choice before the end of the day.
The sanction will be pronounced then."

It turns and flies away, disappearing beyond the yard walls.
Public space!
Anything that isn't a room in a building is public space, now.
Cold, heartless machine!
I know the damn punishment!
I worked hard to get this gig.
I poured blood and tears into it.
I'm qualified, I like doing it, and I'll lose it all for an unfortunate cigarette and four words I couldn't keep to myself.
I'll be in jail for a month.
No reprieve will be offered because of that damn curse.
In the current condition, they won't wait for me.
It was my first day, and it cost me the job.
I hope they'll at least pay what they owe me.
It would cost them tens of thousands had they called a consultant to do that, but I may get a carrot on top of everything else ...

---
date: 2019-09-07T13:39:37+02:00
title: Un sourire
slug: un-sourire
author: chop
license: CC BY-NC-ND 4.0

description: C'est tellement agréable, un sourire, mais, mal interprété, cela pourrait vous mettre dans des situations délicates.
---

Je descends du métro et remonte les couloirs de la gare pour atteindre le quai du train qui me permettra de quitter la capitale pour rentrer chez moi.
Il ne partira que dans une demi-heure ; j'ai le temps et j'en profite pour flâner un peu.

Au détour d'un couloir, je croise une patrouille de militaires.
Une femme et trois hommes, treillis kaki, béret sur la tête, famas en travers de la poitrine avec la main droite sur la poignée.
Même si l'arme n'est pas chargée, ils forment une vue impressionnante, à se déplacer ainsi de façon coordonnée sans avoir besoin de communiquer.
Comme toujours lorsque je croise une patrouille semblable, un sourire se forme sur mon visage.
Ce sourire m'attirera des ennuis, un jour.

Je suis déjà surpris de ne pas avoir plus souvent affaire à des « contrôles aléatoires » de police.
Avec ma gueule bien blanche, bien caucasienne, ma boule à zéro, la capuche de mon hoodie qui dépasse de ma vieille veste en cuir noir, mes mains dans des mitaines aux extrémités décousues et mon vieux sac troué en bandoulière, je me dis que je dois avoir l'air d'un drogué ou d'un néo-nazi.
Peut-être les deux.
Et pourtant, je semble donner confiance à ceux qui croisent mon chemin.

Quoique...
J'ai peut-être parlé trop vite.
L'un d'eux me regarde de travers alors que je passe sur le côté de leur groupe.
Je continue mon chemin, mon sourire idiot cloué aux lèvres, lorsque j'entends une voix derrière moi :

--- Excusez-moi, monsieur, est-ce que je pourrais inspecter votre sac, s'il vous plaît ?

--- Bien entendu, monsieur, réponds-je en me retournant et en attrapant mon sac afin de le présenter ouvert au soldat qui s'est adressé à moi.

Tout en conservant sa main droite sur la poignée de son fusil mitrailleur déchargé, il écarte un cahier et l'ordinateur pour voir les stylos et autres accessoires traînant au fond du sac, avant de le refermer.
Je vois à son air aigri qu'il est contrarié.
Sa main gauche retombe le long de son corps tandis que ses yeux se reconcentrent sur les miens.
Il se pose une question.
Certainement si je ne me fous pas de sa gueule.

--- Vous pouvez y aller, monsieur.
Désolé de vous avoir importuné.

--- Je vous remercie, monsieur.
Heu, monsieur ? tenté-je alors qu'il se retourne.

Il se retourne vers moi et me regarde.

--- J'imagine que vous vous demandiez d'où venait mon sourire ?

Il ressemble à un gamin pris la main dans la boîte à sucreries.

--- Bah... Oui, un peu.

--- Je ne me moquais pas de vous, si cela peut vous rassurer, bien au contraire.
J'admire toujours le savoir-faire de professionnel, qu'il s'agisse de l'artisan réalisant son œuvre ou du militaire se déplaçant avec discipline et efficacité.
Et vous passez votre temps à assurer notre sécurité, un petit sourire est bien le moindre des remerciements que l'on puisse vous adresser.
Bonne journée !

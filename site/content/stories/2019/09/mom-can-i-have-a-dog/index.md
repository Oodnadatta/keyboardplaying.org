---
date: 2019-09-05T08:58:57+02:00
title: Mom, Can I Have a Dog?
slug: mom-can-i-have-a-dog
author: chop
license: CC BY-NC-ND 4.0

description: "<p>Today begins like many days, until Klem asks his mother an unexpected question.</p><p>Short story written for a bimonthly challenge.</p>"

challenge:
  type: bimestriel
  rules:
  - Short story.
  - "Constraint: the text should include a duel or an opposition, and end happily (at least one positive aspect should result from the confrontation)."
---

After she had poured the milk into Klem's cereal bowl, Tilda turned to grab the pitcher.
The juicer was done with the oranges and the grapefruit she had put in a few minutes ago.
As she filled her son's glass, he raised his spoon to his mouth, a bit clumsily.
She put the pitcher down on the table and moved to the worktop with the stove.
The spoon tinkled against the bowl as Klem filled it again, the she heard the sound of a cereal falling limply on the table.
Nothing serious.

She took the four slices of reconstituted bacon Fris has bought for them and removed them from their reusable wrapping.
She was putting them down in the pan when the boy's voice rose from behind her back.

--- Mom, can I have a dog?

The question was so unexpected that she remained stunned for a few seconds.
She surely had misunderstood. Because of the noise of the meat she just had put on the fire.
Yeah, that was it!

As always when speaking with her son, she gave him her full attention.
She sat in front of him, leaning slightly forward.

--- I did not hear you well because of the pan.
Would you mind repeating?

--- Can I please have a dog?

Her back relaxed and fell against the chair.
She had not misunderstood.
Where could this idea come from?
Like all kids who grew up in arcology, Fris knew very little about the concept of pets and she knew that, like her son, he had never seen a dog.
As for her, if she had ever talkd to him about her child life on a farm, she was certain she had never discussed the topic of canine companions.

--- Where did you hear about dogs?

His face lit up as he gathered the memory to answer.

--- At school.
The teacher showed us movies about how people lived in the twentieth century.
In one of them, they had animals in houses.
Cats and dogs.
The film explained that dogs were wolves who had learned to live with humans.

Of course it was school!
This explained that.
Now, how could she respond to such a request?
Objectively, getting a dog would be difficult.
Taking care of it, even more.
Subjectively, she had absolutely no desire of having an animal in her life again.

Still, she and Fris always did everything so that Klem would develop his critical mind.
He was at that age when they could begin giving them responsibilities.
So far, they were quite pride of his education; Klem often seemed well ahead of his age when it came to debating and reasoning.
It was not uncommon for him to logically chain questions until his parents had no longer answers.

But not today.
Tilda did not want a dog in her home, but she would not break the habit by giving a categorical, unexplained answer.
Klem was no longer a child who accepted what adults said.
She wanted to encourage that independent spirit, not stifle it.
She hesitated a bit but went ahead.

--- But, you know, the twentieth century was a long time ago.
Life has changed much since.
For starters, there were much more dogs back then.
It was pretty easy to find one.

--- Lexx said her neighbor is a veterinarian.
She let her come to the clinic and she's seen plenty of dogs!
And also cats, and some other animals.

--- Ah. And did Lexx talk about the formalities before adopting a dog?

--- Yes, the teacher asked her if she knew.
She said you'd have to fill papers to prove you're able to take care of the dog.

--- And are you sure we could do that?

Fris entered the kitchen and muttered a vague "hello" which was almost a grunt.
Tilda knew he would not get to a higher level of communication before he started eating.
Klem knew that too, obviously, and he answered his mother just as if they had not been interrupted by his father, while the latter was moving toward the stove.

--- Yes, I'm sure!
Lexx said that, for most families, you just have to prove one of the parents has a C job.
Dad's got a B job and yours is A, so that wouldn't be any trouble at all!

Fris, who had flipped the bacon in the pan, groaned just then.
Since he never had been susceptible about her working in a higher category, Tilda supposed it had stayed too long on one side.
Fortunately, it had not burned!
Fris's morning mood would have endured for most of the day.

Klem watched his mother, who was thinking about the conversation while listening to what her husband was doing in her back.
She resolved to continue with her son as his father was emptying the pan into the plates and grabbing the eggs he was about to break.
Maybe if she enumerated all the constraints and drawbacks of having a dog, one of these would be an argument to convince Klem.

--- OK, let's admit we get a dog.
You know it can't use humain toilets and it has to be walked on a regular basis.
Who will do it and where?

--- I can take care of it!
If I get up early in the morning, I can do it before school.
At noon, you could pick me up with him, and at the end of day too.
Lexx said the dogs can walk around the parks.
You just have to pick up their poop and take one of the citizen moto-cleaners if they relieve themselves elsewhere.

Tilda could not repress a small laugh, but it was covered by the noise of Fris scrambling the eggs in the pan still greased up by the bacon.

--- But if I come with the dog and it cannot hold it, _I_ will have to take care of it!
And since you're too young to borrow moto-cleaners, it will always fall down onto your father or myself, won't it?

This sentence was followed with a silence.
Klem's gaze was focused above and behind her, which told her her husband had turned to that last statement.
Klem focused on his mother again as the spoon began picking on the eggs again.

--- I'd like to do it.
It's only because of the arcology's rules that I can't do it alone,but I'll do it with you!

--- You'd better!

She said the last with a smile.
She heard the coffee machine start up and grind the beans; Fris was about to serve breakfast.
Klem knew the conversation had to subside for now and began again meticulously eating his cereals, now soggier than ever.
The induction cooker beeped as Fris turned it off and poured the eggs in the plates, near the bacon.
Tilda took the cue.
She grabbed the pitcher and filled both their glasses with orange juice.
The coffee started trickling while Fris carried the plates to their places in front of their seats.
He barely had time to reach the coffee maker when it stopped, the cups full.
He brought them to the table.
He placed one beside Tilda's plate before letting himself fall on his chair, the mug still in his hand.
He swallowed a big sip and put it down, quickly grabbing his fork and knife, and focusing intensely on his plate.
Both of the men of her life were both concentrating on their breakfast, but Tilda chewed hers without really paying attention to it, lost in her thoughts.

When his bowl was over, Klem put his spoon in it, stood up, and took it into the dishwasher, before leaving the room to go and prepare for school.
His parents watched him silently and smiled.
After she heard his bedroom door close, Tilda turned to her husband.

--- What do you think?

He answered without emotion.

--- That I don't want to interfere.
You're the only one here who has any idea what having a dog means.

--- Exactly.
Klem's right about the fact that we live comfortably.
Finding time and money to take care of a dog wouldn't be a problem.

--- I can see you're not fond of the idea.
Weigh the pros and cons carefully.
Tell him about the difficulties you're thinking about and not sharing.
Is it because you don't want to take care of it ourselves?
Because I admit the perspective of getting out several times a day and collecting dog droppings is just mildly ...

--- No, that's not it. It's just that ...

She let it hang for a few and turned to the door as she heard Klem's footsteps heading for the kitchen to join them.
He stopped on the room's threshold and piped happily:

--- Mom, I'm ready for school!

--- I can see that!
Let's get on the way, then.
Will you kiss daddy goodbye?

Their son went to his father and dropped a kiss on his cheek while Tilda grabbed her bag in the entrance.
She shouted from there as Klem joined her.

--- Have a nice day, honey!

Her son took her hand and they left together.
They went through the door, walked to the end of the street and stopped in front of a double sliding door.
Tilda announced their destination ("Stephen Hawking Elementary Course") on the screen that adorned one of them, then turned to her boy.

--- Do you really want a dog?
Wouldn't you rather have a Manta, like Niels's?

She remembered her son's best friend's birthday party.
When she had gone to get Klem, she had discovered the companion drone he had received as a gift.
It was shaped after marine rays, but the device levitated and followed the child it had been bound to.
Niels's normally glowed white, only slightly tinged with blue.

Klem raised his eyes, his head slightly tilted to the right, intensely thinking.
And then looked at his mother again.

--- A Manta's a machine. It's not the same!

--- But it understands you.
You saw how it reacted to what we said when it was around.
You saw the tricks Niels made it do without even having to teach it!

Mantas had a virtual intelligence that allowed them to express pseudo-emotions.
A compliment made it shudder and turn bright blue, all the more if it came from Niels himself.
On the contrary, when Tilda had asked the boy if he was happy of his robot, it had manifested his outrage at the word by turning red and swinging left and right; the reaction was unambiguous: it was not just a machine.

--- But a Manta can't be your friend.
A dog can.

The double door slid open and they climbed aboard the large cabine, already half-occupied, without stopping their conversation.

--- A dog cannot go to school with you, while the Manta could.

--- When we start class, the teacher orders all Mantas to go to sleep and they just obey.
Niels tried to wake Vobby, once, but she listens to the teacher more than him.

--- Well, at least, it ... she listens.
You'll have to teach a dog everything.
To behave, to do what it's told, to releave himself only at authorized spots ...
It's difficult, and it takes time before it knows it.

--- But Mom!
That's just normal!
That's how we know it's not a machine!
And ...

At that point, he looked directly into his mother's eyes.
He didn't just look at the face of the person he was speaking to, he was seeing beyond.
Or at least, that's how it felt to her.
The cabin changed direction, passing from vertical to horizontal move.
This combined with the intensity of the link between Tilda and her son to make her feel dizzy.

--- ... you have to teach me too.
Sometimes, I'm wrong or I do things I'm not supposed to.
You tell me what's right or wrong.
I'd do the same with the dog.
And even when you tell me off, I love you.
And even when Niels and I argue, we're still friends.
That'd be the same with a dog.

Tilda managed to free herself of her son's gaze but was at a loss for words.
Could he really be only six?
Klem did not insist and just waited for the end of their journey.
The cabin made several stops where passengers went in or out.

Tilda couldn't share her own trauma.
Back on the farm, her parents had several dogs but her favorite was Fogue, a gigantic Beauceron.
He was in the family before she came into the world and adopted her from birth.
She had ridden him like a horse.
Once, her father had made her scream while tickling her and he had interposed, his fangs bared.
She could have done anything to him!

But one day, just before she was ten, she came back from a field trip, she found Fogue had changed.
He did not answer his name anymore, and their usual games did not seem to interest him anymore.
He was Fogue, but he behaved like another dog.

She had to wait for two weeks before her parents told her the truth.
Pursuing a rabbit, Fogue had run in front of the wheels of the tractor.
Tilda's parents did not know how to break it to her.
To avoid having to, they prefered to have Fogue cloned and paid for accelerated growth, so that they'd have an identical dog when she came back.
They were no scientists and did not know that Fogue's memories and personality would not be transfered to the clone.
That they would just have another dog, physically identical to the first one.

Tilda did not go near the clone ever again.
The loss of Fogue and the treason of her parents had devastated her, and she had promised never to bind with an animal.
As soon as her studies allowed it, she left the familial farm and got a life inside an arcology.

She emerged from these sad memories when she felt Klem's arm pull her out of te cabin, in front of the school.
He turned to her and kissed her.

--- Goodbye, mom! Have a nice day!

--- Have a good day.

She felt stupefied.
Like in a fog, she watched her son go away, her mind divided.
She could afford to arrive at work later today, and decided to call Lexx's parents for contact information of their neighbor's veterinary practice; she would certainly have good advice for people in her situation.
Facing the sliding doors again, she spoke the address she had just received and waited.

The ride was not long---but no trip was really that long in an arcology, unless you wished to avoid the lift and preferred to walk.
Tilda got out of the cabin and saw the clinic was just beside the light bow, the windows that allowed the sun to light the interior of the arcology throughout the course of the day.
It had been years since she had been this close to outside light.
She thought of the farm.
Her parents came to visit her, but the opposite was not true.

She turned around and saw a little girl walking toward the practice with her parents.
She held both arms forward with an adorable, barely weaned puppy resting on them.
It had a tousled, sandy-colored fur, paws that seemed enormous for its tiny body, and large, sparkling, curious eyes.
Tilda knew she had no need to see the vet any more.
Her decision was made.
Klem would have a dog ...

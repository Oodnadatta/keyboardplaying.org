---
date: 2019-10-27T23:04:52+02:00
title: Je ne suis pas Jésus
slug: je-ne-suis-pas-jesus
author: chop
license: CC BY-NC-ND 4.0

description: "<p>Tim ne pensait pas retrouver les bancs d'une église après toutes ces années. Le revoilà pourtant dans l'église de son enfance.</p><p>Micro-nouvelle rédigée dans le cadre d'un défi hebdomadaire.</p>"

challenge:
  type: hebdomadaire
  rules:
  - Format court.
  - "_Song-fic_ (texte écrit d'après une chanson). Chanson choisie : [_I'm Not Jesus_](https://youtu.be/PMLsF8ajI6U) d'[Apocalyptica](https://www.apocalyptica.com/)."
---

« Laissez les enfants venir à moi, ne les empêchez pas, car le royaume de Dieu est à ceux qui leur ressemblent.
Amen, je vous le dis : celui qui n’accueille pas le royaume de Dieu à la manière d’un enfant n’y entrera pas. »

Les paroles du prêtre lisant l'évangile sortirent Tim de son songe éveillé.
Cela faisait des années qu'il n'était pas entré dans une église.
Jamais il n'aurait imaginé remettre les pieds dans celle-ci.
Rien n'avait changé. Pas même le curé.

Enfin, il avait pris quelques années, quelques kilos et perdu quelques cheveux, mais c'était toujours lui.
Toujours celui qui s'obstinait à l'appeler « Timothée » parce qu'il s'agissait d'un prénom biblique, alors que tout le monde s'arrêtait à la première syllabe.
Tim n'avait plus jamais été à l'aise avec son prénom entier après cette époque.

La messe se terminait et les fidèles quittaient l'édifice pour rejoindre leur foyer, saluant le prêtre au passage.

« Bonsoir, mon père. Passez un bon dimanche. »

D'autres souvenirs remontèrent. Il entendit le prêtre lui répondre :

« Appelle-moi "papa". "Mon père", c'est pour les adultes. »

Non.
Ce n'était pas normal.
Aucun père n'aurait fait ça à son enfant.

L'église était vide.
Tim était à présent seul.
Il n'avait pas quitté son siège, la tête penchée en avant, les yeux fermés.
Les pas du prêtre résonnaient tandis qu'il remontait l'allée centrale en direction de la sacristie.
Puis il entendit sa voix, aussi mielleuse que dans sa mémoire :

« Puis-je vous aider, mon fils ? »

Tim releva la tête et regarda son interlocuteur, avant de se lever et lui faire face.

« Bonsoir, mon père, dit-il.
Non, merci.
Je passais dans la région et je souhaitais assister à un service dans l'église de mon enfance.

--- L'église de votre enfance ? demanda le père.
Vous semblez encore bien jeune.
Se pourrait-il que je vous aie déjà vu sur ces bancs ?

--- Pas sur ces bancs, mon père.
Derrière l'autel, avec vous.
J'étais enfant de chœur. »

Le curé pencha la tête, comme pour mieux chercher dans ses souvenirs.
Puis il le reconnut et l'ombre d'une angoisse passa sur son visage.

« Timothée ! Que fais-tu ici ?
Je veux dire, je ne m'attendais pas à ta visite !

--- Je suis venu reprendre le contrôle de ma vie, répondit l'intéressé en abandonnant tout faux semblant.
Elle n'a jamais été ce qu'elle aurait dû être à cause de vous.

--- Mon enfant ! s'exclama le curé, dont la peur croissait visiblement.
Je n'ai jamais rien fait pour te nuire !
Dieu est amour, j'ai simplement voulu partager cet amour avec toi !

--- Ce n'est pas de l'amour que j'ai reçu. » siffla froidement Tim.

Le prêtre tentait de reculer, mais se heurta aux chaises de l'autre côté de l'allée.
Les larmes aux yeux, il regarda le visage du jeune homme qui aujourd'hui le surplombait.

« J'ai pêché, je le sais. J'ai fait des choses horribles. Pourras-tu un jour me pardonner ? »

Tim s'immobilisa, puis il sembla se détendre et un sourire apparut sur son visage.
Il écarta les bras en disant :

« Jésus nous a enseigné le pardon. »

Hésitant, décontenancé par le changement d'attitude, le curé se releva néanmoins.
Tremblant, il s'approcha pour répondre à l'étreinte.

Tim resserra ses bras autour de lui, puis son bras droit descendit vers sa poche.
Il enfonça la lame dans la chair de son abuseur et murmura à son oreille :

« Je ne suis pas Jésus. Je ne pardonnerai pas. »

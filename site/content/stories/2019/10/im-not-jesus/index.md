---
date: 2019-10-27T23:04:52+02:00
title: I'm Not Jesus
slug: im-not-jesus
author: chop
license: CC BY-NC-ND 4.0

description: "<p>Tim didn't expect to see the interior of a church again. Yet, here he is tonight, inside the one of his childhood.</p><p>Micro-story written for a weekly challenge.</p>"

challenge:
  type: weekly
  rules:
  - Short.
  - "Song-fic (text written from a song). Chosen song: [_I'm Not Jesus_](https://youtu.be/PMLsF8ajI6U) by [Apocalyptica](https://www.apocalyptica.com/)."
---

"Let the little children come to me; do not stop them; for it is to such as these that the kingdom of God belongs.
Truly I tell you, whoever does not receive the kingdom of God as a little child will never enter it."

The priest's words reading the gospel brought Tim out of his waking dream.
It had been years since he had last waled into a church.
He would never had thought setting foot in this one.
Nothing had changed. Not even the cleric.

Well, he had a few more years, a few more pounds and some less hair, but it was still him.
Still the man who insisted on calling him "Timothy" because it was a biblical name, though everyone else only used the first syllable.
Tim had never been at ease with his full first name after this period.

The mass was ending and the faithful were leaving the building to return to home and hearth, saluting the priest on the way.

"Good night, Father. Have a nice Sunday."

Other memories came back.
He heard the priest answer him, "Call me 'Dad.' 'Father' is for the adults."

No.
It was not normal.
It was not how it was supposed to be.
No father would do that to his child.

The church was empty.
Tim was alone now.
He was still on his seat, his head bowed, his eyes closed.


L'église était vide.
Tim était à présent seul.
Il n'avait pas quitté son siège, la tête penchée en avant, les yeux fermés.
The priest's footsteps echoed as he walked up the aisle toward the sacristy.
Then he talked, with a voice as honeyed as Tim remembered.

"Can I help you, my son?"

Tim looked up at his interlocutor for a few seconds before getting up and facing him.

"Good evening, Father.
No, thank you.
I was in the aera and wished to attend a service in the church of mmy childhood."

The priest looked intrigued.

"The church of your childhood?
I must say those don't look _that_ far from now.
Could it be that I have seen you on these benches already?"

"Not on these ones, Father," Tim replied.
"I was behind the altar, with you, as a choir boy."

The cleric tilted his head, as if it would help him remember.
Then he knew him and anguish passed over his face.

"Timothy! What are you doing here?
I mean, I didn't expect to see you!"

"I came to take back my life," he replied, without any trace of smile or warmth left.
"It was never what it was supposed to be because of you."

The priest was visibly scared as he said, quite unconvincingly, "My child!
I've never done anything to hurt you!
God is love, I only wanted to share this love with you!"

This did not calm Tim, but rather sent him into a cold rage.
He was barely audible as he hissed, "It's not love I received."

The abuser tried to back off, but stepped into the chairs on the other side of the aisle.
With tears in his eyes, he looked up at the face of the young man who was now taller than him.

"I sinned, I know it.
I did horrible things.
Will you one day forgive me?"

Tim stopped.
He suddenly seemed to relax, some warm and smile coming back to his face.
He spread his arms wide, saying, "Jesus taught us to forgive."

The priest was disconcerted by the change of attitude but he got to his feet nevertheless.
This was an opportunity not to miss.
Hesistant, trembling, he approached his past victim to meet the hug.

Tim tightened his arms around him.
Then, his right arm went down to his pocket.
He plunged the blade into the flesh of his rapist and whispered in his ear:

"I'm not Jesus. I will not forgive."

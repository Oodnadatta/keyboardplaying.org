---
date: 2019-12-14T22:27:07+02:00
title: Shudder Before the Beautiful
slug: shudder-before-the-beautiful
author: chop
license: CC BY-NC-ND 4.0

description: "<p>Stanley's experience is a success! A great success, at that. Great enough that he can indulge in a reverie the beauty of which can't let him indifferent.</p><p>Micro-story written for a weekly challenge.</p>"

challenge:
  type: weekly
  rules:
  - Short.
  - "Song-fic (text written from a song). Chosen song: [_Shudder Before the Beautiful_](https://youtu.be/7oQ8sNSYXmQ) de[Nightwish](http://nightwish.com/)."
---

Stanley couldn't believe his eyes.
Of course, he had designed his experiences to achieve a result such as the one in his hand, but he had barely dared to dream about success until today.
Yet, the facts were here, laid down on paper: within his sample, initially a mixture similar to what the primitive atmosphere must have been, components had appeared.
Amino acids, the components of proteins; sugars; lipids; even nucleic acids, the building bricks of DNA!
From toxic molecules, he had managed to create the precursors of life.
His hands and the trembling sheet they were holding fell on the counter as he slumped onto his laboratory stool.
He fell in a reverie where he could see Earth right after its creation.

The atmosphere differed from what he knew today.
In fact, it was not even breathable: methane, ammonia, water vapor, carbon dioxide and hydrogen sulfide.
This primordial soup evolved under the effect of the different energies it received, especially ultraviolet rays from the sun, and the basic monomers reacted and became other compounds.
The molecules lengthened, grew more complex and arranged themselves into proteins or fragments of ribonucleic acid.
The power of chance if often deeply underestimated.
From the initial chaos, structured forms emerged, infinitely richer and more stable.
This was, however, only the beginning of a long series of transformations.

The lipids assembled to create walls, delimiting cellular structures.
Deoxyribonucleic acid entered into competition with its cousin and, more stable than the latter, it gained a dominating place.
Through the information stored on these supports, the chemical reactions within these cells became programmed operations, sequences of ordered and repeatable instructions.
Among these, there was an order to replicate this information, allowing cells to multiply.
Like human operators performing precise operations on an industrial chain, enzymes sometimes missed, resulting in transcription errors.
The consequences could range from minimal to devastating: it was all about the life plan of a cell, from birth to death.
In some cases, it became illegible and the cell was simply not viable.
In others, the difference went unnoticed.
There remained the most interesting case: change.

Certain cells distinguished themselves from their ancestors because of these erroneous transcriptions.
They could develop their own features and sometimes capacities that gave them an advantage in their environment.
Being a better fit, they multiplied faster and more than their ancestors.
The evolution continued.
Cells began to collaborate, to work in groups.
Multicellular organisms appeared.
Underwater plants came first and developed photosynthesis, allowing them to transform carbon dioxide and release oxygen.
The plants reached the surface and finally the land.

Ultimately, a form of consciousness emerged.
Basic nervous systems evolved rapidly to give birth to brains.
The organisms who acquired those also gained members: fins to explore the seas, then legs to scout the land and at last wings to take off to the skies.
Evolution and selection struggled continuously and it seemed to Stanley that life exploded before his eyes into endless forms most beautiful.
He shuddered before this vision.

---
date: 2019-12-21T14:56:05+02:00
title: Challenge (2019-12-20)
slug: challenge-2019-12-20
author: chop
license: CC BY-NC-ND 4.0

description: "<p>It's 2 AM and I hear a sound in the chimney. I know it's not Santa. I'm absolutely certain of it because _I_ killed Santa Klaus.</p><p>Micro-story written for a weekly challenge.</p>"

challenge:
  type: weekly
  rules:
  - Short.
  - "Imposed beginning: \"It's 2 AM and I hear a sound in the chimney. I know it's not Santa. I'm absolutely certain of it because _I_ killed Santa Klaus.\""
---

It's 2 AM and I hear a sound in the chimney.
I know it's not Santa.
I'm absolutely certain of it because _I_ killed Santa Klaus.
His body is right here, by my feet.

And yet, who else but the old boy could want to come in that way?
Even his elves, when they need to leave the North Pole, prefer portals than risking themselves through those pipes and possibly land into a lit fire.

A ululation!
It was just an owl.
I hate how these pipes make you hear the sound as if it comes from inside the room.

Let's move!
I still have a job to finish here.
The worst that could happen to me would be to wake up the house's inhabitants.
There's one rule above all others in our job: don't be seen.
My old partner could tell you tales for hours about what he had to face because of his lack of discretion...

But I got him!
He fell into the trap, just as expected.
The time I spent working on my plan was rewarded.
He's had me too many times, won too often.
I really needed this one!

I had my list for that night.
I had copied the last house at the bottom of his own.
That left me enough time to take care of my targets and then come and wait for him here.
He didn't suspect a thing.
The deed was done in a breeze.

Ah! Yeah, that was great work, but I need to get moving now.
Let's make him disappear and go home.
Come on, old boy, in the basket!
Yeah, as incredible as it sounds, he will fit in entirely.
That reminds me, he never thanked me for teaching him how to make containers that are bigger on the inside.
And still, he uses his hood every single year.

I miss our old walks throughout the country, when he was still wearing his bishop's clothes and I was following him.
We could discuss for hours on the best way to save these young souls: is it better to reward the deserving or punish the villains?
I could hit them with birch branches back then, and they were the ones I loaded into my basket.
And the effect the mere noise of the chains and bells he made me wear had on women and children! Ah!

C'm'on! Enough nostalgia, already!
Let's go!
I am impatient to see him resuscitated so that we can sit around our traditional schnapps!

Oh, I almost forgot my other target under this roof...
It's that sock, I think.
Yes, that's right!
I have a beautiful one left, just for you.
There, a nice piece of coal.
If you'd rather have a gift from old Nikolaus next year, you'll listen better to your parents and you'll stop bothering your little comrades.

A corner of shadow where I can disappear?
Here, perfect!
That's a whole lot more practical and classier than the old man's craze for fireplaces!

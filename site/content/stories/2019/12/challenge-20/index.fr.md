---
date: 2019-12-21T14:56:05+02:00
title: Défi du 2019-12-20
slug: defi-2019-12-20
author: chop
license: CC BY-NC-ND 4.0

description: "<p>Il est 2 heures du matin et j'entends du bruit dans la cheminée. Je sais que ce n'est pas le Père Noël. J'en suis absolument certain car c'est moi qui ai tué le Père Noël.</p><p>Micro-nouvelle rédigée dans le cadre d'un défi hebdomadaire.</p>"

challenge:
  type: hebdomadaire
  rules:
  - Format court.
  - "Début imposé : « Il est 2 heures du matin et j'entends du bruit dans la cheminée. Je sais que ce n'est pas le Père Noël. J'en suis absolument certain car c'est moi qui ai tué le Père Noël. »"
---

Il est 2 heures du matin et j'entends du bruit dans la cheminée.
Je sais que ce n’est pas le Père Noël.
J'en suis absolument certain car c'est moi qui ai tué le Père Noël.
Son corps est là, à mes pieds.

Pourtant, qui d'autre que le vieux joufflu pourrait bien vouloir passer par là ?
Même ses elfes, quand ils doivent se déplacer, préfèrent créer des portails que de se faufiler dans ces conduites et risquer d'atterrir dans un feu allumé.

Un ululement !
Ce n'était qu'une chouette.
J'ai horreur de la façon dont ces tuyaux renvoient le son comme s'il venait de la pièce.

Allez, je dois me dépêcher de terminer mon boulot ici.
Le pire qui pourrait m'arriver serait de réveiller les occupants de la maison.
S'il y a bien une règle d'or dans notre métier, c'est de ne pas se faire voir.
Mon vieux partenaire pourrait en raconter des pas mal sur ce qui a pu lui arriver à cause de son manque de discrétion...

Mais je l'ai eu !
Il est tombé dans le piège, comme prévu.
Reconnaissons que j'ai passé du temps à roder mon plan.
Il m'a eu trop de fois, a remporté trop souvent la victoire.
J'avais vraiment besoin de celle-ci !

J'avais ma propre liste, pour cette nuit.
J'ai copié la dernière maison dessus en bas de la sienne.
Cela me laissait le temps de m'occuper de mes cibles puis de venir l'attendre ici.
Il n'a rien suspecté et ç'a été un jeu d'enfant de l'avoir.

Ah ! Allez, assez d'autocongratulations, il faut le faire disparaître et rentrer.
Allez, le grand rouge, dans mon panier !
Aussi incroyable que ça paraisse, oui, il entre tout entier.
Il ne m'a d'ailleurs jamais remercié de lui avoir appris à faire des contenants plus grands à l'intérieur.
Ça le dépanne pourtant bien, avec sa hotte !

Que nos vieilles parades me manquent, quand il défilait dans les contrées avec ses habits d'évêque, moi à sa suite !
Nous pouvions discuter des heures durant sur le meilleur moyen de sauver ces jeunes âmes : valait-il mieux récompenser les méritants ou punir les vilains ?
Je pouvais les frapper avec mes branches de bouleau, et c'étaient eux que je chargeais dans mon panier.
Et l'effet qu'avait sur les femmes et les enfants le simple bruit des chaînes et cloches qu'il me faisait porter à l'époque !

Allez, trêve de nostalgie.
Rentrons !
Je suis pressé de le ressusciter pour qu'on puisse s'asseoir autour de notre traditionnel schnaps !
Diable ! Il a aussi pris énormément de poids depuis cet âge !
Mais c'est bon, c'est chargé.

Oh, j'ai failli oublier mon autre cible sous ce toit...
C'est cette chaussette, je crois.
Oui, c'est bien ça !
Il m'en reste un beau, rien que pour toi.
Voilà, un merveilleux morceau de charbon.
Si tu préfères un cadeau du vieux Nicolas l'an prochain, tu écouteras mieux tes parents et arrêteras d'embêter tes camarades !

Un coin d'ombre où disparaître ?
Parfait !
C'est quand même plus pratique et plus classe que sa manie des cheminées !

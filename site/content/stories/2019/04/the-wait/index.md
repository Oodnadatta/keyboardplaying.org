---
date: 2019-04-01T10:52:27+02:00
title: The Wait
slug: the-wait
author: chop
license: CC BY-NC-ND 4.0

description: "<p>She waits, like every time she needs it, but she's losing patience.</p><p>Short story written for a bimonthly challenge.</p>"

challenge:
  type: bimonthly
  rules:
  - Short story.
  - "Theme: the sleep."
---

She's beginning to lose her patience.
Of course, she knows it won't be any help: it never comes when she's angry.
But tonight, she needs it.
She did everything she could to prepare for her exam tomorrow.
Now it's up to it.

But it's just like keys: it's always when she needs it the most that it plays hard to get.
Actually, it always never comes when it's waited upon, just as if it likes its coming to be a surprise.
She must admit, it's got quite a talent to join her without her realizing it.

She turns over, her move made abrupt by the frustration she's trying to vent by doing so, all in vain.
Time passes and, still, no sign of it.
Again, she's growing impatient.
It's no use!

A herbal tea would probably be of help.
She gets up and goes to the kitchen.
The teapot is upside-down on the drainrack.
She grabs it and fills it with water before putting it down on the hotplate, which she turns on.
While the water is heating, she prepares a blend of verbena and linden, and adds two spoonfuls of honey at the bottom of her mug.

The teapot is starting to whistle.
She turns off the plate and pours the steaming water into the mug before submerging her infusion bag.
Turning her eyes up, she watches her student housing, which she can see almost wholy without moving.

The kitchen, a passage barely wide enough to open the drawers and other furnitures where she stores her dishes and food, includes only one worktop, with its sink too close to the hotplate to be compliant with any electric security standard.
Her mug is resting on a bar above the workplan, and beyond which she sees the main room: her office-living room-dining room.
She kept the sofa bed and the easy chair of the previous occupier.
They're both of Swedish construct and she bought it for the third of the original price.
The sofa bed is just under the bar, in front of a coffee table.
Well, what she calls a coffee table is just a plank on sawhorses with adjustable height, but it does the job.
The chair is also turned toward the table, on the left of the sofa.
And on the left of the sofa, another plank on higher sawhorses is her desk.

The bag on her desk gives her an idea.
She goes around the bar and goes to it.
There, she extracts the book she uses to occupy the twenty minutes of bus she has to endure each morning and evening.
Transportation repels many of her comrades, but she enjoys the cut in her day.
It's an occasion to take her mind off school.

Which is exactly what she needs: take her mind off it!
She puts the book down on her coffee table and inspects the mug on the bar.
The color looks great.
She carries it to her nose and smells the aromas.
It's perfect!
She uses her spoon to take out the bag ond press it against the rim in order to extract all the liquid, before throwing it in the trash can.
Missed!
Too bad, she'll pick it up tomorrow.

She stirs consciently to dissolve all the honey, which was mostly staying idly at the bottom, and raises the mug to her lips to taste the result of her preparation.
It's too hot!
She put it down on the table, grabs the book instead and sits down into the easy chair, which is reserved to her rare moments of relaxation.

She opens at the page indicated by her bookmark, ready to read, but stops there, perplexed: the text is upside-down.
She is sure she was holding it correctly.
Puzzled, she closes the book to examine its cover.
Up _is_ up, except for the title.

She is obvioulsy getting tired, but still has to wait for it.
She takes a look at her watch.
Night has fallen, but it's not that late yet.
She can go and have a walk around the neighborhood to empty her head.
The tea will be at an ideal temperature when she'll come back and she may take a hot shower---if 37 °C is "hot".
It shouldn't take long to come after that.

She puts a light jacket and her sneakers on, and leaves her student room, locking it behind her.
The elevator is out of order, of course, but she always takes the stairs anyway.
She never really got over the day when she had been stuck alone for nearly three hours.
She was seven.

Out of the residence, she goes immediately right.
There's not much to see in the streets around.
Only buildings, each grayer than the next.
The student house is obviously the most recent building around and was certainly the whiter when it was just finished.
Today, it's become yallowish and the shadow of windowsills seems to be printed on the facade.

But there's a zone, a few hundred of yards further, where some trees and a bit of green persist.
Of course, a non-so-small part of this square was transformed into a dog pen, so that these proud companions can take it off somewhere else than on the sidewalks, but that contact with nature still always feels good to her.

She arrives at an intersection and goes right again.
The little square of greenery is right where it should be.
The public lighting does not totally go through the foliage of the trees and the buildings around, making it an isolated island in the middle of this vast urban ocean.
Sitting down on the bench, she close her eyes.
The grass under her feet and the dew starting to gather, the scarce light, the rustling of the leaves in the light breeze ...
Here's for some relaxation!

But something isn't right.
She can't explain it, but she doesn't feel alone.
Like something is lurking in the shadows.
That noise! That wasn't the leaves ...

She gets up and faces the noise, in the darker corner of the square, but she can only see darkness.
Her eyes pinned to the black area, she realizes how nonsensical things are: her own shadow points right to a total absence of light.
It should illuminated by the street lamp in her back.
Yet, defying all logic, nothing can be seen in that absolute obscurity.

Then, under her very eyes, the shadow starts spreading.
Not like if the lights were dimming, no.
Rather as if that area of shadows extended immaterial tentacles.
Her brain feels paralyzed by the absurdity of the situation.
She's a rational person, she's studying to become a scientist, but what's unfolding before her is beyond her understanding.
One of the tentacles is about to reach her shadow.
Her deepest instincts kick in.
Right now, it's fight or flight!
And facing an adversary so unknown, so intangible, flight is the only logical response!

She leaves and runs as fast as she can toward the residence, but she doesn't recognizes the streets.
It's as if she suddenly transported to the other side of the city.
She runs, not daring to turn back, and summons all her cognitive abilities.

Shadow is only the absence of light.
It cannot exist in a luminous zone and it's not a living entity with tentacles.
Furthermore, she could not at one spot and another one the next instant.
Unless ...

She can think of only one solution to this mystery, but how to be sure ...
There! She just turned at the corner of a street and sees the student residence right in front of her.
She rushes to the entrance, breathless.

The door is closed, her keys seem to have disappeared and the intercom has been broken for weeks.
She looks behind her, at the way she just took, and sees the shadow appearing at the corner, just a few hundreds of yards from her.
Raising the eyes to her studio apartment on the second floor, she sees the window of her bedroom remained open.
In desperation, she starts climbing the facade to reach it.

Reaching the first floor is not hard.
The guardrail on each window are excellent holds.
Despite the climbing lessons she's had from a young age, she would never try such a feat normally, but she feels that shadow is menacing enough to go beyond usual caution.
Holding to the sides of the window, she straightens up, feet on the sill, then on the guardrail she previously hung to.

Placing her left hand at the top of the window, she reaches out but is still short of her windowsill.
She lowers her eyes and sees that the shadow now spreads its tentacles to the bottom of the building.
No choice left: she pushes on her legs and catches the sill of her window with the tips of her fingers.
Against all odds, she succeeds to heave herself and get the top of her body on the handrail.

Pushing on her legs again, drawing on her last strength, she finally gets into her room.
She gets up and closes the window immediately.
She draws back slowly and stumbles against her own bed.
Her hand hits something.
Is that ... a leg?
Her head swings about and she sees.
She's asleep in her bed.

Finally she understands!
She wasn't waiting for the sleep to come, she was only dreaming it.
Lowering her eyes, she finds that she's already dressed to sleep.
She just has to lie down and fully enjoy this restorative night.
She settles comfortably and closes her eyes, letting the shadow cover her window so that her dream can end in obscurity.

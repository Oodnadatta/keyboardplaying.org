---
date: 2019-04-01T10:52:27+02:00
title: L'attente
slug: l-attente
author: chop
license: CC BY-NC-ND 4.0

description: "<p>Elle attend, comme à chaque fois qu'elle a besoin de lui. L'impatience commence à la gagner.</p><p>Nouvelle rédigée dans le cadre d'un défi bimestriel.</p>"

challenge:
  type: bimestriel
  rules:
  - Nouvelle.
  - "Thème à respecter : le sommeil."
---

L'agacement commence à avoir raison de sa patience.
Bien entendu, elle a conscience que cela ne lui apportera rien : il ne vient jamais lorsqu'elle s'énerve.
Mais ce soir, elle a besoin de lui.
Elle a fait tout ce qu'elle pouvait pour préparer son concours demain.
À présent, tout repose sur son bon vouloir.

Mais il est comme un trousseau de clés : c'est toujours lorsqu'elle a le plus besoin de lui qu'il se fait le plus désirer.
En fait, il ne vient pratiquement jamais lorsqu'il est attendu.
À croire qu'il aime que son arrivée soit une surprise.
Il faut lui reconnaître un certain talent pour la rejoindre sans qu'elle ne s'en rende compte.

Elle se retourne, son mouvement rendu brusque par la frustration qu'elle tente ainsi d'évacuer, vainement.
Le temps passe et, toujours, aucun signe de lui.
De nouveau, l'impatience la gagne.
Cela ne sert à rien !

Une infusion lui ferait certainement du bien.
Elle se lève et se dirige vers la cuisine.
La bouilloire est retournée sur l'égouttoir.
Elle s'en saisit et la remplit d'eau avant de la poser sur la plaque de cuisson, qu'elle allume.
Pendant que l'eau chauffe, elle prépare un mélange de tilleul et de verveine, puis verse deux cuillères de miel au fond de son mug.

La bouilloire commence à siffler.
Elle coupe la plaque et verse l'eau dans le mug avant d'y submerger son sachet d'infusion.
Elle lève les yeux et observe son logement étudiant, dont elle voit pratiquement l'intégralité sans avoir à se déplacer.

La cuisine, un passage à peine suffisamment large pour ouvrir les tiroirs et portes des meubles dans lesquels elle range sa vaisselle et ses réserves, n'offre qu'un plan de travail, l'évier étant trop proche des plaques de cuisson pour respecter n'importe quelle norme de sécurité électrique.
Un bar, sur lequel son mug est posé, surplombe le plan de travail et ouvre sur la pièce principale ; son bureau-salon-salle à manger.
Elle a conservé le clic-clac et le fauteuil du précédent occupant.
Ce sont des produits d'un grand fabricant suédois, qu'elle a rachetés pour le tiers du prix original.
Le clic-clac est placé directement sous le bar, en face d'une table basse.
Enfin, ce qu'elle qualifie de table basse n'est jamais qu'une planche sur un tréteau à hauteur réglable.
Le fauteuil fait également face à la table, sur la gauche du sofa.
Et sur la gauche du fauteuil, une autre planche de bois, sur deux tréteaux, fait office de bureau.

Le sac posé sur son bureau lui donne une idée.
Elle fait le tour du bar et se dirige droit vers lui, pour extraire de son sac le livre qu'elle lit pour occuper ses vingt minutes de bus matin et soir.
Les transports rebutent plusieurs de ses camarades, mais elle y voit l'occasion de se changer les idées, de faire une vraie coupure dans sa journée.

Précisément ce dont elle a besoin pour arrêter de penser à lui !
Elle pose le livre sur sa table basse puis se dirige vers son bar pour inspecter le contenu de son mug.
La couleur est belle.
Elle le porte à son nez et hume les arômes.
L'infusion est parfaite !
Elle extrait le sachet et le presse contre le bord de la tasse avec sa cuillère pour en extraire tout le liquide, avant de le jeter vers la poubelle.
Raté !
Tant pis, elle le ramassera demain.

Elle remue consciencieusement afin de dissoudre tout le miel, dont l'essentiel était encore au fond, puis lève la tasse à ses lèvres pour goûter le résultat de sa préparation.
C'est trop chaud.
Elle pose le mug sur la table basse, se saisit du livre et s'assoit dans le fauteuil, celui qu'elle réserve à ses rares moments de détente.

Elle ouvre le livre à la page indiquée par son marque-page, prête à lire.
Elle s'interrompt, surprise : le texte est à l'envers.
Elle est pourtant certaine qu'elle tenait son livre dans le bon sens.
Perplexe, elle referme le livre pour en examiner la couverture.
Celle-ci est effectivement dans le bon sens, mais ce n'est pas le cas du titre.

La fatigue lui joue des tours, mais lui ne vient toujours pas.
Elle regarde sa montre.
La nuit est tombée mais il n'est pas encore si tard ; elle peut faire un tour de quartier pour se vider la tête.
L'infusion sera à température idéale à son retour et elle pourra prendre une bonne douche chaude --- dans la mesure où une douche à trente-sept degrés peut être qualifiée de « chaude » --- et il ne devrait plus tarder à arriver ensuite.

Elle enfile une veste légère, ses sneakers et sort de sa chambre étudiante, qu'elle verrouille.
L'ascenseur est en panne, bien évidemment, mais elle prend de toute façon toujours les escaliers, depuis le jour où, à sept ans, elle était restée coincée seule pendant près de trois heures.

Sortie de la résidence, elle bifurque immédiatement à droite.
Ce n'est pas vraiment un voisinage intéressant pour une promenade.
Il n'y a rien d'exceptionnel.
Uniquement des bâtiments, tous plus gris les uns que les autres.
La maison des élèves est sans doute l'immeuble le plus récent du quartier et était certainement blanc au départ.
Aujourd'hui, il a pris une teinte jaunâtre et l'ombre des rebords de fenêtre semble imprimée sur la façade.

Mais il y a une zone, à quelques centaines de mètres, où persistent quelques arbres et un peu de verdure.
Bien entendu, une part non négligeable de ce petit carré a été transformée en parc à chien, afin que les compagnons à quatre pattes puissent se soulager ailleurs que sur les trottoirs, mais ce petit contact avec la nature lui fait toujours tellement de bien.

Elle arrive au carrefour et tourne encore une fois à droite.
Le petit carré de verdure est à sa place.
L'éclairage public ne traverse pas complètement le feuillage des arbres et les immeubles entourant ce minuscule parc, créant une île isolée au milieu de l'océan urbain.
S'asseyant sur le banc, elle ferme les yeux.
L'herbe sous ses pieds et l'humidité qui commence à s'y condenser, la lumière raréfiée, le bruissement des feuilles dans la brise légère...
Elle se représente le jardin de sa grand-mère, en pente, avec le banc tout en haut, qui permet de voir la rue par-dessus les haies de troènes.
Voilà de quoi se détendre un peu !

Mais quelque chose n'est pas normal.
Elle ne saurait l'expliquer, mais c'est comme si elle ressentait une présence.
Quelque chose qui serait tapi dans les ombres.
Ce bruit ! Ce n'est pas celui des feuilles.

Elle se lève et se retourne vers le bruit, dans le coin le plus sombre du parc, mais ne voit rien d'autre que l'obscurité.
Les yeux rivés sur la zone noire, elle réalise l'incohérence de la situation : sa propre ombre est dirifée vers une totale absence de lumière.
Ce point devrait être directement éclairé par le lampadaire derrière elle.
Pourtant, contre toute logique, il y règne un noir absolu.

Et là, tandis qu'elle observe, l'ombre s'étend.
Pas comme si l'éclairage baissait, non.
Plutôt comme si cette zone d'ombres étandait d'immatériels tentacules.
Elle se sent comme si son cerveau était paralysé par l'absurdité de la situation.
Elle est cartésienne, elle fait des études pour devenir scientifique, mais ce qui se passe en face d'elle dépasse sa compréhension.
L'un des tentacules semble sur le point d'atteindre son ombre.
C'est le moment où ses instincts les plus profonds resurgissent, lorsqu'il faut choisir entre combat et fuite.
Et face à un adversaire si inconnu, si intangible, la seule réponse logique est la fuite !

Elle part et court aussi vite qu'elle le peut vers la résidence, mais elle ne reconnait pas les rues.
C'est comme si elle s'était retrouvée à l'autre bout de la ville.
Elle court sans se retourner et fait appel à toutes ses fonctions cognitives.

L'ombre n'est que l'absence de lumière.
Elle ne peut pas exister dans une zone directement éclairée et n'est pas une entité vivante dotée de tentacules.
Par ailleurs, elle n'a pas pu être à un endroit à un moment et à un autre l'instant suivant.
À moins que...

Elle n'entrevoit qu'une réponse à ce mystère, mais comment s'en assurer...
Là ! Elle vient de bifurquer au coin d'une rue et voit la résidence étudiante en face d'elle.
Elle se précipite vers l'entrée, à bout de souffle.

La porte est fermée, ses clés semblent avoir disparu et l'interphone est en panne depuis plusieurs semaines.
Elle regarde derrière elle, le chemin par lequel elle vient d'arriver, et voit que l'ombre semble franchir le coin, à quelques centaines de mètres d'elle.
Les yeux levés vers son logement au deuxième étage, elle constate que la fenêtre de sa chambre est restée ouverte.
En désespoir de cause, elle entreprend d'escalader la façade et de se réfugier chez elle.

Parvenir au premier étage n'est pas difficile.
Les barreaux servant de rambarde à chaque fenêtre fournissent également d'excellentes prises.
En dépit des cours d'escalade qu'elle suit depuis l'enfance, elle ne se risquerait jamais à escalader une façade en temps normal, mais cette ombre lui semble être une menace suffisante pour aller plus loin que d'habitude.
Se tenant sur les côtés de la fenêtre, elle se dresse, les pieds sur le rebord puis sur les barreaux même auxquels elle s'est accrochée.

Plaçant sa main gauche au haut de la fenêtre, elle tend le bras mais n'atteint pas le rebord de sa fenêtre.
Elle baisse alors les yeux et constate que l'ombre étend ses tentacules jusqu'au pied du bâtiment.
Tentant le tout pour le tout, elle pousse sur ses jambes et attrape le rebord de sa fenêtre du bout des doigts.
Contre toute attente, elle parvient, à la force de ses bras, à se hisser et à appuyer le haut de son corps sur le dernier barreau de sa fenêtre.

Poussant sur ses jambes, puisant dans ses dernières forces, elle parvient à entrer dans sa chambre.
Elle se relève et ferme la fenêtre immédiatement.
Elle recule et trébuche contre son propre lit, tombant assise dessus.
Sa main heurte quelque chose. Une jambe ?
Sa tête pivote malgré elle et elle voit.
Elle est couchée dans son lit, endormie.

Enfin, elle comprend !
Elle n'attendait pas le sommeil, elle rêvait seulement de cette attente.
Baissant les yeux, elle constate qu'elle est déjà en tenue pour dormir ; il ne lui reste qu'à se coucher et profiter pleinement de cette nuit réparatrice.
Elle s'installe confortablement et ferme les yeux, laissant l'ombre recouvrir sa fenêtre et son rêve s'achever dans l'obscurité.

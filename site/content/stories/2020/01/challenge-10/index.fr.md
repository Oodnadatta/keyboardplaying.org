---
date: 2020-01-10T16:42:45+02:00
title: Défi du 2020-01-10
slug: defi-2020-01-10
author: chop
license: CC BY-NC-ND 4.0

description: "<p>Je traverse la ville pour retrouver cet arbre que j'ai découvert hier soir. Je dois le revoir.</p><p>Micro-nouvelle rédigée dans le cadre d'un défi hebdomadaire.</p>"

challenge:
  type: hebdomadaire
  rules:
  - Format court.
  - "Utiliser tous les mots suivants : arbre, capillaire, exaltation, lambic, pizza, strapontin, urbain"
---

Le paysage défile, mais je suis pressé d'arriver.
Ce n'est pas que ce strapontin soit inconfortable --- bien que si, il l'est --- mais cette traversée urbaine n'en finit pas.

Je suis impatient !
Je dois retrouver cet arbre.
Il était tellement grand, imposant...
Non, « majestueux ».
Voilà le mot !
C'est un tel trésor caché au détour d'une ruelle de cette morne ville.
Sa simple vision m'a mis dans un tel état d'exaltation, hier soir.
Je dois vraiment le revoir maintenant qu'il fait jour.

Aujourd'hui, c'est plutôt une exaltation capillaire que je connais.
J'ai mal au crâne.
Non, le crâne n'est pas innervé, je le sais bien, mais j'ai l'impression que mes cheveux tentent d'y plonger leurs racines.

Là, c'est l'arrêt !
Je saute hors du bus et me dirige vers l'endroit où...
Non, je ne peux pas m'être trompé, c'était ici !
Pourquoi n'y a-t-il que cet arbrisseau mort ?!
Non, j'ai dû me tromper.

Dans quoi ai-je marché ?
Du vomi ?! Charmant !
Qu'est-ce que c'est que ces morceaux bizarres ?
De l'ananas...
Mais, je me souviens...
C'est moi qui ai été malade !
Ce sont les restes de ma pizza hawaïenne.

Mais alors, cet arbre mort ?
Je comprends mieux pourquoi les copains étaient pliés.
Il faut vraiment que j'arrête les soirées au lambic...

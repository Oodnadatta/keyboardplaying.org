---
title: Sustainable digital
description: Posts about ideas to contribute to making the digital---and especially software creation---a sustainable thing rather than another weight that'll drown us faster
translationKey: sustainable
---

---
title: Numérique responsable
description: Des idées pour nous aider à faire du numérique --- et plus particulièrement la création logicielle --- quelque chose de durable plutôt qu'un autre boulet qui nous noiera plus rapidement
translationKey: sustainable
---

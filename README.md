# _Keyboard Playing_

This repository contains all the source and contents for the website [keyboardplaying.org](https://keyboardplaying.org).

The website is built using the [Hugo] static generator.
Its compilation and deployment is automated through [Gitlab CI/CD](https://about.gitlab.com/product/continuous-integration/).

Feel free to clone the repository to test out my templates and get started with Hugo.
You can also copy the content as long as you respect the [licensing constraints](#licensing).


## Licensing

### Source code

The source code includes everything that is generic and used to build the website: HTML template, JavaScript scripts, SCSS stylesheets, Node, Webpack and other tools configuration ...

**All source code is licensed under [the MIT license](https://mit-license.org/).**

Don't hesitate to draw inspiration from it (yeah, copy-pasting is authorized if it helps you).


### Web site content

This repository also includes the content published on the website.

**Unless otherwise noted[^1], all contents are licensed under a [Creative Commons Attribution 4.0 International license](https://creativecommons.org/licenses/by/4.0/).**

[^1]: In case a content does not respect the default license, it is mentioned in the related page's front matter, or as a license file in the same directory.


### Exceptions

Some sources and images are available in this repo but are not my property.
Most of them were embedded to take advantage of modern HTTP protocols (so that your browser won't have to open a connection to a third-party site).
They remain the property of their respective authors.

> This website includes Font Awesome Pro icons I bought a license for.
> Those icons are protected by [the Font Awesome Pro License](https://fontawesome.com/license).

> Duke images are shared by Yannick Kirschhoffer, under the [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/) license.

> [XKCD comics](https://xkcd.com/) are licensed under [CC BY-NC 2.5](https://creativecommons.org/licenses/by-nc/2.5/)

## Content

### Posts and stories to come

> A [public Gitlab board](https://gitlab.com/keyboardplaying/keyboardplaying.org/-/boards/1340192) is available with all planned subjects.
> If you have a Gitlab account, you should be able to vote on issues to show you are interested in seeing them published as soon as possible.


## Running and building the site

### Technological stack

This site was inspired from [Netlify's Victor Hugo template](https://github.com/netlify-templates/victor-hugo).
The main idea is to have a website statically generated using [Hugo] while relying on [Node.js] to pilot the building process.
Scripts and styles are bundled and optimized using [Webpack].


### Quick start

1. Prerequisites: you should have [Git](https://git-scm.com), [Node.js] and NPM installed.
2. Clone the repository.
3. Run `npm i` to install all required Node dependencies.
4. Run one of the following commands to test out the website:
   * `npm run start`: Starts the website locally (`http://localhost:1313` for English, `http://localhost:1314` for French);
   * `npm run preview`: Same as `npm run start`, but also includes draft posts (excluded by default);
   * `npm run build`: Builds the static version of the site in `dist/en` and `dist/fr`. `tmp` is used for generated assets (e.g. Webpack and SVG sprites).


### Notes and warnings

- SVG icons are not watched.
If you need to add an icon to the SVG sprite, you will need to restart the script to have it taken into account.

- Hugo can minify the generated HTML.
However, to ease the debugging of templates, it was chosen to activate this option only for the building of sites.
This may result in differences between the locally served versions and the final build.



## Contributing

Merge requests are welcome, to enhance the source or fix mistakes in the content.


## Typography

### For all languages

* Mathematical signs: `+−×`

* Non-breaking space: ` ` (U+00A0) (<kbd>Alt</kbd>+<kbd>0</kbd><kbd>1</kbd><kbd>6</kbd><kbd>0</kbd> or <kbd>Alt</kbd>+<kbd>2</kbd><kbd>5</kbd><kbd>5</kbd>)

### French special characters

* _Espace fine insécable_: ` ` (U+202F) (to use with big punctuation: `!`, `?`, `;` and as thousand separator)
* _Espace-mot insécable_: ` ` (U+00A0) (<kbd>Alt</kbd>+<kbd>0</kbd><kbd>1</kbd><kbd>6</kbd><kbd>0</kbd> or <kbd>Alt</kbd>+<kbd>2</kbd><kbd>5</kbd><kbd>5</kbd>; to use with `:` and `«»`)
* Quotes: `« here my quoted text »` 
* Middle dot: `·` (for "inclusive writing")

[Hugo]: https://gohugo.io
[Node.js]: https://nodejs.org/en/
[Webpack]: https://webpack.js.org/

import axios from 'axios';
import serialize from 'form-serialize';

/** ID of the comment submission form. */
const STATICMAN_FORM_ID = 'staticman-form';

/** Query selector to select all "reply to thread" buttons. */
const STATICMAN_REPLY_BTN_SELECTOR = '#comments .reply-btn';

/** ID of the span used to display the author being replied to. */
const REPLYING_TO_AUTHOR_ID = 'replying-to';

/**
 * Submits a form to the Staticman server asynchronously.
 *
 * @param {*} e the form submission event
 */
const processCommentForm = (e) => {
    // Prevent default behaviour
    if (e.preventDefault) e.preventDefault();

    const form = e.currentTarget;
    form.classList.remove('success', 'error');
    form.classList.add('loading');

    axios({
        method: form.method,
        url: form.action,
        headers: { 'content-type': 'application/x-www-form-urlencoded' },
        data: serialize(form)
    })
        .then(() => {
            form.reset();
            form.classList.add('success');
        })
        .catch(() => {
            form.classList.add('error');
            form.classList.remove('loading');
        });

    // Prevent the form from being submitted
    return false;
};

const resetCommentFormHandler = (e) => {
    // Prevent default behaviour
    if (e.preventDefault) e.preventDefault();

    resetCommentForm(e.currentTarget);
};

const resetCommentForm = (form) => {
    form.reset();
    form.querySelector('[name="fields[replyThread]"]').value = null;
    form.classList.remove('replying', 'loading', 'success', 'error');
};

const setReplyTarget = (form) => {
    return (e) => {
        // The default behaviour is not prevented, but some additional behaviour are set

        resetCommentForm(form);
        const data = e.currentTarget.dataset;
        form.querySelector('[name="fields[replyThread]"]').value = data.thread;

        const comment = document.getElementById(data.comment);
        document.getElementById(REPLYING_TO_AUTHOR_ID).innerText = comment.querySelector('.comment-author').textContent;
        form.classList.add('replying');
    };
};

/**
 * Initializes the comment form with required handlers.
 */
const initCommentForm = () => {
    // Handle Staticman forms
    const form = document.getElementById(STATICMAN_FORM_ID);
    if (form) {
        form.addEventListener('submit', processCommentForm);
        form.addEventListener('reset', resetCommentFormHandler);

        const replyBtnHandler = setReplyTarget(form);
        const replyBtns = document.querySelectorAll(STATICMAN_REPLY_BTN_SELECTOR);
        replyBtns.forEach(btn => {
            btn.addEventListener('click', replyBtnHandler);
        });
    }
};

export { initCommentForm };

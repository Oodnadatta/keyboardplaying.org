// Import required JS
import { initCommentForm } from './js/staticman';

// Import stylesheet
import './css/main.scss';

/*
 * Self-executing function to be run once the page has been loaded.
 * This function sets all required listeners.
 */
(function () {
    initCommentForm();
})();
